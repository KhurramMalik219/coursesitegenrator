/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import djf.components.AppDataComponent;

/**
 *
 * @author khurr
 */
public class CSGData implements AppDataComponent {

    TAData taData;
    CourseData courseData;
    ScheduleData scheduleData;
    RecitationData recitationData;
    TeamData teamData;
    StudentData studentData; 
    CourseSiteGeneratorApp app;

    /**
     *
     * @param initApp
     */
    public CSGData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        taData = new TAData(app);
        recitationData = new RecitationData(app); 
        courseData = new CourseData(app); 
        scheduleData = new ScheduleData(app);
        teamData = new TeamData(app);
        studentData = new StudentData(app); 

    }

    @Override
    public void resetData() {
        taData.resetData();
        teamData.resetData();
        studentData.resetData();
        scheduleData.resetData();
        recitationData.resetData();
        courseData.resetData();
        
    }

    public TAData getTAData() {
        return taData;
    }

    public CourseData getCourseData() {
        return courseData;
    }

    public ScheduleData getScheduleData() {
        return scheduleData;

    }
    public RecitationData getRecitationData(){
        return recitationData;
    }

    public TeamData getTeamData() {
        return teamData;
    }

    public StudentData getStudentData() {
        return studentData;
    }

}
