/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableBooleanValue;

/**
 *
 * @author khurr
 */
/**
 * This class represents a Teaching Assistant for the table of TAs.
 *
 * @author Richard McKenna
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E> {

    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final BooleanProperty isGrad;

    /**
     * Constructor initializes both the TA name and email.
     */
    public TeachingAssistant(String initName, String initEmail, boolean initTsGrad) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        isGrad = new SimpleBooleanProperty(initTsGrad);

    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    public ObservableBooleanValue getIsGrad() {
        return isGrad;
    }

    public void setIsGrad(boolean initIsGrad) {
        isGrad.set(initIsGrad);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String initEmail) {
        email.set(initEmail);
    }

    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant) otherTA).getName());
    }

    @Override
    public String toString() {
        return name.getValue();
    }
}
