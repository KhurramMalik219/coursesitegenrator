/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.workspace.CSGWorkspace;
import csg.workspace.CourseWorkspace;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author khurr
 */
public class CourseData implements AppDataComponent {

    CourseSiteGeneratorApp app;
    // ObservableList<Course> course; 
    Course course;

    public CourseData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        //course = FXCollections.observableArrayList(); 
    }

    public void addCourse(String subject, String semester, String number, String year, String title, String instructName, String instructHome) {
        // Course c 
        course = new Course(subject, semester, number, year, title, instructName, instructHome);
        //course.add(c); 
    }

    @Override
    public void resetData() {
        // course.setNumber("");
        //course.setSemester("");
        //course.setInstructName("");
        //course.setSemester("");
        //course.setSubject("");
        //course.setTitle("");
        //course.setYear("");
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        CourseWorkspace courseWorkspace = csgWorkspace.getCourseWorkspace();
        courseWorkspace.getSubjectComboBox().setValue(null);
        courseWorkspace.getSubjectComboBox().setPromptText(null);
        courseWorkspace.getSemesterComboBox().setValue(null);
        courseWorkspace.getSemesterComboBox().setPromptText(null);

        courseWorkspace.getNumberComboBox().setValue(null);
        courseWorkspace.getNumberComboBox().setPromptText(null);

        courseWorkspace.getInstHomeTF().setText(null);
        courseWorkspace.getInstNameTF().setText(null);
        courseWorkspace.getYearComboBox().setValue(null);
        courseWorkspace.getTitleTF().setText(null);

    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public Course getCourse() {
        return course;
    }

    public void addCourseToWorkspace() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        CourseWorkspace courseWorkspace = csgWorkspace.getCourseWorkspace();
        courseWorkspace.getSubjectComboBox().setValue(course.getSubject());
        courseWorkspace.getSubjectComboBox().setPromptText(course.getSubject());
        courseWorkspace.getSemesterComboBox().setValue(course.getSemester());
        courseWorkspace.getSemesterComboBox().setPromptText(course.getSemester());

        courseWorkspace.getNumberComboBox().setValue(course.getNumber());
        courseWorkspace.getNumberComboBox().setPromptText("" + course.getNumber());

        courseWorkspace.getInstHomeTF().setText(course.getInstructHome());
        courseWorkspace.getInstNameTF().setText(course.getInstructName());
        courseWorkspace.getYearComboBox().setValue(course.getYear());
        courseWorkspace.getTitleTF().setText(course.getTitle());
    }

    // public ObservableList<Course> getCourse() {
    //     return course;
    // }
}
