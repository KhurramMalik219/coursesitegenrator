/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author khurr
 * @param <E>
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E> {

    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty day_time;
    private final StringProperty location;
    private final StringProperty ta_1;
    private final StringProperty ta_2;
    private final StringProperty startTime;
    private final StringProperty endTime;
    private final StringProperty day;

    public Recitation(String initSection, String initInstructor, String initDay, String initStartTime, String initEndTime, String initLocation,
            String initSuperTA1, String initSuperTA2) {
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        startTime = new SimpleStringProperty(initStartTime);
        endTime = new SimpleStringProperty(initEndTime);
        location = new SimpleStringProperty(initLocation);
        ta_1 = new SimpleStringProperty(initSuperTA1);
        ta_2 = new SimpleStringProperty(initSuperTA2);
        day = new SimpleStringProperty(initDay);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime start = LocalTime.parse(startTime.get());
        LocalTime end = LocalTime.parse(endTime.get());

        System.out.println("RECITATION " + formatter.format(start));
        System.out.println("RECITATION END" + formatter.format(end));
        String formatedStart = formatter.format(start);
        String formatedEnd = formatter.format(end);
        day_time = new SimpleStringProperty(day.get() + " " + formatedStart + "-" + formatedEnd);

    }

    public String getStartTime() {
        return startTime.get();
    }

    public void setStartTime(String initStart) {
        startTime.set(initStart);
    }

    public String getEndTime() {
        return endTime.get();
    }

    public void setEndTime(String initEnd) {
        endTime.set(initEnd);
    }

    public String getDay() {
        return day.get();
    }

    public void setDay(String initDayTime) {
        day.set(initDayTime);
    }

    public String getSection() {
        return section.get();
    }

    public void setSection(String initSection) {
        section.set(initSection);
    }

    public String getInstructor() {
        return instructor.get();
    }

    public void setInstructor(String initInstructor) {
        instructor.set(initInstructor);
    }

    public String getDay_time() {
        return day_time.get();
    }

    public void setDay_time(String startTime, String endTime, String day) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime start = LocalTime.parse(startTime);
        LocalTime end = LocalTime.parse(endTime);

        System.out.println("RECITATION " + formatter.format(start));
        System.out.println("RECITATION END" + formatter.format(end));
        String formatedStart = formatter.format(start);
        String formatedEnd = formatter.format(end);
        day_time.set(day + " " + formatedStart + "-" + formatedEnd);
    }

    public String getLocation() {
        return location.get();
    }

    public void setLocation(String initLocation) {
        location.set(initLocation);
    }

    public String getTa_1() {
        return ta_1.get();
    }

    public void setTa_1(String initSuperTA1) {
        ta_1.set(initSuperTA1);
    }

    public String getTa_2() {
        return ta_2.get();
    }

    public void setTa_2(String initSuperTA2) {
        ta_2.set(initSuperTA2);
    }

    @Override
    public int compareTo(E otherRecitation) {
        return getSection().compareTo(((Recitation) otherRecitation).getSection());
    }

}
