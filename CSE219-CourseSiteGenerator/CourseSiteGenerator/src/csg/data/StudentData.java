/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author khurr
 */
public class StudentData implements AppDataComponent {

    CourseSiteGeneratorApp app;
    ObservableList<Student> students;

    public StudentData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        students = FXCollections.observableArrayList();
    }

    public void addStudent(String firstName, String lastName, String Team, String role) {
        Student stu = new Student(firstName, lastName, Team, role);
        students.add(stu);
    }

    public ObservableList<Student> deleteStudentsInTeam(String teamName) {

        ObservableList<Student> studentsToBeDeleted = FXCollections.observableArrayList();
        for (Student s : students) {
            if (s.getTeam().equals(teamName)) {
                studentsToBeDeleted.add(s);
            }
        }
        students.removeAll(studentsToBeDeleted);
        return studentsToBeDeleted; 

    }

    public void deleteStudent(String firstName, String lastName) {
        for (Student s : students) {
            if (s.getFirstName().equals(firstName) && s.getLastName().equals(lastName)) {
                students.remove(s);
                return;
            }
        }

    }

    public void updateTeamName(String orgTeamName, String newTeamName) {
        for (Student s : students) {
            if (s.getTeam().equalsIgnoreCase(orgTeamName)) {
                s.setTeam(newTeamName);

            }
        }

    }

    public boolean containsStudent(String firstName, String lastName) {
        for (Student s : students) {
            if (s.getFirstName().equalsIgnoreCase(firstName) && s.getLastName().equalsIgnoreCase(lastName)) {
                return true;
            }
        }
        return false;
    }

    public void updateStudent(String orgFirstName, String orgLastName, String firstName, String lastName, String team, String role) {

        Student s = getStudent(orgFirstName, orgLastName);
        s.setFirstName(firstName);
        s.setLastName(lastName);
        s.setTeam(team);
        System.out.println(team);
        s.setRole(role);

    }

    public Student getStudent(String firstName, String lastName) {
        for (Student s : students) {
            if (s.getFirstName().equalsIgnoreCase(firstName) && s.getLastName().equalsIgnoreCase(lastName)) {
                return s;
            }
        }
        return null;

    }
    public ObservableList<Student> getStudentsInTeam(String team){
        ObservableList<Student> list = FXCollections.observableArrayList();
    
        for (Student s : students) {
            if(s.getTeam().equals(team))
                list.add(s);
        }  
        return list; 
    }

    @Override
    public void resetData() {
        students.clear();
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public ObservableList<Student> getStudents() {
        return students;
    }

}
