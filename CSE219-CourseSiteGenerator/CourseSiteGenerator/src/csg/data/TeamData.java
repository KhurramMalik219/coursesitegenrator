/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.workspace.CSGWorkspace;
import csg.workspace.ProjectWorkspace;
import djf.components.AppDataComponent;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

/**
 *
 * @author khurr
 */
public class TeamData implements AppDataComponent {

    CourseSiteGeneratorApp app;
    ObservableList<Team> teams;

    public TeamData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        teams = FXCollections.observableArrayList();
    }

    public void addTeam(String initName, String red, String green, String blue, String initTextColor, String initLink) {
        Team t = new Team(initName, red, green, blue, initTextColor, initLink);
        teams.add(t);
        
    }

    public void addTeamToWorkspace(String name) {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        workspace.getTeamComboBox().getItems().add(name);

    }

    public void deleteTeam(String name) {
        for (Team t : teams) {
            if (name.equals(t.getName())) {
                teams.remove(t);
                return;
            }
        }
    }

    public boolean containsTeam(String teamName) {
        for (Team t : teams) {
            if (t.getName().equalsIgnoreCase(teamName)) {
                return true;
            }
        }
        return false;
    }

   // public void updateTeamNameChanged(String orgName, String newName) {
   //     Team t = getTeam(orgName);
   //     t.setName(newName);
   // }
    public void updateTeam(String newName,String link,String color,String red,String green,String blue,String textColor, String oldName){
        Team t= getTeam(oldName);
        t.setName(newName);
        t.setColor(color);
        t.setRed(red);
        t.setBlue(blue);
        t.setGreen(green);
        t.setLink(link);
        t.setTextColor(textColor);
    }

    @Override
    public void resetData() {
        teams.clear();
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public ObservableList<Team> getTeams() {
        return teams;
    }

    public Team getTeam(String name) {
        for (Team t : teams) {
            if (t.getName().equalsIgnoreCase(name)) {
                return t;
            }
        }
        return null;
    }
}
