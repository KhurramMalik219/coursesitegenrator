/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.workspace.CSGWorkspace;
import csg.workspace.RecitationWorkspace;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author khurr
 */
public class RecitationData implements AppDataComponent {
    
    CourseSiteGeneratorApp app;    
    ObservableList<Recitation> recitation;
    
    public RecitationData(CourseSiteGeneratorApp initApp) {
        app = initApp;        
        recitation = FXCollections.observableArrayList();
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
    }
    
    public void addRecitation(String section, String instructor, String day, String startTime, String endTime, String location, String superTA1, String superTA2) {
        Recitation rec = new Recitation(section, instructor, day, startTime, endTime, location, superTA1, superTA2);
        recitation.add(rec);
        //Collections.sort(recitation);
    }

    public void deleteRecitation(String section) {
        for (Recitation rec : recitation) {
            if (section.equals((rec.getSection()))) {
                recitation.remove(rec);
                return;                
            }
        }
    }

    public void addTAToComboBox(String name) {
        CSGWorkspace workspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace w = workspace.getRecitationWorkspace();
        w.getSuperTA_1ComboBox().getItems().add(name);        
        w.getSuperTA_2ComboBox().getItems().add(name);        
        
    }
    public void updateRecitation(String orgSection, String section, String instructor, String day, String startTime, String endTime, String location, String ta1, String ta2){
        Recitation r = getRecitation(orgSection); 
        r.setSection(section);
        r.setInstructor(instructor);
        r.setDay(day);
        r.setStartTime(startTime);
        r.setEndTime(endTime);
        r.setLocation(location);
        r.setTa_1(ta1);
        r.setTa_2(ta2);
        r.setDay_time(startTime, endTime, day);
        
    }
    public Recitation getRecitation(String section){
        for(Recitation r : recitation){
            if(r.getSection().equals(section)){
                return r;
            }
        }
        return null; 
    }

    public boolean containsRecitation(String section) {
        for (Recitation rec : recitation) {
            if (rec.getSection().equals(section)) {
                return true;
            }
        }
        return false;        
        
    }

    @Override
    public void resetData() {
        recitation.clear();
    }
    
    public CourseSiteGeneratorApp getApp() {
        return app;
    }
    
    public ObservableList<Recitation> getRecitation() {
        return recitation;
    }
    
}
