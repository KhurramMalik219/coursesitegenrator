/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.workspace.CSGWorkspace;
import csg.workspace.ScheduleWorkspace;
import djf.components.AppDataComponent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.util.Callback;

/**
 *
 * @author khurr
 */
public class ScheduleData implements AppDataComponent {

    CourseSiteGeneratorApp app;
    ObservableList<Schedule> schedule;
    public static String startMonday;
    public static String endFriday;

    public ScheduleData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        schedule = FXCollections.observableArrayList();
        startMonday = null;
        endFriday = null;

    }

    public void addSchedule(String type, String date, String time, String title,
            String topic, String link, String criteria) {
        Schedule sc = new Schedule(type, date, time, title, topic, link, criteria);
        schedule.add(sc);

    }

    public void removeSchedule(String type, String date, String title) {
        for (Schedule s : schedule) {
            if (type.equals(s.getType()) && date.equals(s.getDate()) && title.equals(s.getTitle())) {
                schedule.remove(s);
                return;
            }
        }
    }

    public boolean containsSchedule(String date) {
        for (Schedule s : schedule) {
            if (s.getDate().equals(date)) {
                return true;
            }
        }
        return false;
    }

    public void updateSchedule(String orgDate, String type, String date, String time, String title, String topic, String link, String criteria) {
        Schedule s = getSchedule(orgDate);
        s.setDate(date);
        s.setType(type);
        s.setTime(time);
        s.setTitle(title);
        s.setTopic(topic);
        s.setLink(link);
        s.setCriteria(criteria);
    }

    public Schedule getSchedule(String orgDate) {
        for (Schedule s : schedule) {
            if (s.getDate().equals(orgDate)) {
                return s;
            }
        }
        return null;
    }

    @Override
    public void resetData() {
        schedule.clear();
        startMonday = null;
        endFriday = null;
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public ObservableList<Schedule> getSchedule() {
        return schedule;
    }

    public String getStartMonday() {
        return startMonday;
    }

    public void setStartMonday(String startMonday) {
        ScheduleData.startMonday = startMonday;
    }

    public String getEndFriday() {
        return endFriday;
    }

    public void setEndFriday(String endFriday) {
        ScheduleData.endFriday = endFriday;
    }

    public void loadDatesToWorkspace() {
        CSGWorkspace workspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace w = workspace.getScheduleWorkspace();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);

        LocalDate startMondayDate = LocalDate.parse(startMonday, formatter);
        LocalDate endFridayDate = LocalDate.parse(endFriday, formatter);
        w.getMondayDatePicker().setValue(startMondayDate);
        w.getFridayDatePicker().setValue(endFridayDate);
        final Callback<DatePicker, DateCell> dayCellFactory
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item.isBefore(w.getMondayDatePicker().getValue()) || (item.getDayOfWeek() != DayOfWeek.FRIDAY)) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        w.getFridayDatePicker().setDayCellFactory(dayCellFactory);

    }

    public LocalDate mondayLocalDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);

        return LocalDate.parse(startMonday, formatter);

    }

    public LocalDate fridayLocalDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);

        return LocalDate.parse(endFriday, formatter);

    }

    public ObservableList<Schedule> getSpecificScheduleList(String type) {
        ObservableList<Schedule> List = FXCollections.observableArrayList();
        for (Schedule s : schedule) {
            if (s.getType().equalsIgnoreCase(type)) {
                List.add(s);
            }
        }
        return List;
    }

    public LocalDate getLocalDate(String date) {
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);

        return LocalDate.parse(date, formatter);

    }
    

}
