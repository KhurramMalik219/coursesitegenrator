/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author khurr
 */
public class Team<E extends Comparable<E>> implements Comparable<E> {

    private final StringProperty name;
    private final StringProperty color;
    private final StringProperty red;
    private final StringProperty green;
    private final StringProperty blue; 
    private final StringProperty textColor;
    private final StringProperty link;
    
    

    public Team(String initName, String initRed,String initGreen, String initBlue, String initTextColor, String initLink)   {
        name = new SimpleStringProperty(initName);
        red= new SimpleStringProperty(initRed);
        green=new SimpleStringProperty(initGreen);
        blue=new SimpleStringProperty(initBlue);
        color=new SimpleStringProperty(red.get()+green.get()+blue.get());
        textColor = new SimpleStringProperty(initTextColor);
        link = new SimpleStringProperty(initLink);
        
        

    }
  
    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getColor() {
        return color.get();
    }

    public void setColor(String initColor) {
        color.set(initColor);
    }

    public String getTextColor() {
        return textColor.get();
    }

    public void setTextColor(String initTextColor) {
        textColor.set(initTextColor);
    }

    public String getLink() {
        return link.get();
    }

    public void setLink(String initLink) {
        link.set(initLink);
    }

    public String getRed() {
        return red.get();
    }
    public void setRed(String initRed){
        red.set(initRed);
    }
    public String getGreen() {
        return green.get();
    }
    public void setGreen(String initGreen){
        green.set(initGreen);
    }

    public String getBlue() {
        return blue.get();
    }
    public void setBlue(String initBlue){
        blue.set(initBlue);
    }

    @Override
    public int compareTo(E otherTeam) {
        return getName().compareTo(((Team)otherTeam).getName()); 
    }
    

}
