/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author khurr
 */
public class Course<E extends Comparable<E>> implements Comparable<E> {

    private final StringProperty subject;
    private final StringProperty semester;
    private final StringProperty number;
    private final StringProperty year;
    private final StringProperty title;
    private final StringProperty instructName;
    private final StringProperty instructHome;

    /**
     * Constructor initializes Course Details
     *
     * @param initSubject
     * @param initSemester
     * @param initNumber
     * @param initYear
     * @param initTitle
     * @param initInstructName
     * @param initInstructHome
     */
    public Course(String initSubject, String initSemester, String initNumber, String initYear, String initTitle, String initInstructName, String initInstructHome) {
        subject = new SimpleStringProperty(initSubject);
        semester = new SimpleStringProperty(initSemester);
        number = new SimpleStringProperty(initNumber);
        year = new SimpleStringProperty(initYear);
        title = new SimpleStringProperty(initTitle);
        instructName = new SimpleStringProperty(initInstructName);
        instructHome = new SimpleStringProperty(initInstructHome);
    }

    // ACCESSOR AND MUTATOR METHODS FOR THE PROPERTIES OF COURSE 
    public String getSubject() {
        return subject.get(); 
    }

    public void setSubject(String initSubject) {
        subject.set(initSubject);
    }

    public String getSemester() {
        return semester.get(); 
    }

    public void setSemester(String initSemester) {
        semester.set(initSemester);
    }

    public String getNumber() {
        return number.get();
    }

    public void setNumber(String initNumber) {
        number.set(initNumber);
    }

    public String getYear() {
        return year.get();
    }

    public void setYear(String initYear) {
        year.set(initYear);
    }

    public String getTitle() {
        return title.get(); 
    }

    public void setTitle(String initTitle) {
        title.set(initTitle);
    }

    public String getInstructName() {
        return instructName.get(); 
    }

    public void setInstructName(String initInstructName) {
        instructName.set(initInstructName);
    }

    public String getInstructHome() {
        return instructHome.get(); 
    }

    public void setInstructHome(String initInstructHome) {
        instructHome.set(initInstructHome);
    }

    @Override
    public int compareTo(E o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
