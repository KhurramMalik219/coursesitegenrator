package csg.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.Course;
import csg.data.CourseData;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import csg.data.Student;
import csg.data.StudentData;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.data.TeamData;
import csg.workspace.CSGWorkspace;
import csg.workspace.ColorUtils;
import csg.workspace.CourseWorkspace;
import csg.workspace.CourseWorkspace.CourseTableData;
import static djf.settings.AppPropertyType.EXPORT_TITLE;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import javafx.collections.FXCollections;
import javafx.stage.DirectoryChooser;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;

/**
 * This class serves as the file component for the TA manager app. It provides
 * all saving and loading services for the application.
 *
 * @author Richard McKenna
 */
public class CSGFiles implements AppFileComponent {

    // THIS IS THE APP ITSELF
    CourseSiteGeneratorApp app;

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_ISGRADLABEL = "isGrad";
    //RECITATION
    static final String JSON_RECITATION = "recitations";
    static final String JSON_REC_SECTION = "section";
    static final String JSON_REC_INSTRUCTOR = "instructor";
    static final String JSON_REC_DAYTIME = "day_time";
    static final String JSON_REC_LOCATION = "location";
    static final String JSON_REC_SUPERTA1 = "ta_1";
    static final String JSON_REC_SUPERTA2 = "ta_2";
    static final String JSON_REC_DAY = "day";
    static final String JSON_REC_START = "startTime";
    static final String JSON_REC_END = "endTime";

    //COURSE INFO 
    static final String JSON_COURSE = "course_info";
    static final String JSON_C_SUBJECT = "subject";
    static final String JSON_C_SEMESTER = "semester";
    static final String JSON_C_NUMBER = "number";
    static final String JSON_C_YEAR = "year";
    static final String JSON_C_TITLE = "title";
    static final String JSON_C_INSTRUCTHOME = "instructor_home";
    static final String JSON_C_INSTRUCTNAME = "instructor_name";

    // SCHEDULE INFO 
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_S_TYPE = "type";
    static final String JSON_S_TIME = "time";
    static final String JSON_S_DATE = "date";
    static final String JSON_S_TITLE = "title";
    static final String JSON_S_TOPIC = "topic";
    static final String JSON_S_LINK = "link";
    static final String JSON_S_CRITERIA = "criteria";
    static final String JSON_S_STARTMONDAY = "startingMonday";
    static final String JSON_S_ENDFRIDAY = "endingFriday";
    static final String JSON_S_MONTH = "month";

    // STUDENT INFO STUFF 
    static final String JSON_STUDENT = "students";
    static final String JSON_STU_FIRSTNAME = "firstName";
    static final String JSON_STU_LASTNAME = "lastName";
    static final String JSON_STU_TEAM = "team";
    static final String JSON_STU_ROLE = "role";

    // TEAM STUFF 
    static final String JSON_TEAM = "teams";
    static final String JSON_TEAM_NAME = "name";
    static final String JSON_TEAM_COLOR = "color";
    static final String JSON_TEAM_RED = "red";
    static final String JSON_TEAM_GREEN = "green";
    static final String JSON_TEAM_BLUE = "blue";
    static final String JSON_TEAM_TEXTCOLOR = "text_color";
    static final String JSON_TEAM_LINK = "link";
    static final String JSON_TEAM_COLOR_VALUE = "colorValue";
    static final String JSON_TEAM_TEXTCOLORVALUE = "textColorValue";

    public CSGFiles(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT

        CSGData CSGdataManager = (CSGData) data;
        TAData dataManager = CSGdataManager.getTAData();
        RecitationData recDataManager = CSGdataManager.getRecitationData();
        CourseData courseDataManager = CSGdataManager.getCourseData();
        ScheduleData sDataManager = CSGdataManager.getScheduleData();
        TeamData teamDataManager = CSGdataManager.getTeamData();
        StudentData studentDataManager = CSGdataManager.getStudentData();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // LOAD THE START AND END HOURS
        String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        // LOAD COURSE INFO
        String subject = json.getString(JSON_C_SUBJECT);
        String semester = json.getString(JSON_C_SEMESTER);
        String number = json.getString(JSON_C_NUMBER);
        String year = json.getString(JSON_C_YEAR);
        String title = json.getString(JSON_C_TITLE);
        String instructorName = json.getString(JSON_C_INSTRUCTNAME);
        String instructorHome = json.getString(JSON_C_INSTRUCTHOME);
        //int number = Integer.parseInt(numberString);
        //int year = Integer.parseInt(yearString);
        courseDataManager.addCourse(subject, semester, number, year, title, instructorName, instructorHome);
        courseDataManager.addCourseToWorkspace();

        String startingMonday = json.getString(JSON_S_STARTMONDAY);
        String endingFriday = json.getString(JSON_S_ENDFRIDAY);
        sDataManager.setStartMonday(startingMonday);
        sDataManager.setEndFriday(endingFriday);
        sDataManager.loadDatesToWorkspace();

        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean isGrad = jsonTA.getBoolean(JSON_ISGRADLABEL);
            dataManager.addTA(name, email, isGrad);
            recDataManager.addTAToComboBox(name);

        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }

        // RECITATION DATA STUFF -----------------------------------------------
        JsonArray jsonRecArray = json.getJsonArray(JSON_RECITATION);
        for (int i = 0; i < jsonRecArray.size(); i++) {
            JsonObject jsonRec = jsonRecArray.getJsonObject(i);
            String section = jsonRec.getString(JSON_REC_SECTION);
            String instructor = jsonRec.getString(JSON_REC_INSTRUCTOR);
            String day = jsonRec.getString(JSON_REC_DAY);
            String startTime = jsonRec.getString(JSON_REC_START);
            String endTime = jsonRec.getString(JSON_REC_END);
            //String dayTime = jsonRec.getString(JSON_REC_DAYTIME);
            String location = jsonRec.getString(JSON_REC_LOCATION);
            String superTA1 = jsonRec.getString(JSON_REC_SUPERTA1);
            String superTA2 = jsonRec.getString(JSON_REC_SUPERTA2);

            recDataManager.addRecitation(section, instructor, day, startTime, endTime, location, superTA1, superTA2);

        }

        //SCHEDULE DATA LOADING-------------------------------------------------
        JsonArray jsonSArray = json.getJsonArray(JSON_SCHEDULE);
        for (int i = 0; i < jsonSArray.size(); i++) {
            JsonObject jsonSchedule = jsonSArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_S_TYPE);
            String date = jsonSchedule.getString(JSON_S_DATE);
            String time = jsonSchedule.getString(JSON_S_TIME);
            String scheduletitle = jsonSchedule.getString(JSON_S_TITLE);
            String topic = jsonSchedule.getString(JSON_S_TOPIC);
            String link = jsonSchedule.getString(JSON_S_LINK);
            String criteria = jsonSchedule.getString(JSON_S_CRITERIA);
            sDataManager.addSchedule(type, date, time, scheduletitle, topic, link, criteria);

        }
        //----------------------------------------------------------------------

        //TEAM DATA LOADING ----------------------------------------------------
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAM);
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String teamName = jsonTeam.getString(JSON_TEAM_NAME);
            String teamRed = jsonTeam.getString(JSON_TEAM_RED);
            String teamGreen = jsonTeam.getString(JSON_TEAM_GREEN);
            String teamBlue = jsonTeam.getString(JSON_TEAM_BLUE);
            String link = jsonTeam.getString(JSON_TEAM_LINK);
            String textColor = jsonTeam.getString(JSON_TEAM_TEXTCOLOR);

            teamDataManager.addTeam(teamName, teamRed, teamGreen, teamBlue, textColor, link);
            teamDataManager.addTeamToWorkspace(teamName);
        }

        //----------------------------------------------------------------------
        //STUDENT DATA LOADING -------------------------------------------------
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENT);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String studentFirstName = jsonStudent.getString(JSON_STU_FIRSTNAME);
            String studentLastName = jsonStudent.getString(JSON_STU_LASTNAME);
            String studentTeam = jsonStudent.getString(JSON_STU_TEAM);
            String studentRole = jsonStudent.getString(JSON_STU_ROLE);

            studentDataManager.addStudent(studentFirstName, studentLastName, studentTeam, studentRole);
        }

    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        CSGData CSGdataManager = (CSGData) data;

        TAData dataManager = CSGdataManager.getTAData();

        // NOW BUILD THE TA JSON OBJCTS TO SAVE
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_ISGRADLABEL, ta.getIsGrad().getValue())
                    .build();

            taArrayBuilder.add(taJson);
        }

        JsonArray undergradTAsArray = taArrayBuilder.build();

        // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> officeHours;
        try {
            officeHours = TimeSlot.buildOfficeHoursList(dataManager); // noraml method line 
        } catch (NullPointerException e) {
            //ArrayList<TimeSlot> officeHoursForTest = dataManager.getTimeSlot();  // FOR TESTING 
            officeHours = dataManager.getTimeSlot();
        }
        for (TimeSlot ts : officeHours) {
            JsonObject tsJson = Json.createObjectBuilder()
                    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
                    .add(JSON_NAME, ts.getName()).build();
            timeSlotArrayBuilder.add(tsJson);
        }
        JsonArray timeSlotsArray = timeSlotArrayBuilder.build();

        //-----------------------RECITATION ------------------------------------
        RecitationData recDataManager = CSGdataManager.getRecitationData();
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = recDataManager.getRecitation();
        for (Recitation rec : recitations) {
            JsonObject recJson = Json.createObjectBuilder()
                    .add(JSON_REC_SECTION, rec.getSection())
                    .add(JSON_REC_INSTRUCTOR, rec.getInstructor())
                    //.add(JSON_REC_DAYTIME, rec.getDay_time())
                    .add(JSON_REC_DAY, rec.getDay())
                    .add(JSON_REC_START, rec.getStartTime())
                    .add(JSON_REC_END, rec.getEndTime())
                    .add(JSON_REC_LOCATION, rec.getLocation())
                    .add(JSON_REC_SUPERTA1, rec.getTa_1())
                    .add(JSON_REC_SUPERTA2, rec.getTa_2())
                    .build();

            recArrayBuilder.add(recJson);
        }
        JsonArray recitationArray = recArrayBuilder.build();

        //---------------------------------------------------------------------
        CourseData courseDataManager = CSGdataManager.getCourseData();
        Course c = courseDataManager.getCourse();
        /* //COURSE 
         
        JsonArrayBuilder courseArrayBuilder = Json.createArrayBuilder(); 
        ObservableList<Course> course=courseDataManager.getCourse(); 
        for(Course c : course){
            JsonObject courseJson = Json.createObjectBuilder()
                    .add(JSON_C_SUBJECT, c.getSubject())
                    .add(JSON_C_SEMESTER, c.getSemester())
                    .add(JSON_C_NUMBER, c.getNumber())
                    .add(JSON_C_YEAR, c.getYear())
                    .add(JSON_C_TITLE, c.getTitle())
                    .add(JSON_C_INSTRUCTNAME, c.getInstructName())
                    .add(JSON_C_INSTRUCTHOME, c.getInstructHome())
                    .build(); 
            courseArrayBuilder.add(courseJson);
        }
        JsonArray courseArray = courseArrayBuilder.build(); 
        
        // COURSE END */

        //--------------------- SCHEDULE ----------------------------------- 
        ScheduleData sDataManager = CSGdataManager.getScheduleData();
        JsonArrayBuilder sArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedule = sDataManager.getSchedule();
        for (Schedule s : schedule) {
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_TYPE, s.getType())
                    .add(JSON_S_DATE, s.getDate())
                    .add(JSON_S_TIME, s.getTime())
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_TOPIC, s.getTopic())
                    .add(JSON_S_LINK, s.getLink())
                    .add(JSON_S_CRITERIA, s.getCriteria())
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray sArray = sArrayBuilder.build();
        // ---------------------------------------

        //-------------- TEAM -----------------------------------------------
        TeamData teamData = CSGdataManager.getTeamData();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = teamData.getTeams();
        for (Team t : teams) {
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add(JSON_TEAM_RED, t.getRed())
                    .add(JSON_TEAM_GREEN, t.getGreen())
                    .add(JSON_TEAM_BLUE, t.getBlue())
                    .add(JSON_TEAM_TEXTCOLOR, t.getTextColor())
                    .add(JSON_TEAM_LINK, t.getLink())
                    //.add(JSON_TEAM_COLOR_VALUE, t.getColorValue())   // ADDED LINES HERE
                    //.add(JSON_TEAM_TEXTCOLORVALUE,t.getTextColorValue())
                    .build();
            teamArrayBuilder.add(teamJson);
        }
        JsonArray teamArray = teamArrayBuilder.build();

        //----------------------------------------------------------------------
        // ------------------ STUDENT ------------------------------------------
        StudentData stuDataManager = CSGdataManager.getStudentData();
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = stuDataManager.getStudents();
        for (Student s : students) {
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_STU_LASTNAME, s.getLastName())
                    .add(JSON_STU_FIRSTNAME, s.getFirstName())
                    .add(JSON_STU_TEAM, s.getTeam())
                    .add(JSON_STU_ROLE, s.getRole())
                    .build();
            studentArrayBuilder.add(studentJson);

        }
        JsonArray studentArray = studentArrayBuilder.build();
        //----------------------------------------------------------------------
        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_C_SUBJECT, c.getSubject())
                .add(JSON_C_SEMESTER, c.getSemester())
                .add(JSON_C_NUMBER, "" + c.getNumber())
                .add(JSON_C_YEAR, "" + c.getYear())
                .add(JSON_C_TITLE, c.getTitle())
                .add(JSON_C_INSTRUCTNAME, c.getInstructName())
                .add(JSON_C_INSTRUCTHOME, c.getInstructHome())
                .add(JSON_START_HOUR, "" + dataManager.getStartHour())
                .add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_S_STARTMONDAY, sDataManager.getStartMonday())
                .add(JSON_S_ENDFRIDAY, sDataManager.getEndFriday())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add(JSON_RECITATION, recitationArray)
                .add(JSON_SCHEDULE, sArray)
                .add(JSON_TEAM, teamArray)
                .add(JSON_STUDENT, studentArray)
                // .add(JSON_COURSE, courseArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, File currentWorkFile) throws IOException {
        //String filePath = "";
        //exportTAData(data, filePath);
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        try {
            // PROMPT THE USER FOR A FILE NAME

//            DirectoryChooser fc = new DirectoryChooser();
//            fc.setInitialDirectory(new File(PATH_WORK));
//            fc.setTitle(props.getProperty(EXPORT_TITLE));
//
//            File selectedFile = fc.showDialog(app.getGUI().getWindow());
            CSGWorkspace workspace = (CSGWorkspace) app.getWorkspaceComponent();
            CourseWorkspace cw = workspace.getCourseWorkspace();
            String selectedFileString = cw.getDirLabel().getText();
            File selectedFile = new File(selectedFileString);
            File choose = new File("../CSE_219_Test/public_html");

            String officeHoursGridFilePath = selectedFile.getAbsoluteFile() + "/js/OfficeHoursGridData.json";
            String teamsAndStudentsFilePath = selectedFile.getAbsolutePath() + "./js/TeamsAndStudents.json";
            String recitationDataFilePath = selectedFile.getAbsolutePath() + "/js/RecitationsData.json";
            String scheduleDataFilePath = selectedFile.getAbsolutePath() + "/js/ScheduleData.json";
            String projectDataFilePath = selectedFile.getAbsolutePath() + "/js/ProjectsData.json";
            //    copy(choose, selectedFile);
            ObservableList<CourseWorkspace.CourseTableData> list = cw.getCourseTable().getItems();
            boolean isProjectPage = false;
            System.out.println("hello");
            for (CourseTableData c : list) {
                if (c.getFileName().equalsIgnoreCase("projects.html")) {
                    System.out.println("PROJECT PAGE" + c.getCheck());
                    isProjectPage = c.getCheck();
                }
            }

            if (isProjectPage) {
                choose = new File("../CSE308Tester/public_html");
                FileUtils.copyDirectory(choose, selectedFile);
                exportTAData(data, officeHoursGridFilePath);
                exportRecitationData(data, recitationDataFilePath);
                exportScheduleData(data, scheduleDataFilePath);
                exportProjectData(data, projectDataFilePath);
                exportTeamAndStudentsData(data, teamsAndStudentsFilePath);
            }
            else{
                FileUtils.copyDirectory(choose, selectedFile);
                exportTAData(data, officeHoursGridFilePath);
                exportRecitationData(data, recitationDataFilePath);
                exportScheduleData(data, scheduleDataFilePath);
            }
            

            //File choose2 = new File("../CSE308Tester/public_html");
            //FileUtils.copyDirectory(choose2, selectedFile);
            //officeHoursGridFilePath = selectedFile.getAbsoluteFile() + "/js/OfficeHoursGridData.json";
            //teamsAndStudentsFilePath = selectedFile.getAbsolutePath() + "./js/TeamsAndStudents.json";
            //recitationDataFilePath = selectedFile.getAbsolutePath() + "/js/RecitationsData.json";
            //scheduleDataFilePath = selectedFile.getAbsolutePath() + "/js/ScheduleData.json";
            // projectDataFilePath = selectedFile.getAbsolutePath() + "/js/ProjectsData.json";
            //exportProjectData(data, projectDataFilePath);
            //exportTeamAndStudentsData(data, teamsAndStudentsFilePath);
            //exportTAData(data, officeHoursGridFilePath);
            //exportRecitationData(data, recitationDataFilePath);
            //exportScheduleData(data, scheduleDataFilePath);
            File newDestination = new File(selectedFile.getAbsoluteFile() + "/js/OfficeHoursGridData.json");

            try {
                FileUtils.copyFile(currentWorkFile, newDestination);
            } catch (NullPointerException e) {
                System.out.println("file is not saved");
            }

        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            ioe.printStackTrace();
        }
    }

    // HELPER METHOD FOR EXPORTING TeamAndStudent.JSON 
    public void exportProjectData(AppDataComponent data, String filePath) throws IOException {
        CSGData CSGdataManager = (CSGData) data;
        StudentData stuDataManager = CSGdataManager.getStudentData();

        //-------------- TEAM -----------------------------------------------
        TeamData teamData = CSGdataManager.getTeamData();
        CourseData cData = CSGdataManager.getCourseData();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = teamData.getTeams();
        ObservableList<Student> sList = FXCollections.observableArrayList();
        for (Team t : teams) {

            sList = stuDataManager.getStudentsInTeam(t.getName());
            JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
            for (Student st : sList) {
                // JsonObject studentJson = Json.createObjectBuilder()
                //      .add("", st.getFirstName()).build();
                studentArrayBuilder.add(st.getFirstName() + " " + st.getLastName());
            }
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add("students", studentArrayBuilder)
                    .add("link", t.getLink())
                    .build();

            teamArrayBuilder.add(teamJson);

        }
        JsonArray teamArray = teamArrayBuilder.build();

        //---------------------------------------------------------------------
        JsonObject workJson = Json.createObjectBuilder().add("semester", cData.getCourse().getSemester() + " " + cData.getCourse().getYear())
                .add("projects", teamArray).build();

        JsonArrayBuilder worksArrayBuilder = Json.createArrayBuilder();
        worksArrayBuilder.add(workJson);

        //         .add("semester", cData.getCourse().getSemester() + " " + cData.getCourse().getYear())
        //       .add("projects", teamArray)
        //JsonObject dataManagerJSO2 = Json.createObjectBuilder()
        //     .add("work", dataManagerJSO).build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("work", worksArrayBuilder)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    // HELPER METHOD TO EXPORT SCHEDULE DATA 
    /**
     *
     * @param data
     * @param filePath
     */
    public void exportScheduleData(AppDataComponent data, String filePath) throws FileNotFoundException {
        CSGData CSGdataManager = (CSGData) data;

        ScheduleData sDataManager = CSGdataManager.getScheduleData();
        JsonArrayBuilder sArrayBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> list = sDataManager.getSpecificScheduleList("Holiday");
        for (Schedule s : list) {
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_MONTH, sDataManager.getLocalDate(s.getDate()).getMonthValue() + "")
                    .add(JSON_DAY, sDataManager.getLocalDate(s.getDate()).getDayOfMonth() + "")
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_LINK, s.getLink())
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray holidayArray = sArrayBuilder.build();
        // ---------------------------------------
        list = sDataManager.getSpecificScheduleList("lecture");
        for (Schedule s : list) {
            String link = s.getLink();
            if (link == null) {
                link = "none";
            }
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_MONTH, sDataManager.getLocalDate(s.getDate()).getMonthValue() + "")
                    .add(JSON_DAY, sDataManager.getLocalDate(s.getDate()).getDayOfMonth() + "")
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_TOPIC, s.getTopic())
                    .add(JSON_S_LINK, link)
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray lectureArray = sArrayBuilder.build();
        //--------------------------------------------
        list = sDataManager.getSpecificScheduleList("Reference");
        for (Schedule s : list) {
            String link = s.getLink();
            if (link == null) {
                link = "none";
            }
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_MONTH, sDataManager.getLocalDate(s.getDate()).getMonthValue() + "")
                    .add(JSON_DAY, sDataManager.getLocalDate(s.getDate()).getDayOfMonth() + "")
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_TOPIC, s.getTopic())
                    .add(JSON_S_LINK, link)
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray referenceArray = sArrayBuilder.build();
        //---------------------------------

        list = sDataManager.getSpecificScheduleList("Recitation");
        for (Schedule s : list) {
            String link = s.getLink();
            if (link == null) {
                link = "none";
            }
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_MONTH, sDataManager.getLocalDate(s.getDate()).getMonthValue() + "")
                    .add(JSON_DAY, sDataManager.getLocalDate(s.getDate()).getDayOfMonth() + "")
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_TOPIC, s.getTopic())
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray recitationsArray = sArrayBuilder.build();
        //---------------------------------

        //-------------
        list = sDataManager.getSpecificScheduleList("HW");
        for (Schedule s : list) {
            JsonObject sJson = Json.createObjectBuilder()
                    .add(JSON_S_MONTH, sDataManager.getLocalDate(s.getDate()).getMonthValue() + "")
                    .add(JSON_DAY, sDataManager.getLocalDate(s.getDate()).getDayOfMonth() + "")
                    .add(JSON_S_TITLE, s.getTitle())
                    .add(JSON_S_TOPIC, s.getTopic())
                    .add(JSON_S_LINK, s.getLink())
                    .add(JSON_S_TIME, s.getTime())
                    .add(JSON_S_CRITERIA, s.getCriteria())
                    .build();
            sArrayBuilder.add(sJson);
        }
        JsonArray hwArray = sArrayBuilder.build();
        //---------------------------------

        //-----
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("startingMondayMonth", sDataManager.mondayLocalDate().getMonthValue() + "")
                .add("startingMondayDay", sDataManager.mondayLocalDate().getDayOfMonth() + "")
                .add("endingFridayMonth", sDataManager.fridayLocalDate().getMonthValue() + "")
                .add("endingFridayDay", sDataManager.fridayLocalDate().getDayOfMonth() + "")
                .add("holidays", holidayArray)
                .add("lectures", lectureArray)
                .add("references", referenceArray)
                .add("recitations", recitationsArray)
                .add("hws", hwArray)
                .build();
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    // HELPER METHOD FOR EXPORTING TeamAndStudent.JSON 
    public void exportTeamAndStudentsData(AppDataComponent data, String filePath) throws IOException {
        CSGData CSGdataManager = (CSGData) data;
        //-------------- TEAM -----------------------------------------------
        TeamData teamData = CSGdataManager.getTeamData();
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
        ColorUtils color = new ColorUtils();
        ObservableList<Team> teams = teamData.getTeams();
        for (Team t : teams) {
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add(JSON_TEAM_RED, Integer.parseInt(t.getRed(), 16) + "")
                    .add(JSON_TEAM_GREEN, Integer.parseInt(t.getGreen(), 16) + "")
                    .add(JSON_TEAM_BLUE, Integer.parseInt(t.getBlue(), 16) + "")
                    .add(JSON_TEAM_TEXTCOLOR, color.getColorNameFromHex(Integer.parseInt(t.getTextColor().substring(1), 16)))
                    .build();
            teamArrayBuilder.add(teamJson);
        }
        JsonArray teamArray = teamArrayBuilder.build();

        //----------------------------------------------------------------------
        // ------------------ STUDENT ------------------------------------------
        StudentData stuDataManager = CSGdataManager.getStudentData();
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = stuDataManager.getStudents();
        for (Student s : students) {
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_STU_LASTNAME, s.getLastName())
                    .add(JSON_STU_FIRSTNAME, s.getFirstName())
                    .add(JSON_STU_TEAM, s.getTeam())
                    .add(JSON_STU_ROLE, s.getRole())
                    .build();
            studentArrayBuilder.add(studentJson);

        }
        JsonArray studentArray = studentArrayBuilder.build();
        //---------------------------------------------------------------------- 
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_TEAM, teamArray)
                .add(JSON_STUDENT, studentArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    // HELPER METHOD FOR EXPORTING RECITATION DATA 
    public void exportRecitationData(AppDataComponent data, String filePath) throws IOException {
        CSGData csgDataManager = (CSGData) data;
        RecitationData dataManager = csgDataManager.getRecitationData();
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitation = dataManager.getRecitation();
        for (Recitation rec : recitation) {
            JsonObject recJson = Json.createObjectBuilder()
                    .add(JSON_REC_SECTION, "<strong>" + rec.getSection() + "</strong>")
                    .add(JSON_REC_DAYTIME, rec.getDay_time())
                    .add(JSON_REC_LOCATION, rec.getLocation())
                    .add(JSON_REC_SUPERTA1, rec.getTa_1())
                    .add(JSON_REC_SUPERTA2, rec.getTa_2())
                    .build();
            recArrayBuilder.add(recJson);

        }
        JsonArray recitationArray = recArrayBuilder.build();

        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_RECITATION, recitationArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    public void exportTAData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA

        CSGData CSGdataManager = (CSGData) data;
        TAData dataManager = CSGdataManager.getTAData();

        // NOW BUILD THE TA JSON OBJCTS TO SAVE
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            taArrayBuilder.add(taJson);
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();

        // NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
        for (TimeSlot ts : officeHours) {
            JsonObject tsJson = Json.createObjectBuilder()
                    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
                    .add(JSON_NAME, ts.getName()).build();
            timeSlotArrayBuilder.add(tsJson);
        }
        JsonArray timeSlotsArray = timeSlotArrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_START_HOUR, "" + dataManager.getStartHour())
                .add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    //**************************************************************************
    //**************************************************************************
    //************************* FOR TESTING ************************************
    //**************************************************************************
    public boolean loadDataForTest(AppDataComponent data, String filePath, String expectedFilePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        boolean result = true;
        CSGData CSGdataManager = (CSGData) data;
        TAData dataManager = CSGdataManager.getTAData();
        RecitationData recDataManager = CSGdataManager.getRecitationData();
        CourseData courseDataManager = CSGdataManager.getCourseData();
        ScheduleData sDataManager = CSGdataManager.getScheduleData();

        //
        RecitationData ErecDataManager = CSGdataManager.getRecitationData();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
        JsonObject expectedjson = loadJSONFile(expectedFilePath);

        // LOAD THE START AND END HOURS
        String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        String EstartHour = expectedjson.getString(JSON_START_HOUR);
        String EendHour = expectedjson.getString(JSON_END_HOUR);
        //startHour.equalsIgnoreCase(EstartHour); 
        if (!startHour.equals(EstartHour)) {
            result = false;
        }
        if (!endHour.equals(EendHour)) {
            result = false;
        }
        //dataManager.initHours(startHour, endHour);

        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        //app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        JsonArray EjsonTAArray = expectedjson.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean isGrad = jsonTA.getBoolean(JSON_ISGRADLABEL);
            //
            JsonObject EjsonTA = EjsonTAArray.getJsonObject(i);
            String Ename = EjsonTA.getString(JSON_NAME);
            String Eemail = EjsonTA.getString(JSON_EMAIL);
            Boolean EisGrad = EjsonTA.getBoolean(JSON_ISGRADLABEL);
            if (!name.equals(Ename)) {
                result = false;
            }
            if (!email.equals(Eemail)) {
                result = false;
            }
            if (!isGrad.equals(EisGrad)) {
                result = false;
            }
            //dataManager.addTA(name, email, isGrad);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        JsonArray EjsonOfficeHoursArray = expectedjson.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            //dataManager.addOfficeHoursReservation(day, time, name);
            JsonObject EjsonOfficeHours = EjsonOfficeHoursArray.getJsonObject(i);
            String Eday = EjsonOfficeHours.getString(JSON_DAY);
            String Etime = EjsonOfficeHours.getString(JSON_TIME);
            String Ename = EjsonOfficeHours.getString(JSON_NAME);
            if (!day.equals(Eday)) {
                result = false;
            }
            if (!time.equals(Etime)) {
                result = false;
            }
            if (!name.equals(Ename)) {
                result = false;
            }
        }

        // RECITATION DATA STUFF -----------------------------------------------  //NOT TO SELF: ONLY NEEDED IF STATEMETNS FOR THE ONES THAT RELIED ON WORKSPACE
        JsonArray jsonRecArray = json.getJsonArray(JSON_RECITATION);             // FOR THE OTHER ONES ie recitation I could have created two recitation data and get the list
        JsonArray EjsonRecArray = expectedjson.getJsonArray(JSON_RECITATION);    // and compare them. Next Time Chnage it to this 

        for (int i = 0; i < jsonRecArray.size(); i++) {
            JsonObject jsonRec = jsonRecArray.getJsonObject(i);
            String section = jsonRec.getString(JSON_REC_SECTION);
            String instructor = jsonRec.getString(JSON_REC_INSTRUCTOR);
            String dayTime = jsonRec.getString(JSON_REC_DAYTIME);
            String location = jsonRec.getString(JSON_REC_LOCATION);
            String superTA1 = jsonRec.getString(JSON_REC_SUPERTA1);
            String superTA2 = jsonRec.getString(JSON_REC_SUPERTA2);
            //  recDataManager.addRecitation(section, instructor, dayTime, location, superTA1, superTA2);
            JsonObject EjsonRec = EjsonRecArray.getJsonObject(i);
            String Esection = EjsonRec.getString(JSON_REC_SECTION);
            String Einstructor = EjsonRec.getString(JSON_REC_INSTRUCTOR);
            String EdayTime = EjsonRec.getString(JSON_REC_DAYTIME);
            String Elocation = EjsonRec.getString(JSON_REC_LOCATION);
            String EsuperTA1 = EjsonRec.getString(JSON_REC_SUPERTA1);
            String EsuperTA2 = EjsonRec.getString(JSON_REC_SUPERTA2);
            // ErecDataManager.addRecitation(Esection, Einstructor, EdayTime, Elocation, EsuperTA1, EsuperTA2);
            //if(!recDataManager.getRecitation().equals(ErecDataManager.getRecitation()))result=false; 
            //System.out.print("RECITATION ");
            //System.out.println(recDataManager.getRecitation().equals(ErecDataManager.getRecitation()));
            if (!section.equals(Esection)) {
                result = false;
            }
            if (!instructor.equals(Einstructor)) {
                result = false;
            }
            if (!dayTime.equals(EdayTime)) {
                result = false;
            }
            if (!location.equals(Elocation)) {
                result = false;
            }
            if (!superTA1.equals(EsuperTA1)) {
                result = false;
            }
            if (!superTA2.equals(EsuperTA2)) {
                result = false;
            }

        }

        //SCHEDULE DATA LOADING-------------------------------------------------
        JsonArray jsonSArray = json.getJsonArray(JSON_SCHEDULE);
        JsonArray EjsonSArray = expectedjson.getJsonArray(JSON_SCHEDULE);

        for (int i = 0; i < jsonSArray.size(); i++) {
            JsonObject jsonSchedule = jsonSArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_S_TYPE);
            String date = jsonSchedule.getString(JSON_S_DATE);
            String time = jsonSchedule.getString(JSON_S_TIME);
            String title = jsonSchedule.getString(JSON_S_TITLE);
            String topic = jsonSchedule.getString(JSON_S_TOPIC);
            String link = jsonSchedule.getString(JSON_S_LINK);
            String criteria = jsonSchedule.getString(JSON_S_CRITERIA);
            //sDataManager.addSchedule(type, date, time, title, topic, link, criteria);
            JsonObject EjsonSchedule = EjsonSArray.getJsonObject(i);
            String Etype = EjsonSchedule.getString(JSON_S_TYPE);
            String Edate = EjsonSchedule.getString(JSON_S_DATE);
            String Etime = EjsonSchedule.getString(JSON_S_TIME);
            String Etitle = EjsonSchedule.getString(JSON_S_TITLE);
            String Etopic = EjsonSchedule.getString(JSON_S_TOPIC);
            String Elink = EjsonSchedule.getString(JSON_S_LINK);
            String Ecriteria = EjsonSchedule.getString(JSON_S_CRITERIA);
            if (!type.equals(Etype)) {
                result = false;
            }
            if (!date.equals(Edate)) {
                result = false;
            }
            if (!time.equals(Etime)) {
                result = false;
            }
            if (!title.equals(Etitle)) {
                result = false;
            }
            if (!topic.equals(Etopic)) {
                result = false;
            }
            if (!link.equals(Elink)) {
                result = false;
            }
            if (!criteria.equals(Ecriteria)) {
                result = false;
            }

        }
        //TEAM DATA LOADING ----------------------------------------------------
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAM);
        JsonArray EjsonTeamArray = expectedjson.getJsonArray(JSON_TEAM);

        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String teamName = jsonTeam.getString(JSON_TEAM_NAME);
            String teamRed = jsonTeam.getString(JSON_TEAM_RED);
            String teamGreen = jsonTeam.getString(JSON_TEAM_GREEN);
            String teamBlue = jsonTeam.getString(JSON_TEAM_BLUE);
            String link = jsonTeam.getString(JSON_TEAM_LINK);
            String textColor = jsonTeam.getString(JSON_TEAM_TEXTCOLOR);

            // teamDataManager.addTeam(teamName, teamRed, teamGreen, teamBlue,textColor, link);
            JsonObject EjsonTeam = EjsonTeamArray.getJsonObject(i);
            String EteamName = EjsonTeam.getString(JSON_TEAM_NAME);
            String EteamRed = EjsonTeam.getString(JSON_TEAM_RED);
            String EteamGreen = EjsonTeam.getString(JSON_TEAM_GREEN);
            String EteamBlue = EjsonTeam.getString(JSON_TEAM_BLUE);
            String Elink = EjsonTeam.getString(JSON_TEAM_LINK);
            String EtextColor = EjsonTeam.getString(JSON_TEAM_TEXTCOLOR);
            if (!teamName.equals(EteamName)) {
                result = false;
            }
            if (!teamRed.equals(EteamRed)) {
                result = false;
            }
            if (!teamGreen.equals(EteamGreen)) {
                result = false;
            }
            if (!teamBlue.equals(EteamBlue)) {
                result = false;
            }
            if (!link.equals(Elink)) {
                result = false;
            }
            if (!textColor.equals(EtextColor)) {
                result = false;
            }
        }

        //----------------------------------------------------------------------
        //STUDENT DATA LOADING -------------------------------------------------
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENT);
        JsonArray EjsonStudentArray = expectedjson.getJsonArray(JSON_STUDENT);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String studentFirstName = jsonStudent.getString(JSON_STU_FIRSTNAME);
            String studentLastName = jsonStudent.getString(JSON_STU_LASTNAME);
            String studentTeam = jsonStudent.getString(JSON_STU_TEAM);
            String studentRole = jsonStudent.getString(JSON_STU_ROLE);

            // studentDataManager.addStudent(studentFirstName, studentLastName, studentTeam, studentRole);
            JsonObject EjsonStudent = EjsonStudentArray.getJsonObject(i);
            String EstudentFirstName = EjsonStudent.getString(JSON_STU_FIRSTNAME);
            String EstudentLastName = EjsonStudent.getString(JSON_STU_LASTNAME);
            String EstudentTeam = EjsonStudent.getString(JSON_STU_TEAM);
            String EstudentRole = EjsonStudent.getString(JSON_STU_ROLE);

            if (!studentFirstName.equals(EstudentFirstName)) {
                result = false;
            }
            if (!studentLastName.equals(EstudentLastName)) {
                result = false;
            }
            if (!studentTeam.equals(EstudentTeam)) {
                result = false;
            }
            if (!studentRole.equals(EstudentRole)) {
                result = false;
            }

        }
        return result;

    }
}
//----------------------------------------------------------------------

