/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.StudentData;
import csg.workspace.CSGWorkspace;
import csg.workspace.ProjectWorkspace;
import java.util.Collections;
import jtps.jTPS_Transaction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author khurr
 */
public class UpdateStudent_Transaction implements jTPS_Transaction {

    private String oldfirstName;
    private String oldlastName;
    private String oldteam;
    private String oldrole;

    private String firstName;
    private String lastName;
    private String team;
    private String role;
    CourseSiteGeneratorApp app;

    public UpdateStudent_Transaction(String oldfirstName, String oldlastName, String oldteam, String oldrole, String firstName, String lastName, String team, String role, CourseSiteGeneratorApp app) {
        this.oldfirstName = oldfirstName;
        this.oldlastName = oldlastName;
        this.oldteam = oldteam;
        this.oldrole = oldrole;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.role = role;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        StudentData data = csgData.getStudentData();
        data.updateStudent(oldfirstName, oldlastName, firstName, lastName, team, role);

        Collections.sort(workspace.studentTable.getItems());
        workspace.studentTable.refresh();
        workspace.firstNameTF.setText(firstName);
        workspace.lastNameTF.setText(lastName);
        workspace.teamComboBox.setValue(team);
        workspace.roleTF.setText(role);
    }

    @Override
    public void undoTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        StudentData data = csgData.getStudentData();
        data.updateStudent(firstName, lastName, oldfirstName, oldlastName, oldteam, oldrole);
        Collections.sort(workspace.studentTable.getItems());
        workspace.studentTable.refresh();
        workspace.studentTable.refresh();
        workspace.firstNameTF.setText(oldfirstName);
        workspace.lastNameTF.setText(oldlastName);
        workspace.teamComboBox.setValue(oldteam);
        workspace.roleTF.setText(oldrole);

    }

}
