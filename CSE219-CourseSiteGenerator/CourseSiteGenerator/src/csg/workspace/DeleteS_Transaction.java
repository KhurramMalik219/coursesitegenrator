/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.ScheduleData;
import java.util.Collections;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class DeleteS_Transaction implements jTPS_Transaction {

    private final String type;
    private final String time;
    private final String title;
    private final String topic;
    private final String link;
    private final String criteria;
    private final String date;
    private final CourseSiteGeneratorApp app;

    public DeleteS_Transaction(String type, String time, String title, String topic, String link, String criteria, String date, CourseSiteGeneratorApp app) {
        this.type = type;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        this.date = date;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        data.removeSchedule(type, date, title);
        workspace.sTable.refresh();
        workspace.typeComboBox.setValue(null);
        workspace.datePicker.setValue(null);
        workspace.timeTF.clear();
        workspace.titleTF.clear();
        workspace.topicTF.clear();
        workspace.linkTF.clear();
        workspace.criteriaTF.clear();
        workspace.addUpdateClearHBox.getChildren().clear();
        workspace.addUpdateClearHBox.getChildren().addAll(workspace.addUpdateButton, workspace.clearButton);
        
    }

    @Override
    public void undoTransaction() {
        CSGData csgData = (CSGData) app.getDataComponent();
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        ScheduleData data = csgData.getScheduleData();
        data.addSchedule(type, date, time, title, topic, link, criteria);
        
        Collections.sort(workspace.sTable.getItems());
        workspace.sTable.refresh();

    }

}
