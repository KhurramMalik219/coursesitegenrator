/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import java.util.Collections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class AddS_Transaction implements jTPS_Transaction {

    private final String type;
    private final String time;
    private final String title;
    private final String topic;
    private final String link;
    private final String criteria;
    private final String date;
    private final CourseSiteGeneratorApp app;

    public AddS_Transaction(String initType, String initTime, String initDate, String initTitle, String initTopic, String initLink, String initCriteria, CourseSiteGeneratorApp initApp) {

        type = initType;
        time = initTime;
        title = initTitle;
        topic = initTopic;
        link = initLink;
        criteria = initCriteria;
        date = initDate;
        app = initApp;

    }

    @Override
    public void doTransaction() {
        CSGData csgData = (CSGData) app.getDataComponent();
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        ScheduleData data = csgData.getScheduleData();
        data.addSchedule(type, date, time, title, topic, link, criteria);
        Collections.sort(workspace.sTable.getItems());
    }

    @Override
    public void undoTransaction() {
        System.out.println("Undo ADd schedule");
        CSGData csgData = (CSGData) app.getDataComponent();
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        ScheduleData data = csgData.getScheduleData();
        data.removeSchedule(type, date, title);
        workspace.typeComboBox.setValue(null);
        workspace.datePicker.setValue(null);
        workspace.timeTF.clear();
        workspace.titleTF.clear();
        workspace.topicTF.clear();
        workspace.linkTF.clear();
        workspace.criteriaTF.clear();
        workspace.addUpdateClearHBox.getChildren().clear();
        workspace.addUpdateClearHBox.getChildren().addAll(workspace.addUpdateButton, workspace.clearButton);

    }

}
