/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.CourseData;
import static csg.workspace.TAController.jTPS;
import djf.ui.AppGUI;
import jtps.jTPS;

/**
 *
 * @author khurr
 */
public class CSGController {
    
    CourseSiteGeneratorApp app;
    static jTPS jTPS = new jTPS();
    
    public CSGController(CourseSiteGeneratorApp initApp) {
        app = initApp;
        
    }

    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }
    
    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

   
    
}
