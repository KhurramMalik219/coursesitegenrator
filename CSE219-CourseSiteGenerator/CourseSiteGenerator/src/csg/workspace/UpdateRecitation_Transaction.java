/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.RecitationData;
import java.time.LocalTime;
import java.util.Collections;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class UpdateRecitation_Transaction implements jTPS_Transaction {

    private String oldsection;
    private String oldinstructor;
    private String oldday;
    private String oldstartTime;
    private String oldendTime;
    private String oldlocation;
    private String oldta1;
    private String oldta2;
    CourseSiteGeneratorApp app;

    private String section;
    private String instructor;
    private String day;
    private String startTime;
    private String endTime;
    private String location;
    private String ta1;
    private String ta2;

    public UpdateRecitation_Transaction(String oldsection, String oldinstructor, String oldday, String oldstartTime, String oldendTime, String oldlocation, 
            String oldta1, String oldta2, CourseSiteGeneratorApp app, String section, String instructor, String day, String startTime, String endTime, String location, String ta1, String ta2) {
        this.oldsection = oldsection;
        this.oldinstructor = oldinstructor;
        this.oldday = oldday;
        this.oldstartTime = oldstartTime;
        this.oldendTime = oldendTime;
        this.oldlocation = oldlocation;
        this.oldta1 = oldta1;
        this.oldta2 = oldta2;
        this.app = app;
        this.section = section;
        this.instructor = instructor;
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
    }

    @Override
    public void doTransaction() {
        CSGWorkspace CSGworkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGworkspace.getRecitationWorkspace();
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData data = CSGdata.getRecitationData();
        data.updateRecitation(oldsection, section, instructor, day, startTime, endTime, location, ta1, ta2);
        Collections.sort(workspace.recTable.getItems());
        workspace.recTable.refresh();
        workspace.sectionTF.setText(section);
        workspace.instructorTF.setText(instructor);
        workspace.dayComboBox.setValue(day);
        workspace.locationTF.setText(location);
        workspace.superTA_1ComboBox.setValue(ta1);
        workspace.superTA_2ComboBox.setValue(ta2);
        workspace.startTimeSpinner.getValueFactory().setValue(LocalTime.parse(startTime));
            workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(endTime));
    }

    @Override
    public void undoTransaction() {
        CSGWorkspace CSGworkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGworkspace.getRecitationWorkspace();
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData data = CSGdata.getRecitationData();
        data.updateRecitation(section, oldsection, oldinstructor, oldday, oldstartTime, oldendTime, oldlocation, oldta1, oldta2);
        Collections.sort(workspace.recTable.getItems());
        workspace.recTable.refresh();
         workspace.sectionTF.setText(oldsection);
        workspace.instructorTF.setText(oldinstructor);
        workspace.dayComboBox.setValue(oldday);
        workspace.locationTF.setText(oldlocation);
        workspace.superTA_1ComboBox.setValue(oldta1);
        workspace.superTA_2ComboBox.setValue(oldta2);
        workspace.startTimeSpinner.getValueFactory().setValue(LocalTime.parse(oldstartTime));
            workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(oldendTime));
        
    }

}
