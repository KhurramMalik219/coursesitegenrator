/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.CourseData;
import static djf.settings.AppPropertyType.EXPORT_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppGUI;
import java.io.File;
import javafx.stage.DirectoryChooser;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class CourseDetailsController {

    CourseSiteGeneratorApp app;

    public CourseDetailsController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    public void saveCourse() {
        System.out.println("SAVE COURSE");
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        CourseWorkspace workspace = csgWorkspace.getCourseWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        CourseData data = csgData.getCourseData();

        String subject = workspace.subjectComboBox.getValue().toString();
        String number = workspace.numberComboBox.getValue().toString();
        String semester = workspace.semesterComboBox.getValue().toString();
        String year = workspace.yearComboBox.getValue().toString();

        String title = workspace.titleTF.getText();
        String instName = workspace.instNameTF.getText();
        String instructHome = workspace.instHomeTF.getText();

        data.getCourse().setSubject(subject);
        data.getCourse().setSemester(semester);
        data.getCourse().setNumber(number);
        data.getCourse().setYear(year);
        data.getCourse().setTitle(title);
        data.getCourse().setInstructHome(instructHome);
        data.getCourse().setInstructName(instName);

        markWorkAsEdited();

    }

    public void handleChangeDirectoryButton() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser fc = new DirectoryChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(EXPORT_TITLE));

        File selectedFile = fc.showDialog(app.getGUI().getWindow());
        System.out.println("SELECTED FILE " + selectedFile);
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        CourseWorkspace workspace = csgWorkspace.getCourseWorkspace();
        workspace.dirLabel.setText(selectedFile.toString());
       // workspace.exportDirLabel.setText(selectedFile.toString());

    }

}
