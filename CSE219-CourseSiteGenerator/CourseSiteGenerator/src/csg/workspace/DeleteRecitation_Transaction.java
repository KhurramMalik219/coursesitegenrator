/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.RecitationData;
import java.util.Collections;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class DeleteRecitation_Transaction implements jTPS_Transaction {

    private String section;
    private String instructor;
    private String day;
    private String startTime;
    private String endTime;
    private String location;
    private String ta1;
    private String ta2;
    CourseSiteGeneratorApp app;

    public DeleteRecitation_Transaction(String section, String instructor, String day, String startTime, String endTime, String location, String ta1, String ta2, CourseSiteGeneratorApp app) {
        this.section = section;
        this.instructor = instructor;
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
        this.app = app;
    }
    

    @Override
    public void doTransaction() {
        CSGWorkspace CSGworkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGworkspace.getRecitationWorkspace();
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData data = CSGdata.getRecitationData();
        data.deleteRecitation(section);
        workspace.recitationClearSelection();
        Collections.sort(data.getRecitation());
        workspace.recTable.refresh();
    }

    @Override
    public void undoTransaction() {
        CSGWorkspace CSGworkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGworkspace.getRecitationWorkspace();
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData data = CSGdata.getRecitationData();
        data.addRecitation(section, instructor, day, startTime, endTime, location, ta1, ta2);
        workspace.recitationClearSelection();
        Collections.sort(data.getRecitation());
        workspace.recTable.refresh();

    }

}
