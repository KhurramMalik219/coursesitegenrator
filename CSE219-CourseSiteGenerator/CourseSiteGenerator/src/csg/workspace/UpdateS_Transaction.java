/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Locale;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class UpdateS_Transaction implements jTPS_Transaction {

    private final String oldType;
    private final String oldDate;
    private final String oldTime;
    private final String oldTitle;
    private final String oldTopic;
    private final String oldLink;
    private final String oldCriteria;
    private final String type;
    private final String time;
    private final String title;
    private final String topic;
    private final String link;
    private final String criteria;
    private final String date;
    private final CourseSiteGeneratorApp app;

    public UpdateS_Transaction(String oldType, String oldDate, String oldTime, String oldTitle, String oldTopic, String oldLink, String oldCriteria, String type, String time, String title, String topic, String link, String criteria, String date, CourseSiteGeneratorApp app) {
        this.oldType = oldType;
        this.oldDate = oldDate;
        this.oldTime = oldTime;
        this.oldTitle = oldTitle;
        this.oldTopic = oldTopic;
        this.oldLink = oldLink;
        this.oldCriteria = oldCriteria;
        this.type = type;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        this.date = date;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        data.updateSchedule(oldDate, type, date, time, title, topic, link, criteria);
        Collections.sort(workspace.sTable.getItems());
        workspace.sTable.refresh();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);
        String dateString = date;
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        workspace.addUpdateClearHBox.getChildren().clear();
        workspace.addUpdateClearHBox.getChildren().addAll(workspace.updateButton,workspace. clearSelectionButton, workspace.deleteButton);
        workspace.typeComboBox.setValue(type);
        workspace.datePicker.setValue(localDate);
        workspace.timeTF.setText(time);
        workspace.titleTF.setText(title);
        workspace.topicTF.setText(topic);
        workspace.linkTF.setText(link);
        workspace.criteriaTF.setText(criteria);

    }

    @Override
    public void undoTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        data.updateSchedule(date, oldType, oldDate, oldTime, oldTitle, oldTopic, oldLink, oldCriteria);
        Collections.sort(workspace.sTable.getItems());
        workspace.sTable.refresh();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);
        String dateString = oldDate;
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        workspace.addUpdateClearHBox.getChildren().clear();
        workspace.addUpdateClearHBox.getChildren().addAll(workspace.updateButton, workspace.clearSelectionButton, workspace.deleteButton);
        workspace.typeComboBox.setValue(oldType);
        workspace.datePicker.setValue(localDate);
        workspace.timeTF.setText(oldTime);
        workspace.titleTF.setText(oldTitle);
        workspace.topicTF.setText(oldTopic);
        workspace.linkTF.setText(oldLink);
        workspace.criteriaTF.setText(oldCriteria);

    }

}
