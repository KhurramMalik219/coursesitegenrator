/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.StudentData;
import java.util.Collections;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class AddStudent_Transaction implements jTPS_Transaction {

    private String firstName;
    private String lastName;
    private String team;
    private String role;
    CourseSiteGeneratorApp app;

    public AddStudent_Transaction(String firstName, String lastName, String team, String role, CourseSiteGeneratorApp initApp) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.role = role;
        app = initApp;
    }

    @Override
    public void doTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        StudentData data = csgData.getStudentData();
        data.addStudent(firstName, lastName, team, role);
        Collections.sort(workspace.studentTable.getItems());
        workspace.studentTable.refresh();
        workspace.studentClear();
    }

    @Override
    public void undoTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        StudentData data = csgData.getStudentData();
        data.deleteStudent(firstName, lastName);
        Collections.sort(workspace.studentTable.getItems());
        workspace.studentTable.refresh();
        workspace.studentClear();

    }

}
