/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorProp.*;
import csg.data.CSGData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class ScheduleController {

    CourseSiteGeneratorApp app;
    static jTPS jTPS = new jTPS();

    public ScheduleController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    private void markWorkAsEdited() {
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    public void handleAddSchedule() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        ComboBox typeCBox = workspace.getTypeComboBox();
        DatePicker datePicker = workspace.getDatePicker();

        TextField timeTF = workspace.getTimeTF();
        TextField titleTF = workspace.getTitleTF();
        TextField topicTF = workspace.getTopicTF();
        TextField linkTF = workspace.getLinkTF();
        TextField criteriaTF = workspace.getCriteriaTF();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
        String date = (datePicker.getValue()).format(formatter);
        System.out.println(date);
        System.out.println(datePicker.getValue());

        if (typeCBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));
        } else if (datePicker.getValue() == null) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_DATE_TITLE), props.getProperty(MISSING_DATE_MESSAGE));
        } else if (titleTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TITLE_TITLE), props.getProperty(MISSING_TITLE_MESSAGE));
        } else if (data.containsSchedule(date)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TITLE_TITLE), "DaTA Already Exist Sir....");

        } else {

            String type = typeCBox.getValue().toString();
            // 

            String time = timeTF.getText();
            String title = titleTF.getText();
            String topic = topicTF.getText();
            String link = linkTF.getText();
            String criteria = criteriaTF.getText();
            jTPS_Transaction transaction = new AddS_Transaction(type, time, date, title, topic, link, criteria, app);
            jTPS.addTransaction(transaction);
            //data.addSchedule(type, date, time, title, topic, link, criteria);
            markWorkAsEdited();

        }

        // System.out.println("something");
    }

    public void handleKeyPress(KeyCode code) {
        if (code == KeyCode.DELETE) {
            CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
            ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
            TableView sTable = workspace.getsTable();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            Object selectedItem = sTable.getSelectionModel().getSelectedItem();

            if (selectedItem != null) {
                Schedule s = (Schedule) selectedItem;
                String type = s.getType();
                String date = s.getDate();
                String title = s.getTitle();
                String time = s.getTime();
                String topic = s.getTopic();
                String link = s.getLink();
                String criteria = s.getCriteria();

                jTPS_Transaction transaction1 = new DeleteS_Transaction(type, time, title, topic, link, criteria, date, app);
                jTPS.addTransaction(transaction1);
                /*CSGData csgData = (CSGData) app.getDataComponent();
                ScheduleData data = csgData.getScheduleData();
                data.removeSchedule(type, date, title);
                workspace.typeComboBox.setValue(null);
                workspace.datePicker.setValue(null);
                workspace.timeTF.clear();
                workspace.titleTF.clear();
                workspace.topicTF.clear();
                workspace.linkTF.clear();
                workspace.criteriaTF.clear();
                workspace.addUpdateClearHBox.getChildren().clear();
                workspace.addUpdateClearHBox.getChildren().addAll(workspace.addUpdateButton, workspace.clearButton);
                 */
                markWorkAsEdited();

            }

        }
    }

    public void handleUpdateSchedule() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();

        TableView sTable = workspace.getsTable();

        Object selectedItem = sTable.getSelectionModel().getSelectedItem();
        Schedule s = (Schedule) selectedItem;
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        //OLD DATA 
        String orgType = s.getType();
        String orgDate = s.getDate();
        String orgTime = s.getTime();
        String orgTitle = s.getTitle();
        String orgTopic = s.getTopic();
        String orgLink = s.getLink();
        String orgCriteria = s.getCriteria();

        // NEW data 
        String type = workspace.typeComboBox.getValue().toString();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
        String date = (workspace.datePicker.getValue()).format(formatter);
        String time = workspace.timeTF.getText();
        String title = workspace.titleTF.getText();
        String topic = workspace.topicTF.getText();
        String link = workspace.linkTF.getText();
        String criteria = workspace.criteriaTF.getText();

        if (workspace.typeComboBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));
        } else if (workspace.datePicker.getValue() == null) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_DATE_TITLE), props.getProperty(MISSING_DATE_MESSAGE));
        } else if (workspace.titleTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TITLE_TITLE), props.getProperty(MISSING_TITLE_MESSAGE));
        } else if (data.containsSchedule(date) && !(orgDate.equals(date))) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TITLE_TITLE), "DaTe Already Exist Sir....\n Please change Date");

        } else {
            if (!orgType.equals(type) || !orgDate.equals(date) || !orgTime.equals(time) || !orgTitle.equals(title) || !orgTopic.equals(topic) || !orgLink.equals(link) || !orgCriteria.equals(criteria)) {
//                data.updateSchedule(orgDate, type, date, time, title, topic, link, criteria);
//                Collections.sort(workspace.sTable.getItems());
//                workspace.sTable.refresh();

                jTPS_Transaction transaction = new UpdateS_Transaction(orgType, orgDate, orgTime, orgTitle, orgTopic, orgLink, orgCriteria, type, time, title,
                        topic, link, criteria, date, app);
                jTPS.addTransaction(transaction);
                markWorkAsEdited();

            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));

            }
        }

    }
    public void handleMondayDatePicker(){
         CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
        String date = (workspace.mondayDatePicker.getValue()).format(formatter);
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        data.setStartMonday(date);
        
        markWorkAsEdited();
        
        
    }
    public void handleFridayDatePicker(){
         CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
        String date = (workspace.fridayDatePicker.getValue()).format(formatter);
        CSGData csgData = (CSGData) app.getDataComponent();
        ScheduleData data = csgData.getScheduleData();
        data.setEndFriday(date);
         markWorkAsEdited();
        
    }

    public void handleScheduleClicked() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ScheduleWorkspace workspace = csgWorkspace.getScheduleWorkspace();

        TableView sTable = workspace.getsTable();

        Object selectedItem = sTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            CSGData CSGData = (CSGData) app.getDataComponent();
            ScheduleData data = CSGData.getScheduleData();
            Schedule sch = (Schedule) selectedItem;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);
            String dateString = sch.getDate();
            System.out.println(dateString);
            LocalDate localDate = LocalDate.parse(dateString, formatter);
            System.out.println("MONTH: "+localDate.getMonthValue());
            System.out.println("DATE "+localDate.getDayOfMonth());
            System.out.println("YEAR " +localDate.getYear());

            workspace.typeComboBox.setValue(sch.getType());
            workspace.datePicker.setValue(localDate);
            workspace.timeTF.setText(sch.getTime());
            workspace.titleTF.setText(sch.getTitle());
            workspace.topicTF.setText(sch.getTopic());
            workspace.linkTF.setText(sch.getLink());
            workspace.criteriaTF.setText(sch.getCriteria());

            // need to fix button formmating, need to make add , clear in one HBox ... 
        }

    }

}
