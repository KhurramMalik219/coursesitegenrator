/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.StudentData;
import csg.data.TeamData;
import java.util.Collections;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class UpdateTeam_Transaction implements jTPS_Transaction {

    private String oldname;
    private String oldlink;
    private String oldredString;
    private String oldgreenString;
    private String oldblueString;
    private String oldcolor;
    private String oldtextColor;

    private String name;
    private String link;
    private String redString;
    private String greenString;
    private String blueString;
    private String color;
    private String textColor;
    CourseSiteGeneratorApp app;

    public UpdateTeam_Transaction(String oldname, String oldlink, String oldredString, String oldgreenString, String oldblueString, String oldcolor, String oldtextColor,
            String name, String link, String redString, String greenString, String blueString, String color, String textColor, CourseSiteGeneratorApp app) {
        this.oldname = oldname;
        this.oldlink = oldlink;
        this.oldredString = oldredString;
        this.oldgreenString = oldgreenString;
        this.oldblueString = oldblueString;
        this.oldcolor = oldcolor;
        this.oldtextColor = oldtextColor;
        this.name = name;
        this.link = link;
        this.redString = redString;
        this.greenString = greenString;
        this.blueString = blueString;
        this.color = color;
        this.textColor = textColor;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        data.updateTeam(name, link, color, redString, greenString, blueString, textColor, oldname);
        Collections.sort(workspace.teamTable.getItems());
        workspace.teamTable.refresh();

        StudentData studentData = csgData.getStudentData();
        studentData.updateTeamName(oldname, name);
        workspace.teamComboBox.getItems().remove(oldname);
        workspace.teamComboBox.getItems().add(name);
        Collections.sort(workspace.teamComboBox.getItems());
        Collections.sort(data.getTeams());
        workspace.teamTable.refresh();
        workspace.studentTable.refresh();
        workspace.nameTF.setText(name);
        workspace.linkTF.setText(link);

        workspace.colorPicker.setValue(Color.valueOf(color));
        workspace.textColorPicker.setValue(Color.valueOf(textColor));
        workspace.studentClearSelection();

    }

    @Override
    public void undoTransaction() {
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        data.updateTeam(oldname, oldlink, oldcolor, oldredString, oldgreenString, oldblueString, oldtextColor, name);
        Collections.sort(workspace.teamTable.getItems());
        workspace.teamTable.refresh();

        StudentData studentData = csgData.getStudentData();
        studentData.updateTeamName(name, oldname);
        workspace.teamComboBox.getItems().remove(name);
        workspace.teamComboBox.getItems().add(oldname);
        Collections.sort(workspace.teamComboBox.getItems());
        Collections.sort(data.getTeams());
        workspace.teamTable.refresh();
        workspace.studentTable.refresh();
         workspace.nameTF.setText(oldname);
        workspace.linkTF.setText(oldlink);

        workspace.colorPicker.setValue(Color.valueOf(oldcolor));
        workspace.textColorPicker.setValue(Color.valueOf(oldtextColor));
         workspace.studentClearSelection();

    }

}
