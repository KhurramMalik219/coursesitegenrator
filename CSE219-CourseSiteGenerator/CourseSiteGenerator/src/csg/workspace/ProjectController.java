/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorProp.*;
import csg.data.CSGData;
import csg.data.Student;
import csg.data.StudentData;
import csg.data.Team;
import csg.data.TeamData;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class ProjectController {

    CourseSiteGeneratorApp app;
    static jTPS jTPS = new jTPS();

    public ProjectController(CourseSiteGeneratorApp initApp) {
        app = initApp;

    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    private void markWorkAsEdited() {
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    public void handleStudentClicked() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TableView studentTable = workspace.getStudentTable();

        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Student st = (Student) selectedItem;
            System.out.println(st.getFirstName());
            workspace.firstNameTF.setText(st.getFirstName());
            workspace.lastNameTF.setText(st.getLastName());
            workspace.teamComboBox.setValue(st.getTeam());
            workspace.roleTF.setText(st.getRole());

        }
    }

    public void handleAddStudent() {

        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TextField firstNameTF = workspace.getFirstNameTF();
        TextField lastNameTF = workspace.getLastNameTF();
        ComboBox teamCBox = workspace.getTeamComboBox();
        TextField roleTF = workspace.getRoleTF();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (firstNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE)); //EIDT STRINGS!!!! 
        } else if (lastNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));

        } else if (teamCBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));

        } else if (roleTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));

        } else {
            String firstName = firstNameTF.getText();
            String lastName = lastNameTF.getText();
            String team = teamCBox.getValue().toString();
            String role = roleTF.getText();
            jTPS_Transaction transaction1 = new AddStudent_Transaction(firstName, lastName, team, role, app);
            jTPS.addTransaction(transaction1);
//            CSGData csgData = (CSGData) app.getDataComponent();
//            StudentData data = csgData.getStudentData();
//            data.addStudent(firstName, lastName, team, role);
//            Collections.sort(workspace.studentTable.getItems());
            markWorkAsEdited();

        }

    }

    public void handleUpdateStudent() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView studentTable = workspace.getStudentTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        Student s = (Student) selectedItem;
        //OLD DATA 
        String orgFirstName = s.getFirstName();
        String orgLastName = s.getLastName();
        String orgTeam = s.getTeam();
        String orgRole = s.getRole();
        //

        //NEW DATA
        String firstName = workspace.firstNameTF.getText();
        String lastName = workspace.lastNameTF.getText();
        String team = workspace.teamComboBox.getValue().toString();
        String role = workspace.roleTF.getText();
        //    

        CSGData csgData = (CSGData) app.getDataComponent();
        StudentData data = csgData.getStudentData();
        if (workspace.firstNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("M1", props.getProperty(MISSING_TYPE_MESSAGE)); //EIDT STRINGS!!!! 
        } else if (workspace.lastNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("M2", props.getProperty(MISSING_TYPE_MESSAGE));

        } else if (workspace.teamComboBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("M3", props.getProperty(MISSING_TYPE_MESSAGE));

        } else if (workspace.roleTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TYPE_TITLE), props.getProperty(MISSING_TYPE_MESSAGE));

        } else if (data.containsStudent(firstName, lastName) && !(firstName.equalsIgnoreCase(orgFirstName) && lastName.equalsIgnoreCase(orgLastName))) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("DATA ALREADY EXIST", props.getProperty(MISSING_TYPE_MESSAGE));
        } else {
            if (!firstName.equalsIgnoreCase(orgFirstName) || !lastName.equalsIgnoreCase(orgLastName) || !team.equalsIgnoreCase(orgTeam) || !role.equalsIgnoreCase(orgRole)) {
                //data.updateStudent(orgFirstName, orgLastName, firstName, lastName, team, role);
                //Collections.sort(workspace.studentTable.getItems());
                //workspace.studentTable.refresh();

                jTPS_Transaction transaction1 = new UpdateStudent_Transaction(orgFirstName, orgLastName, orgTeam, orgRole, firstName, lastName, team, role, app);
                jTPS.addTransaction(transaction1);
                markWorkAsEdited();

            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }

        }

    }

    public void handleAddTeam() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TextField teamNameTF = workspace.getNameTF();
        ColorPicker colorPicker = workspace.getColorPicker();
        ColorPicker textColorPicker = workspace.getTextColorPicker();
        TextField linkTF = workspace.getLinkTF();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();
        /*String hex = String.format("#%02X%02X%02X",
                (int) (colorPicker.getValue().getRed() * 255),
                (int) (colorPicker.getValue().getGreen() * 255),
                (int) (colorPicker.getValue().getBlue() * 255));
        System.out.println(hex);
        int red3 = (int) (colorPicker.getValue().getRed() * 255);
        String redString = Integer.toHexString(red3);
        System.out.println("redString " + redString);
         */
        if (teamNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));

        } else if (linkTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));

        } else if (data.containsTeam(teamNameTF.getText())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Project Controller, DUPLICATE NAME FOUND", props.getProperty(MISSING_TA_NAME_MESSAGE));

        } else {

            String name = teamNameTF.getText();

            int green = (int) (colorPicker.getValue().getGreen() * 255);
            String greenString = Integer.toHexString(green);

            int red = (int) (colorPicker.getValue().getRed() * 255);
            String redString = Integer.toHexString(red);

            int blue = (int) (colorPicker.getValue().getBlue() * 255);
            String blueString = Integer.toHexString(blue);
            String textColor = String.format("#%02X%02X%02X",
                    (int) (textColorPicker.getValue().getRed() * 255),
                    (int) (textColorPicker.getValue().getGreen() * 255),
                    (int) (textColorPicker.getValue().getBlue() * 255));
            String link = linkTF.getText();

            //String hexvalue = String.format("#%02X%02X%02X",
            //    (int) (colorPicker.getValue().getRed() * 255),
            //   (int) (colorPicker.getValue().getGreen() * 255),
            //  (int) (colorPicker.getValue().getBlue() * 255));
            jTPS_Transaction transaction1 = new AddTeam_Transaction(name, link, redString, greenString, blueString, textColor, app);
            jTPS.addTransaction(transaction1);
            //data.addTeam(name, redString, greenString, blueString, textColor, link);
            //Collections.sort(data.getTeams());
            //workspace.teamComboBox.getItems().add(name);
            //Collections.sort(workspace.teamComboBox.getItems());

            markWorkAsEdited();
        }

    }

    public void handleTeamClicked() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TableView teamTable = workspace.getTeamTable();

        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Team t = (Team) selectedItem;

            String teamName = t.getName();
            String link = t.getLink();
            String color = t.getColor();
            String textColor = t.getTextColor();
            // int red = Integer.parseInt(t.getRed(), 16);
            //int green = Integer.parseInt(t.getGreen(),16);
            //int blue = Integer.parseInt(t.getBlue(),16);
            workspace.nameTF.setText(teamName);
            workspace.linkTF.setText(link);
            System.out.println(t.getRed() + " - " + t.getGreen() + " - " + t.getBlue());
            workspace.colorPicker.setValue(Color.valueOf(color));
            workspace.textColorPicker.setValue(Color.valueOf(textColor));

            //System.out.println("COlor VALUE:"+workspace.colorPicker.getValue());
            //String testColorValue = workspace.colorPicker.getValue().toString();
            //System.out.println("TEST COLOR VALUE: " + testColorValue );
            //workspace.textColorPicker.setValue(Color.rgb(red, green, blue));
            //System.out.println(t.getTextColor());
            //workspace.colorPicker.setValue(Color.valueOf(t.getTextColor()));
        }
    }

    public void handleUpdateTeam() {
        System.out.println("team update button pressed");
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();

        TableView teamTable = workspace.getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        Team t = (Team) selectedItem;

        //ORG DATA
        String orgName = t.getName();
        String orgLink = t.getLink();
        String orgColor = t.getColor();
        String orgTextColor = t.getTextColor();

        //-----------------------------------
        TextField teamNameTF = workspace.getNameTF();
        ColorPicker colorPicker = workspace.getColorPicker();
        ColorPicker textColorPicker = workspace.getTextColorPicker();
        TextField linkTF = workspace.getLinkTF();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();

        //NEW DATA 
        String name = teamNameTF.getText();
        String link = linkTF.getText();

        int green = (int) (colorPicker.getValue().getGreen() * 255);
        String greenString = Integer.toHexString(green);

        int red = (int) (colorPicker.getValue().getRed() * 255);
        String redString = Integer.toHexString(red);

        int blue = (int) (colorPicker.getValue().getBlue() * 255);
        String blueString = Integer.toHexString(blue);

        String color = redString + greenString + blueString;
        String textColor = String.format("#%02X%02X%02X",
                (int) (textColorPicker.getValue().getRed() * 255),
                (int) (textColorPicker.getValue().getGreen() * 255),
                (int) (textColorPicker.getValue().getBlue() * 255));

        if (teamNameTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));

        } else if (linkTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));

        } else if (data.containsTeam(name) && !name.equals(selectedItem)) { // NEW Name same as name already in list
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } else {

            /* if (!orgName.equalsIgnoreCase(name) ) { //TEAM NAME CHANGED, EDIT TEAM NAME OF STUDENTS TOO 

                System.out.println("testhere");
                data.updateTeamNameChanged(orgName, name);
                StudentData studentData = csgData.getStudentData();
                studentData.updateTeamName(orgName, name);
                workspace.teamComboBox.getItems().remove(orgName);
                workspace.teamComboBox.getItems().add(name);
                Collections.sort(workspace.teamComboBox.getItems());
                Collections.sort(data.getTeams());
                workspace.teamTable.refresh();
                workspace.studentTable.refresh();
                markWorkAsEdited();
            } else*/ if (!orgName.equalsIgnoreCase(name) || !orgLink.equalsIgnoreCase(link) || !orgColor.equalsIgnoreCase(color) || !orgTextColor.equalsIgnoreCase(textColor)) // if anything else was changed  
            {

                jTPS_Transaction transaction1 = new UpdateTeam_Transaction(orgName, orgLink, t.getRed(), t.getGreen(), t.getBlue(), orgColor,
                        orgTextColor, name, link, redString, greenString, blueString, color, textColor, app);
                jTPS.addTransaction(transaction1);
                //data.updateTeam(link, color, redString, greenString, blueString, textColor, orgName);
                //workspace.teamTable.refresh();
                markWorkAsEdited();
            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }

        }

    }

    public void handleDeleteTeam() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TableView teamTable = workspace.getTeamTable();

        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Team t = (Team) selectedItem;
           // CSGData csgData = (CSGData) app.getDataComponent();
           //TeamData data = csgData.getTeamData();
            jTPS_Transaction transaction1 = new DeleteTeam_Transaction(t.getName(), t.getLink(), t.getRed(),
                    t.getGreen(), t.getBlue(), t.getTextColor(), app);
            jTPS.addTransaction(transaction1);
//            StudentData studentData = csgData.getStudentData();
//            studentData.deleteStudentsInTeam(t.getName());
//            workspace.teamComboBox.getItems().remove(t.getName());
//            data.deleteTeam(t.getName());
//            workspace.nameTF.clear();
//            workspace.linkTF.clear();
//            workspace.colorPicker.setValue(Color.WHITE);
//            workspace.textColorPicker.setValue(Color.WHITE);
//            workspace.teamAddUpdateClearHBox.getChildren().clear();
//            workspace.teamAddUpdateClearHBox.getChildren().addAll(workspace.teamAddUpdateButton, workspace.teamClearButton);
            markWorkAsEdited();

        }

    }

    public void handleDeleteStudent() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        TableView studentTable = workspace.getStudentTable();

        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Student s = (Student) selectedItem;
            //CSGData csgData = (CSGData) app.getDataComponent();
            //StudentData data = csgData.getStudentData();

            //data.deleteStudent(s.getFirstName(), s.getLastName());
            jTPS_Transaction transaction1 = new DeleteStudent_Transaction(s.getFirstName(), s.getLastName(), s.getTeam(), s.getRole(), app);
            jTPS.addTransaction(transaction1);
            markWorkAsEdited();
        }

    }
}
