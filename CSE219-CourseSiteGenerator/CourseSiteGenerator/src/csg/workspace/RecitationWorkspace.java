/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CSGData;
import csg.data.Recitation;
import csg.data.RecitationData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class RecitationWorkspace extends AppWorkspaceComponent {

    CourseSiteGeneratorApp app;

    RecitationController controller;  // ADD LATER! 
    VBox recitationBox;
    VBox addEditBox;
    TimeSpinner startTimeSpinner;
    TimeSpinner endTimeSpinner;
    HBox recitationHeaderBox;
    HBox addEditHeaderBox;
    HBox addUpdateClearHBox;

    Label recitationHeaderLabel;
    Label addEditHeaderLabel;

    Label sectionLabel;
    Label instructorLabel;
    Label dayTimeLabel;
    Label locationLabel;
    Label superTA_1Label;
    Label superTA_2Label;

    Button addUpdateButton;
    Button clearButton;
    Button updateButton;
    Button clearSelectionButton;
    Button deleteButton;

    TextField sectionTF;
    TextField instructorTF;
    TextField dayTimeTF;
    TextField locationTF;
    ComboBox dayComboBox;
    ComboBox superTA_1ComboBox;
    ComboBox superTA_2ComboBox;

    //FOR RECITATION TABLE 
    TableView<Recitation> recTable;
    TableColumn<Recitation, String> sectionColumn;
    TableColumn<Recitation, String> instructorColumn;
    TableColumn<Recitation, String> dayTimeColumn;
    TableColumn<Recitation, String> locationColumn;
    TableColumn<Recitation, String> superTA_1Column;
    TableColumn<Recitation, String> superTA_2Column;

    public RecitationWorkspace(CourseSiteGeneratorApp initApp) {
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        startTimeSpinner = new TimeSpinner();
        endTimeSpinner = new TimeSpinner();

        recitationBox = new VBox(25);
        recitationBox.setPadding(new Insets(0, 0, 0, 50));
        addEditBox = new VBox(20);
        addEditBox.setPadding(new Insets(10, 0, 0, 0));

        recitationHeaderBox = new HBox();
        recitationHeaderBox.setPadding(new Insets(0, 0, 10, 0));
        addEditHeaderBox = new HBox();
        addUpdateClearHBox = new HBox();
        addUpdateClearHBox.setSpacing(10);

        String recitationHeaderText = props.getProperty(CourseSiteGeneratorProp.RHL_HEADER_TEXT.toString());
        String addEditHeaderText = props.getProperty(CourseSiteGeneratorProp.ADL_HEADER_TEXT.toString());

        recitationHeaderLabel = new Label(recitationHeaderText);
        recitationHeaderBox.getChildren().add(recitationHeaderLabel);

        addEditHeaderLabel = new Label(addEditHeaderText);
        addEditHeaderBox.getChildren().add(addEditHeaderLabel);
        addEditBox.getChildren().add(addEditHeaderBox);

        String sectionLabelText = props.getProperty(CourseSiteGeneratorProp.SECTION_LABEL_TEXT.toString());
        String instructorLabelText = props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_LABEL_TEXT.toString());
        String dayTimeLabelText = props.getProperty(CourseSiteGeneratorProp.DAY_TIME_LABEL_TEXT.toString());
        String locationLabelText = props.getProperty(CourseSiteGeneratorProp.LOCATION_LABEL_TEXT.toString());
        String superTA1LabelText = props.getProperty(CourseSiteGeneratorProp.SUPER_TA_LABEL_TEXT.toString());
        String superTA2LabelText = props.getProperty(CourseSiteGeneratorProp.SUPER_TA_LABEL_TEXT.toString());
        String addUpdateButtonText = props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString());
        String updateButtonText = props.getProperty(CourseSiteGeneratorProp.UPDATE_REC_BUTTON_TEXT.toString());
        String clearSelectionButtonText = props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString());
        String deleteButtonText = props.getProperty(CourseSiteGeneratorProp.DELETE_BUTTON_TEXT.toString());

        sectionLabel = new Label(sectionLabelText);
        instructorLabel = new Label(instructorLabelText);
        dayTimeLabel = new Label(dayTimeLabelText);
        locationLabel = new Label(locationLabelText);
        superTA_1Label = new Label(superTA1LabelText);
        superTA_2Label = new Label(superTA2LabelText);

        sectionTF = new TextField();
        sectionTF.setPromptText("R01");
        instructorTF = new TextField();
        dayTimeTF = new TextField();
        locationTF = new TextField();

        superTA_1ComboBox = new ComboBox();
        superTA_2ComboBox = new ComboBox();
        dayComboBox = new ComboBox();
        dayComboBox.getItems().addAll("Mondays", "Tuesdays,", "Wednesdays,", "Thursdays,", "Fridays,");
        addUpdateButton = new Button(addUpdateButtonText);
        clearButton = new Button(clearButtonText);
        updateButton = new Button(updateButtonText);
        clearSelectionButton = new Button(clearSelectionButtonText);
        deleteButton = new Button(deleteButtonText);
        //addUpdateClearHBox.getChildren().addAll(addUpdateButton, clearButton);

        GridPane grid = new GridPane();
        grid.setHgap(50);
        grid.setVgap(30);

        ColumnConstraints labelColumn = new ColumnConstraints();
        labelColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(labelColumn);

        ColumnConstraints inputColumn = new ColumnConstraints();
        inputColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(inputColumn);

        grid.add(sectionLabel, 0, 0);
        grid.add(sectionTF, 1, 0);
        grid.add(instructorLabel, 0, 1);
        grid.add(instructorTF, 1, 1);
        grid.add(new Label("dayLabel"), 0, 2);
        grid.add(dayComboBox, 1, 2);
        grid.add(new Label("Start Time [24hr]"), 0, 3);
        grid.add(startTimeSpinner, 1, 3);
        grid.add(new Label("End Time [24hr]"), 0, 4);
        grid.add(endTimeSpinner, 1, 4);
        grid.add(locationLabel, 0, 5);
        grid.add(locationTF, 1, 5);
        grid.add(superTA_1Label, 0, 6);
        grid.add(superTA_1ComboBox, 1, 6);
        grid.add(superTA_2Label, 0, 7);
        grid.add(superTA_2ComboBox, 1, 7);
        addUpdateClearHBox.getChildren().addAll(addUpdateButton, clearButton);
        //dayTimeTF.setEditable(false);
        //dayTimeTF.setStyle("-fx-background-color: blue");

        recTable = new TableView();

        recTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData recData = CSGdata.getRecitationData();
        ObservableList<Recitation> tableData = recData.getRecitation();
        recTable.setItems(tableData);
        String sectionColumnText = props.getProperty(CourseSiteGeneratorProp.SECTION_COLUMN_TEXT.toString());
        String instructorColumnText = props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_COLUMN_TEXT.toString());
        String dayTimeColumnText = props.getProperty(CourseSiteGeneratorProp.DAY_TIME_COLUMN_TEXT.toString());
        String locationColumnText = props.getProperty(CourseSiteGeneratorProp.LOCATION_COLUMN_TEXT.toString());
        String superTA_1ColumnText = props.getProperty(CourseSiteGeneratorProp.TA_COLUMN_TEXT.toString());
        String superTA_2ColumnText = props.getProperty(CourseSiteGeneratorProp.TA_COLUMN_TEXT.toString());

        sectionColumn = new TableColumn(sectionColumnText);
        instructorColumn = new TableColumn(instructorColumnText);
        dayTimeColumn = new TableColumn(dayTimeColumnText);
        locationColumn = new TableColumn(locationColumnText);
        superTA_1Column = new TableColumn(superTA_1ColumnText);
        superTA_2Column = new TableColumn(superTA_2ColumnText);

        sectionColumn.setCellValueFactory(new PropertyValueFactory<>("section"));
        instructorColumn.setCellValueFactory(new PropertyValueFactory<>("instructor"));
        dayTimeColumn.setCellValueFactory(new PropertyValueFactory<>("day_time"));
        locationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        superTA_1Column.setCellValueFactory(new PropertyValueFactory<>("ta_1"));
        superTA_2Column.setCellValueFactory(new PropertyValueFactory<>("ta_2"));

        recTable.getColumns().add(sectionColumn);
        recTable.getColumns().add(instructorColumn);
        recTable.getColumns().add(dayTimeColumn);
        recTable.getColumns().add(locationColumn);
        recTable.getColumns().add(superTA_1Column);
        recTable.getColumns().add(superTA_2Column);

        recitationBox.getChildren().add(recitationHeaderBox);
        //recitationBox.getChildren().add(recTable);
        recitationBox.getChildren().addAll(grid, addUpdateClearHBox);
        //SplitPane sPane = new SplitPane(recitationBox , new ScrollPane(recTable)); 
        ScrollPane s = new ScrollPane(recTable);
        s.setFitToHeight(true);
        s.setFitToWidth(true);
        HBox hbox2 = new HBox(100);
        hbox2.getChildren().addAll(recitationBox, s);
        
        // recitationBox.setAlignment(Pos.CENTER);
        workspace = new BorderPane();
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(hbox2);
        recitationBox.prefWidthProperty().bind(workspace.widthProperty().multiply(1));
        recTable.prefWidthProperty().bind(workspace.widthProperty().multiply(1.4));
        recTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.0));
        //recTable.minWidth(recTable.getWidth() * 1.5); 
        //recTable.prefWidthProperty().bind(workspace.widthProperty().multiply(06));
        //recTable.setMaxWidth(app.getGUI().getPrimaryStageWidth()*3);
        //recTable.setMinWidth(app.getGUI().getPrimaryStageWidth());
        //s.setMinWidth(app.getGUI().getPrimaryStageWidth() / 1.5);
       // recTable.prefWidthProperty().bind(s.prefWidthProperty().multiply(1));
       sectionColumn.prefWidthProperty().bind(recTable.widthProperty().divide(6));
       instructorColumn.prefWidthProperty().bind(recTable.widthProperty().divide(6));
       dayTimeColumn.prefWidthProperty().bind(recTable.widthProperty().divide(6));
       locationColumn.prefWidthProperty().bind(recTable.widthProperty().divide(6));
       superTA_1Column.prefWidthProperty().bind(recTable.widthProperty().divide(6));
       superTA_2Column.prefWidthProperty().bind(recTable.widthProperty().divide(6));

        //-----------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------
        //EVENT HANDLING 
        controller = new RecitationController(app);

        addUpdateButton.setOnAction(e -> {
            controller.handleAddRecitation();

        });

        recTable.setOnMousePressed(e -> {

            controller.handleRecitationClicked(workspace);
            System.out.println("Recitation Clicked (test code, RecitationWorkspace");

        });
        clearButton.setOnAction(e -> {
            recitationClear();
        });
        clearSelectionButton.setOnAction(e -> {
            recitationClearSelection();
        });

        deleteButton.setOnAction(e -> {
            controller.handleDelete();
            // recitationClearSelection(); 
        });
        recTable.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DELETE)) {
                controller.handleDelete();
                //recitationClearSelection(); 
            }

        });
        updateButton.setOnAction(e -> {
            controller.handleUpdateRecitation();
        });
        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });
        app.getGUI().getUndoButton().setOnAction(e -> {
            controller.handleReDoTransaction();
        });
        app.getGUI().getRedoButton().setOnAction(e -> {
            controller.handleUndoTransaction();
        });

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
        startTimeSpinner.valueProperty().addListener((obs, oldTime, newTime)
                -> System.out.println(formatter.format(newTime)));
        endTimeSpinner.valueProperty().addListener((obs, oldTime, newTime)
                -> System.out.println(formatter.format(newTime)));

    }

    public void recitationClear() {
        sectionTF.clear();
        instructorTF.clear();
        dayComboBox.setValue(null);
        String resetTime = "00:00";
        startTimeSpinner.getValueFactory().setValue(LocalTime.parse(resetTime));
        endTimeSpinner.getValueFactory().setValue(LocalTime.parse(resetTime));
        // String timeString=workspace.startTimeSpinner.getValueFactory().getValue().toString(); 
        //workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(timeString));
        //workspace.instructorTF.setText(rec.getInstructor());
        // workspace.endTimeSpinner.setValueFactory(workspace.startTimeSpinner.getValueFactory());
        //dayTimeTF.clear();
        locationTF.clear();
        sectionTF.setPromptText("R01");
        superTA_1ComboBox.setValue("");
        superTA_2ComboBox.setValue("");

    }

    public void recitationClearSelection() {
        addUpdateClearHBox.getChildren().clear();
        sectionTF.clear();
        instructorTF.clear();
        dayComboBox.setValue(null);
        String resetTime = "00:00";
        startTimeSpinner.getValueFactory().setValue(LocalTime.parse(resetTime));
        endTimeSpinner.getValueFactory().setValue(LocalTime.parse(resetTime));
        // String timeString=workspace.startTimeSpinner.getValueFactory().getValue().toString(); 
        //workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(timeString));
        //workspace.instructorTF.setText(rec.getInstructor());
        // workspace.endTimeSpinner.setValueFactory(workspace.startTimeSpinner.getValueFactory());
        //dayTimeTF.clear();
        locationTF.clear();
        superTA_1ComboBox.setValue("");
        superTA_2ComboBox.setValue("");
        sectionTF.setPromptText("R01");
        addUpdateClearHBox.getChildren().addAll(addUpdateButton, clearButton);
    }

    @Override
    public void resetWorkspace() {

    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {

    }

    public VBox getRecitationBox() {
        return recitationBox;
    }

    public VBox getAddEditBox() {
        return addEditBox;
    }

    public HBox getRecitationHeaderBox() {
        return recitationHeaderBox;
    }

    public HBox getAddEditHeaderBox() {
        return addEditHeaderBox;
    }

    public HBox getAddUpdateClearHBox() {
        return addUpdateClearHBox;
    }

    public Label getRecitationHeaderLabel() {
        return recitationHeaderLabel;
    }

    public Label getAddEditHeaderLabel() {
        return addEditHeaderLabel;
    }

    public Label getSectionLabel() {
        return sectionLabel;
    }

    public Label getInstructorLabel() {
        return instructorLabel;
    }

    public Label getDayTimeLabel() {
        return dayTimeLabel;
    }

    public Label getLocationLabel() {
        return locationLabel;
    }

    public Label getSuperTA_1Label() {
        return superTA_1Label;
    }

    public Label getSuperTA_2Label() {
        return superTA_2Label;
    }

    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public TextField getSectionTF() {
        return sectionTF;
    }

    public TextField getInstructorTF() {
        return instructorTF;
    }

    public TextField getDayTimeTF() {
        return dayTimeTF;
    }

    public TextField getLocationTF() {
        return locationTF;
    }

    public ComboBox getSuperTA_1ComboBox() {
        return superTA_1ComboBox;
    }

    public ComboBox getSuperTA_2ComboBox() {
        return superTA_2ComboBox;
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public RecitationController getController() {
        return controller;
    }

    public TableView<Recitation> getRecTable() {
        return recTable;
    }

    public TableColumn<Recitation, String> getSectionColumn() {
        return sectionColumn;
    }

    public TableColumn<Recitation, String> getInstructorColumn() {
        return instructorColumn;
    }

    public TableColumn<Recitation, String> getDayTimeColumn() {
        return dayTimeColumn;
    }

    public TableColumn<Recitation, String> getLocationColumn() {
        return locationColumn;
    }

    public TableColumn<Recitation, String> getSuperTA_1Column() {
        return superTA_1Column;
    }

    public TableColumn<Recitation, String> getSuperTA_2Column() {
        return superTA_2Column;
    }

    public Pane getWorkspace() {
        return workspace;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public TimeSpinner getStartTimeSpinner() {
        return startTimeSpinner;
    }

    public TimeSpinner getEndTimeSpinner() {
        return endTimeSpinner;
    }

    public Button getUpdateButton() {
        return updateButton;
    }

    public Button getClearSelectionButton() {
        return clearSelectionButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public ComboBox getDayComboBox() {
        return dayComboBox;
    }

}
