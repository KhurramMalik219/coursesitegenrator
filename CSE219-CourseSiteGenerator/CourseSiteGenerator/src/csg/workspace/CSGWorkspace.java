/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class CSGWorkspace extends AppWorkspaceComponent {

    // ACCESS TO THE APP COMPONENT 
    CourseSiteGeneratorApp app;
    TAWorkspace taWorkspace;
    RecitationWorkspace recitationWorkspace;
    ScheduleWorkspace scheduleWorkspace;
    CourseWorkspace courseWorkspace;
    ProjectWorkspace projectWorkspace;
    CSGController controller;

    VBox mainPane;
    TabPane tabPane;

    public CSGWorkspace(CourseSiteGeneratorApp initApp) {
        // KEEP THIS FOR LATER 
        app = initApp;
        taWorkspace = new TAWorkspace(app);
        recitationWorkspace = new RecitationWorkspace(app);
        scheduleWorkspace = new ScheduleWorkspace(app);
        courseWorkspace = new CourseWorkspace(app);
        projectWorkspace = new ProjectWorkspace(app);

        mainPane = new VBox();
        AnchorPane root = new AnchorPane();
        AnchorPane.setLeftAnchor(mainPane, 0.0);
        AnchorPane.setRightAnchor(mainPane, 0.0);
        mainPane = new VBox();

        //mainPane.setFillWidth(true);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //mainPane.getChildren().add(taWorkspace.getWorkspace()); 
        tabPane = new TabPane();
        Tab courseTab = new Tab(props.getProperty(CourseSiteGeneratorProp.COURSE_TAB_TEXT.toString()));
        Tab taTab = new Tab(props.getProperty(CourseSiteGeneratorProp.TA_DATA_TAB_TEXT.toString()));
        Tab recitationTab = new Tab(props.getProperty(CourseSiteGeneratorProp.RECITATION_TAB_TEXT.toString()));
        Tab scheduleTab = new Tab(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TAB_TEXT.toString()));
        Tab projectTab = new Tab(props.getProperty(CourseSiteGeneratorProp.PROJECT_TAB_TEXT.toString()));

        taTab.setContent(taWorkspace.getWorkspace());
        recitationTab.setContent(recitationWorkspace.getWorkspace());
        scheduleTab.setContent(scheduleWorkspace.getWorkspace());
        courseTab.setContent(courseWorkspace.getWorkspace());
        projectTab.setContent(projectWorkspace.getWorkspace());

        tabPane.getTabs().addAll(courseTab, taTab, recitationTab, scheduleTab, projectTab);
        mainPane.getChildren().add(tabPane);

        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE); // Makes Tabs not able to be closed (idk a better way to word it) 
        //tabPane.setTabMinWidth(mainPane.getWidth());

        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(mainPane);

        //tabPane.pre
        //tabPane.setTabMinWidth(200 );
        System.out.println("TAB PANE VALUE : " + app.getGUI().getPrimaryStageWidth());
        double widthOfScreen = app.getGUI().getPrimaryStageWidth() - 90;
        double tabWidth = widthOfScreen / 5;
        tabPane.setTabMinWidth(tabWidth);
        //  tabPane.setTabMaxWidth(tabWidth);
        controller = new CSGController(app);
        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });
        
    }

    @Override
    public void resetWorkspace() {
        taWorkspace.resetWorkspace();
        recitationWorkspace.resetWorkspace();
        courseWorkspace.resetWorkspace();
        scheduleWorkspace.resetWorkspace();
        projectWorkspace.resetWorkspace();

    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        taWorkspace.reloadWorkspace(dataComponent);
        recitationWorkspace.reloadWorkspace(dataComponent);
        projectWorkspace.reloadWorkspace(dataComponent);
        scheduleWorkspace.reloadWorkspace(dataComponent);
        courseWorkspace.reloadWorkspace(dataComponent);

    }

    public TabPane getTabPane() {
        return tabPane;
    }

    public VBox getMainPane() {
        return mainPane;
    }

    public TAWorkspace getTAWorkspace() {
        return taWorkspace;

    }

    public CourseWorkspace getCourseWorkspace() {
        return courseWorkspace;
    }

    public ScheduleWorkspace getScheduleWorkspace() {
        return scheduleWorkspace;
    }

    public ProjectWorkspace getProjectWorkspace() {
        return projectWorkspace;
    }

    public RecitationWorkspace getRecitationWorkspace() {
        return recitationWorkspace;
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

}
