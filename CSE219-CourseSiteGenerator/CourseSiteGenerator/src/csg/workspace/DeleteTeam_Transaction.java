/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.Student;
import csg.data.StudentData;
import csg.data.TeamData;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class DeleteTeam_Transaction implements jTPS_Transaction {

    private String name;
    private String link;
    private String redString;
    private String greenString;
    private String blueString;
    private String textColor;
    private ObservableList<Student> deletedStudents;
    CourseSiteGeneratorApp app;

    public DeleteTeam_Transaction(String name, String link, String redString, String greenString, String blueString, String textColor, CourseSiteGeneratorApp app) {
        this.name = name;
        this.link = link;
        this.redString = redString;
        this.greenString = greenString;
        this.blueString = blueString;
        this.textColor = textColor;
        this.app = app;
        deletedStudents=FXCollections.observableArrayList();
    }

    @Override
    public void doTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();
        StudentData studentData = csgData.getStudentData();
        deletedStudents=studentData.deleteStudentsInTeam(name);
        workspace.teamComboBox.getItems().remove(name);
        data.deleteTeam(name);
        workspace.nameTF.clear();
        workspace.linkTF.clear();
        workspace.colorPicker.setValue(Color.WHITE);
        workspace.textColorPicker.setValue(Color.WHITE);
        workspace.teamAddUpdateClearHBox.getChildren().clear();
        workspace.teamAddUpdateClearHBox.getChildren().addAll(workspace.teamAddUpdateButton, workspace.teamClearButton);
    }

    @Override
    public void undoTransaction() {
        CSGWorkspace csgWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        ProjectWorkspace workspace = csgWorkspace.getProjectWorkspace();
        CSGData csgData = (CSGData) app.getDataComponent();
        TeamData data = csgData.getTeamData();
        data.addTeam(name, redString, greenString, blueString, textColor, link);
        Collections.sort(data.getTeams());
        workspace.teamComboBox.getItems().add(name);
        Collections.sort(workspace.teamComboBox.getItems());
        StudentData studentData = csgData.getStudentData();
        studentData.getStudents().addAll(deletedStudents);
        Collections.sort(studentData.getStudents());
       
    }

}
