/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CSGData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Collections;
;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import javafx.scene.control.DateCell;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;

/**
 *
 * @author khurr
 */


public class ScheduleWorkspace extends AppWorkspaceComponent {

    CourseSiteGeneratorApp app;
    //ScheduleController controller; 

    Label scheduleHeaderLabel;
    HBox scheudleHeaderHBox;
    HBox calendarHeaderHBox;
    Label calendarHeaderLabel;
    HBox scheudleItemsHeaderBox;
    VBox scheduleVBox;
    HBox calendarHBox;
    HBox addUpdateClearHBox;

    Label scheduleItemsHeaderLabel;

    DatePicker mondayDatePicker;
    DatePicker fridayDatePicker;
    DatePicker datePicker;

    Label startingMondayLabel;
    Label endingFridayLabel;

    TableView<Schedule> sTable;
    TableColumn<Schedule, String> typeColumn;
    TableColumn<Schedule, String> dateColumn;
    TableColumn<Schedule, String> titleColumn;
    TableColumn<Schedule, String> topicColumn;

    Label addEditLabel;
    Label typeLabel;
    Label dateLabel;
    Label timeLabel;
    Label titleLabel;
    Label topicLabel;
    Label linkLabel;
    Label criteriaLabel;

    ComboBox typeComboBox;
    TextField timeTF;
    TextField titleTF;
    TextField topicTF;
    TextField linkTF;
    TextField criteriaTF;

    Button addUpdateButton;
    Button clearButton;
    Button updateButton;
    Button clearSelectionButton;
    Button deleteButton;

    HBox tile;
    VBox leftVBox;
    VBox rightVBox;

    public ScheduleWorkspace(CourseSiteGeneratorApp initApp) {
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        tile = new HBox(30);
        tile.setPadding(new Insets(5, 0, 0, 30));
        leftVBox = new VBox(30);
        rightVBox = new VBox(35);
        scheduleVBox = new VBox(25);
        scheduleVBox.setPadding(new Insets(0, 0, 0, 50));

        scheudleHeaderHBox = new HBox();
        scheudleHeaderHBox.setPadding(new Insets(30, 0, 0, 0));
        scheudleItemsHeaderBox = new HBox();
        calendarHeaderHBox = new HBox();
        calendarHBox = new HBox(50);
        addUpdateClearHBox = new HBox();

        mondayDatePicker = new DatePicker();
        fridayDatePicker = new DatePicker();
        datePicker = new DatePicker();
        datePicker.setEditable(false);
        mondayDatePicker.setEditable(false);
        fridayDatePicker.setEditable(false);

        mondayDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                if (date.getDayOfWeek() != DayOfWeek.MONDAY) {
                    setDisable(true);
                    setStyle("-fx-background-color: #ffc0cb;");
                }
                //setDisable(empty || date.getDayOfWeek() != DayOfWeek.MONDAY);
                //setStyle("-fx-background-color: #ffc0cb;");
            }
        });
        fridayDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.getDayOfWeek() != DayOfWeek.FRIDAY);
            }
        });

        String scheduleHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.SHL_LABEL_TEXT.toString());
        String scheduleItemsHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.SIHL_LABEL_TEXT.toString());
        String calendarHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.CALENDAR_LABEL_TEXT.toString());
        String startingMonLabelText = props.getProperty(CourseSiteGeneratorProp.STARTING_MONDAY_LABEL_TEXT.toString());
        String endingFriLabelText = props.getProperty(CourseSiteGeneratorProp.ENDING_FRIDAY_LABEL_TEXT.toString());
        String addEditLabelText = props.getProperty(CourseSiteGeneratorProp.ADD_EDIT_LABEL_TEXT.toString());
        String typeLabelText = props.getProperty(CourseSiteGeneratorProp.TYPE_LABEL_TEXT.toString());
        String dateLabelText = props.getProperty(CourseSiteGeneratorProp.DATE_LABEL_TEXT.toString());
        String timeLabelText = props.getProperty(CourseSiteGeneratorProp.TIME_LABEL_TEXT.toString());
        String titleLabelText = props.getProperty(CourseSiteGeneratorProp.TITLE_LABEL_TEXT.toString());
        String topicLabelText = props.getProperty(CourseSiteGeneratorProp.TOPIC_LABEL_TEXT.toString());
        String linkLabelText = props.getProperty(CourseSiteGeneratorProp.LINK_LABEL_TEXT.toString());
        String criteriaLabelText = props.getProperty(CourseSiteGeneratorProp.CRITERIA_LABEL_TEXT.toString());
        String updateButtonText = props.getProperty(CourseSiteGeneratorProp.UPDATE_REC_BUTTON_TEXT.toString());
        String clearSelectionButtonText = props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString());
        String deleteButtonText = props.getProperty(CourseSiteGeneratorProp.DELETE_BUTTON_TEXT.toString());

        scheduleHeaderLabel = new Label(scheduleHeaderLabelText);
        calendarHeaderLabel = new Label(calendarHeaderLabelText);
        scheduleItemsHeaderLabel = new Label(scheduleItemsHeaderLabelText);

        startingMondayLabel = new Label(startingMonLabelText);
        endingFridayLabel = new Label(endingFriLabelText);
        addEditLabel = new Label(addEditLabelText);
        typeLabel = new Label(typeLabelText);
        dateLabel = new Label(dateLabelText);
        timeLabel = new Label(timeLabelText);
        titleLabel = new Label(titleLabelText);
        topicLabel = new Label(topicLabelText);
        linkLabel = new Label(linkLabelText);
        criteriaLabel = new Label(criteriaLabelText);

        timeTF = new TextField();
        titleTF = new TextField();
        topicTF = new TextField();
        linkTF = new TextField();
        criteriaTF = new TextField();

        typeComboBox = new ComboBox();
        typeComboBox.getItems().addAll("Holiday", "Lecture", "HW","Recitation","Reference");
        Collections.sort(typeComboBox.getItems());

        addUpdateButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        updateButton = new Button(updateButtonText);
        clearSelectionButton = new Button(clearSelectionButtonText);
        deleteButton = new Button(deleteButtonText);

        sTable = new TableView();
        sTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CSGData CSGdata = (CSGData) app.getDataComponent();
        ScheduleData sData = CSGdata.getScheduleData();
        ObservableList<Schedule> tableData = sData.getSchedule();
        sTable.setItems(tableData);

        typeColumn = new TableColumn(typeLabelText);
        dateColumn = new TableColumn(dateLabelText);
        titleColumn = new TableColumn(titleLabelText);
        topicColumn = new TableColumn(topicLabelText);

        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        topicColumn.setCellValueFactory(new PropertyValueFactory<>("topic"));

        sTable.getColumns().add(typeColumn);
        sTable.getColumns().add(dateColumn);
        sTable.getColumns().add(titleColumn);
        sTable.getColumns().add(topicColumn);

        scheudleHeaderHBox.getChildren().add(scheduleHeaderLabel);
        calendarHeaderHBox.getChildren().add(calendarHeaderLabel);
        scheudleItemsHeaderBox.getChildren().add(scheduleItemsHeaderLabel);

        HBox calLeft = new HBox(15);
        calLeft.getChildren().add(startingMondayLabel);
        calLeft.getChildren().add(mondayDatePicker);

        HBox calRight = new HBox(15);
        calRight.getChildren().add(endingFridayLabel);
        calRight.getChildren().add(fridayDatePicker);

        calendarHBox.getChildren().add(calLeft);
        calendarHBox.getChildren().add(calRight);

        HBox addEditHBox = new HBox();

        addEditHBox.setPadding(new Insets(50, 0, 0, 0));
        addEditHBox.getChildren().add(addEditLabel);

//        leftVBox.getChildren().add(typeLabel);
//        leftVBox.getChildren().add(dateLabel);
//        leftVBox.getChildren().add(timeLabel);
//        leftVBox.getChildren().add(titleLabel);
//        leftVBox.getChildren().add(topicLabel);
//        leftVBox.getChildren().add(linkLabel);
//        leftVBox.getChildren().add(criteriaLabel);
//        leftVBox.getChildren().add(addUpdateButton);
//
//        rightVBox.getChildren().add(typeComboBox);
//        rightVBox.getChildren().add(datePicker);
//        rightVBox.getChildren().add(timeTF);
//        rightVBox.getChildren().add(titleTF);
//        rightVBox.getChildren().add(topicTF);
//        rightVBox.getChildren().add(linkTF);
//        rightVBox.getChildren().add(criteriaTF);
//        rightVBox.getChildren().add(clearButton);
//        tile.getChildren().add(leftVBox);
//        tile.getChildren().add(rightVBox);
        addUpdateClearHBox.setSpacing(10);
        addUpdateClearHBox.getChildren().addAll(addUpdateButton, clearButton);
        GridPane grid = new GridPane();
        grid.setHgap(50);
        grid.setVgap(30);

        ColumnConstraints labelColumn = new ColumnConstraints();
        labelColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(labelColumn);

        ColumnConstraints inputColumn = new ColumnConstraints();
        inputColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(inputColumn);

        grid.add(typeLabel, 0, 0);
        grid.add(typeComboBox, 1, 0);
        grid.add(dateLabel, 0, 1);
        grid.add(datePicker, 1, 1);
        grid.add(timeLabel, 0, 2);
        grid.add(timeTF, 1, 2);
        grid.add(titleLabel, 0, 3);
        grid.add(titleTF, 1, 3);
        grid.add(topicLabel, 0, 4);
        grid.add(topicTF, 1, 4);
        grid.add(linkLabel, 0, 5);
        grid.add(linkTF, 1, 5);
        grid.add(criteriaLabel, 0, 6);
        grid.add(criteriaTF, 1, 6);
        //grid.add(addUpdateClearHBox, 0, 7);
        //grid.add(addUpdateClearHBox, 0, 7,2,1);

        //grid.add(addUpdateClearHBox, 0, 2,2,1);
        scheduleVBox.getChildren().add(scheudleHeaderHBox);
        scheduleVBox.getChildren().add(calendarHeaderHBox);
        scheduleVBox.getChildren().add(calendarHBox);
        scheduleVBox.getChildren().add(addEditHBox);
        scheduleVBox.getChildren().add(grid);
        scheduleVBox.getChildren().add(addUpdateClearHBox);

        //scheduleVBox.getChildren().add(addEditHBox);
        //scheduleVBox.getChildren().add(tile);
        SplitPane sPane = new SplitPane(scheduleVBox, new ScrollPane(sTable));

        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(sPane);
        // scheduleVBox.setStyle("-fx-background-color: black");  // TEMP
//------------------------------------------------------------------------------------------
        ScheduleController controller = new ScheduleController(app);

        sTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.0));
        sTable.prefWidthProperty().bind(workspace.widthProperty().multiply(0.5));
        titleTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.2));
        timeTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.2));
        topicTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.2));
        linkTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.2));
        criteriaTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.2));
        
        typeColumn.prefWidthProperty().bind(sTable.widthProperty().divide(4));
        dateColumn.prefWidthProperty().bind(sTable.widthProperty().divide(4));
       titleColumn.prefWidthProperty().bind(sTable.widthProperty().divide(4));
       topicColumn.prefWidthProperty().bind(sTable.widthProperty().divide(4));

        /*datePicker.setOnAction(e -> {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US);
            String formattedValue = (datePicker.getValue()).format(formatter);
            System.out.println(formattedValue);
            //LocalDate date = datePicker.getValue();
            //System.err.println("Selected date: " + date);
            //String dateValue = "01 12, 2015";
            //myDatePicker.setValue(LocalDate.parse(dateValue, formatter));
        }); */
        addUpdateButton.setOnAction(e -> {

            controller.handleAddSchedule();
        });
        clearButton.setOnAction(e -> {
            typeComboBox.setValue(null);
            datePicker.setValue(null);
            timeTF.clear();
            titleTF.clear();
            topicTF.clear();
            linkTF.clear();
            criteriaTF.clear();

        });
        sTable.setFocusTraversable(true);
        sTable.setOnKeyPressed(e -> {

        });
        app.getGUI().getUndoButton().setOnAction(e -> {
            controller.handleReDoTransaction();
        });
        app.getGUI().getRedoButton().setOnAction(e -> {
            controller.handleUndoTransaction();
        });
        sTable.setOnMousePressed(e -> {
            addUpdateClearHBox.getChildren().clear();
            addUpdateClearHBox.getChildren().addAll(updateButton, clearSelectionButton, deleteButton);
            System.out.println("Clicked SCHEDULE");
            controller.handleScheduleClicked();
        });
        sTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        clearSelectionButton.setOnAction(e -> {
            typeComboBox.setValue(null);
            datePicker.setValue(null);
            timeTF.clear();
            titleTF.clear();
            topicTF.clear();
            linkTF.clear();
            criteriaTF.clear();
            addUpdateClearHBox.getChildren().clear();
            addUpdateClearHBox.getChildren().addAll(addUpdateButton, clearButton);

        });
        deleteButton.setOnAction(e -> {
            controller.handleKeyPress(KeyCode.DELETE);
        });
        sTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        updateButton.setOnAction(e -> {
            controller.handleUpdateSchedule(); 
        });
        mondayDatePicker.setOnAction(e -> {
            controller.handleMondayDatePicker();
            fridayDatePicker.setValue(null);
            final Callback<DatePicker, DateCell> dayCellFactory
                    = new Callback<DatePicker, DateCell>() {
                @Override
                public DateCell call(final DatePicker datePicker) {
                    return new DateCell() {
                        @Override
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);

                            if (item.isBefore(mondayDatePicker.getValue()) || (item.getDayOfWeek() != DayOfWeek.FRIDAY)) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    };
                }
            };
            fridayDatePicker.setDayCellFactory(dayCellFactory);
        });
        fridayDatePicker.setOnAction(e ->{
            controller.handleFridayDatePicker();
        });
        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });


    }

    @Override
    public void resetWorkspace() {

    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {

    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public Label getScheduleHeaderLabel() {
        return scheduleHeaderLabel;
    }

    public HBox getScheudleHeaderHBox() {
        return scheudleHeaderHBox;
    }

    public HBox getCalendarHeaderHBox() {
        return calendarHeaderHBox;
    }

    public Label getCalendarHeaderLabel() {
        return calendarHeaderLabel;
    }

    public HBox getScheudleItemsHeaderBox() {
        return scheudleItemsHeaderBox;
    }

    public VBox getScheduleVBox() {
        return scheduleVBox;
    }

    public HBox getCalendarHBox() {
        return calendarHBox;
    }

    public Label getScheduleItemsHeaderLabel() {
        return scheduleItemsHeaderLabel;
    }

    public DatePicker getMondayDatePicker() {
        return mondayDatePicker;
    }

    public DatePicker getFridayDatePicker() {
        return fridayDatePicker;
    }

    public DatePicker getDatePicker() {
        return datePicker;
    }

    public Label getStartingMondayLabel() {
        return startingMondayLabel;
    }

    public Label getEndingFridayLabel() {
        return endingFridayLabel;
    }

    public TableView<Schedule> getsTable() {
        return sTable;
    }

    public TableColumn<Schedule, String> getTypeColumn() {
        return typeColumn;
    }

    public TableColumn<Schedule, String> getDateColumn() {
        return dateColumn;
    }

    public TableColumn<Schedule, String> getTitleColumn() {
        return titleColumn;
    }

    public TableColumn<Schedule, String> getTopicColumn() {
        return topicColumn;
    }

    public Label getAddEditLabel() {
        return addEditLabel;
    }

    public Label getTypeLabel() {
        return typeLabel;
    }

    public Label getDateLabel() {
        return dateLabel;
    }

    public Label getTimeLabel() {
        return timeLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getTopicLabel() {
        return topicLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Label getCriteriaLabel() {
        return criteriaLabel;
    }

    public ComboBox getTypeComboBox() {
        return typeComboBox;
    }

    public TextField getTimeTF() {
        return timeTF;
    }

    public TextField getTitleTF() {
        return titleTF;
    }

    public TextField getTopicTF() {
        return topicTF;
    }

    public TextField getLinkTF() {
        return linkTF;
    }

    public TextField getCriteriaTF() {
        return criteriaTF;
    }

    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public HBox getTile() {
        return tile;
    }

    public VBox getLeftVBox() {
        return leftVBox;
    }

    public VBox getRightVBox() {
        return rightVBox;
    }

    public Pane getWorkspace() {
        return workspace;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public HBox getAddUpdateClearHBox() {
        return addUpdateClearHBox;
    }

    public Button getUpdateButton() {
        return updateButton;
    }

    public Button getClearSelectionButton() {
        return clearSelectionButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }
    

}
