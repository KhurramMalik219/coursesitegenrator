/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.Course;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.Arrays;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class CourseWorkspace extends AppWorkspaceComponent {

    CourseSiteGeneratorApp app;
    CourseDetailsController controller;
    VBox courseMainPane;
    Label courseInfoHeaderLabel;
    Label siteTemplateHeaderLabel;
    Label pageStyleHeaderLabel;
    //

    Label subjectLabel;
    Label semesterLabel;
    Label imageLabel;
    Label titleLabel;
    Label instNameLabel;
    Label instHomeLabel;
    Label numberLabel;
    Label yearLabel;
    Label exportDirLabel;
    Label dirLabel;

    Label siteTemplateInfoLabel;
    Label siteTempDirLabel;
    Label sitePageLabel;

    Label bannerImageLabel;
    Label leftFooterLabel;
    Label rightFooterLabel;
    Label styleSheetLabel;
    Label noteLabel;

    TableView<CourseTableData> courseTable;
    TableColumn<CourseTableData, Boolean> useCheckCol;
    TableColumn<CourseTableData, String> navBarColumn;
    TableColumn<CourseTableData, String> fileNameColumn;
    TableColumn<CourseTableData, String> scriptColumn;
    ObservableList<CourseTableData> tableData;

    ComboBox subjectComboBox;
    ComboBox semesterComboBox;
    ComboBox numberComboBox;
    ComboBox yearComboBox;
    ComboBox styleSheetComboBox;

    Button changeExportDirButton;
    Button selectDirButton;
    Button changeBannerButton;
    Button changeLeftFooterButton;
    Button changeRightFooterButton;

    TextField titleTF;
    TextField instNameTF;
    TextField instHomeTF;

    VBox courseInfoPane;
    VBox siteTemplatePane;
    VBox pageStylePane;

    /*-------------------*/
    public CourseWorkspace(CourseSiteGeneratorApp initApp) {
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String courseInfoHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.CIHLT_LABEL_TEXT.toString());
        String siteTemplateHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.STLT_LABEL_TEXT.toString());
        String pageStlyeHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.PSHLT_LABEL_TEXT.toString());

        courseInfoHeaderLabel = new Label(courseInfoHeaderLabelText);
        siteTemplateHeaderLabel = new Label(siteTemplateHeaderLabelText);
        pageStyleHeaderLabel = new Label(pageStlyeHeaderLabelText);

        String subjectLabelText = props.getProperty(CourseSiteGeneratorProp.SUBJECT_LABEL_TEXT.toString());
        String semesterLabelText = props.getProperty(CourseSiteGeneratorProp.SEMESTER_LABEL_TEXT.toString());
        String titleLabelText = props.getProperty(CourseSiteGeneratorProp.TITLE_LABEL_TEXT.toString());
        String instNameLabelText = props.getProperty(CourseSiteGeneratorProp.INST_NAME_LABEL_TEXT.toString());
        String instHomeLabelText = props.getProperty(CourseSiteGeneratorProp.INST_HOME_LABEL_TEXT.toString());
        String exportDirLabelText = props.getProperty(CourseSiteGeneratorProp.EXPORT_LABEL_TEXT.toString());
        String dirLabelText = props.getProperty(CourseSiteGeneratorProp.DIR_LABEL_TEXT.toString());
        String numberLabelText = props.getProperty(CourseSiteGeneratorProp.NUMBER_LABEL_TEXT.toString());
        String yearLabelText = props.getProperty(CourseSiteGeneratorProp.YEAR_LABEL_TEXT.toString());
        String siteTemplateInfoText = props.getProperty(CourseSiteGeneratorProp.STIT_LABEL_TEXT.toString());
        String bannerLabelText = props.getProperty(CourseSiteGeneratorProp.BANNER_LABEL_TEXT.toString());
        String leftFooterLabelText = props.getProperty(CourseSiteGeneratorProp.LEFT_FOOTER_LABEL_TEXT.toString());
        String rightFooterLabelText = props.getProperty(CourseSiteGeneratorProp.RIGHT_FOOTER_LABEL_TEXT.toString());
        String styleSheetLabelText = props.getProperty(CourseSiteGeneratorProp.STYLESHEET_LABEL_TEXT.toString());
        String noteLabelText = props.getProperty(CourseSiteGeneratorProp.NOTE_LABEL_TEXT.toString());
        String winterText = "Winter";
        String springText = "Spring";
        String summerText = "Summer";
        String fallText = "Fall";
        subjectLabel = new Label(subjectLabelText);
        semesterLabel = new Label(semesterLabelText);
        titleLabel = new Label(titleLabelText);
        instNameLabel = new Label(instNameLabelText);
        instHomeLabel = new Label(instHomeLabelText);
        exportDirLabel = new Label(exportDirLabelText);
        dirLabel = new Label(dirLabelText); //dirLabelText
        numberLabel = new Label(numberLabelText);
        yearLabel = new Label(yearLabelText);

        siteTemplateInfoLabel = new Label(siteTemplateInfoText);
        bannerImageLabel = new Label(bannerLabelText);
        leftFooterLabel = new Label(leftFooterLabelText);
        rightFooterLabel = new Label(rightFooterLabelText);
        styleSheetLabel = new Label(styleSheetLabelText);
        noteLabel = new Label(noteLabelText);

        subjectComboBox = new ComboBox();
        semesterComboBox = new ComboBox();
        numberComboBox = new ComboBox();
        yearComboBox = new ComboBox();
        styleSheetComboBox = new ComboBox();

        yearComboBox.getItems().addAll("2017", "2018", "2019", "2020");
        numberComboBox.getItems().addAll("219", "220", "380", "308");
        semesterComboBox.getItems().addAll(winterText, springText, summerText, fallText);
        subjectComboBox.getItems().addAll("CSE", "ISE", "MEC", "BME", "AMS", "MAT");

        changeExportDirButton = new Button("CHANGE");//TO XML FILE
        selectDirButton = new Button("Selectect Template");//TO XML FILE
        changeBannerButton = new Button("CHANGE");//TO XML FILE
        changeLeftFooterButton = new Button("CHANGE");//TO XML FILE
        changeRightFooterButton = new Button("CHANGE");//TO XML FILE

        VBox subSemLabel = new VBox(20);
        subSemLabel.getChildren().add(subjectLabel);
        subSemLabel.getChildren().add(semesterLabel);
        VBox subSemComboBox = new VBox(7);
        subSemComboBox.getChildren().add(subjectComboBox);
        subSemComboBox.getChildren().add(semesterComboBox);
        VBox numYearVbox = new VBox(20);
        numYearVbox.getChildren().add(numberLabel);
        numYearVbox.getChildren().add(yearLabel);
        VBox numyearComboBox = new VBox(7);
        numyearComboBox.getChildren().add(numberComboBox);
        numyearComboBox.getChildren().add(yearComboBox);

        // COURSE INFO PANE
        courseInfoPane = new VBox();
        HBox courseInfoHBox = new HBox();
        courseInfoHBox.getChildren().add(courseInfoHeaderLabel);
        courseInfoHBox.setPadding(new Insets(10, 0, 10, 20));
        courseInfoPane.getChildren().add(courseInfoHBox);
        HBox courseInfoTop1 = new HBox();
        HBox courseInfoTop2 = new HBox();
        courseInfoTop1.setPadding(new Insets(0, 0, 0, 30));

        courseInfoTop1.getChildren().add(subSemLabel);
        courseInfoTop1.getChildren().add(subSemComboBox);
        courseInfoTop2.getChildren().add(numYearVbox);
        courseInfoTop2.getChildren().add(numyearComboBox);

        HBox courseInfoTop = new HBox(600);
        courseInfoTop.setPadding(new Insets(0, 0, 20, 0));
        courseInfoTop.setAlignment(Pos.BASELINE_LEFT);

        courseInfoTop.getChildren().addAll(courseInfoTop1, courseInfoTop2);

        courseInfoPane.getChildren().add(courseInfoTop);

        VBox middleLabel = new VBox(20);
        middleLabel.getChildren().add(titleLabel);
        middleLabel.getChildren().add(instNameLabel);
        middleLabel.getChildren().add(instHomeLabel);
        middleLabel.getChildren().add(exportDirLabel);

        titleTF = new TextField();
        instNameTF = new TextField();
        instHomeTF = new TextField();

        VBox middleComboBox = new VBox(7);
        middleComboBox.getChildren().add(titleTF);
        middleComboBox.getChildren().add(instNameTF);
        middleComboBox.getChildren().add(instHomeTF);

        HBox middleHBox = new HBox(100);
        middleHBox.setPadding(new Insets(0, 0, 0, 30));
        middleHBox.getChildren().add(middleLabel);
        middleHBox.getChildren().add(middleComboBox);

        HBox dirHBox = new HBox(10);

        dirHBox.getChildren().add(dirLabel);
        dirHBox.getChildren().add(changeExportDirButton);
        middleComboBox.getChildren().add(dirHBox);
        courseInfoPane.getChildren().add(middleHBox);
        // COURES INFO PANE END

        //SITE TEMPLATE PANE 
        siteTemplatePane = new VBox(10);
        HBox siteTemplateHBox = new HBox();
        siteTemplateHBox.setPadding(new Insets(50, 0, 0, 0));
        siteTemplateHBox.getChildren().add(siteTemplateHeaderLabel);
        siteTemplatePane.getChildren().add(siteTemplateHBox);
        siteTemplatePane.getChildren().add(siteTemplateInfoLabel);
        siteTempDirLabel = new Label("ADD "); //TO XML FILE
        siteTemplatePane.getChildren().add(siteTempDirLabel);
        siteTemplatePane.getChildren().add(selectDirButton);
        siteTemplatePane.getChildren().add(new Label("SITE PAGEs ")); //TO XML FILE
        siteTemplatePane.setPadding(new Insets(0, 0, 0, 30));

        courseTable = new TableView();
        courseTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        String useColumnText = "check";
        String navBarColumnText = "NavBar Title";
        String fileNameColumnText = "File Name";
        String scriptColumnText = "SCript";

        useCheckCol = new TableColumn(useColumnText);
        navBarColumn = new TableColumn(navBarColumnText);
        fileNameColumn = new TableColumn(fileNameColumnText);
        scriptColumn = new TableColumn(scriptColumnText);
        tableData = FXCollections.observableArrayList(new CourseTableData(false, "Home", "index.html", "HomeBuilder.js"),
                new CourseTableData(false, "Syllabus", "syllabus.html", "SyllabusBuilder.js"),
                new CourseTableData(false, "Schedule", "schedule.html", "ScheduleBuilder.js"),
                new CourseTableData(false, "HWs", "hws.html", "HWsBuilder.js"),
                new CourseTableData(false, "Projects", "projects.html", "ProjectBuilder.js"));
        navBarColumn.setCellValueFactory(
                new PropertyValueFactory<>("navTitle")
        );
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("fileName")
        );
        scriptColumn.setCellValueFactory(
                new PropertyValueFactory<>("script")
        );
        //useCheckCol.setCellValueFactory(new PropertyValueFactory<>("isCheck"));
        useCheckCol.setCellFactory(column -> new CheckBoxTableCell());
        useCheckCol.setCellValueFactory(param -> param.getValue().getIsCheck()) ;

        //useCheckCol.setCellFactory(CheckBoxTableCell.forTableColumn(useCheckCol));
        courseTable.setItems(tableData);

        courseTable.getColumns().add(useCheckCol);
        courseTable.getColumns().add(navBarColumn);
        courseTable.getColumns().add(fileNameColumn);
        courseTable.getColumns().add(scriptColumn);

        courseTable.setEditable(true);

        siteTemplatePane.getChildren().add(courseTable);
        //SITE TEMPLATE PANE END

        //Page Style PANE 
        pageStylePane = new VBox(30);
        pageStylePane.setPadding(new Insets(30, 0, 0, 30));
        pageStylePane.getChildren().add(pageStyleHeaderLabel);
        HBox pageHBox = new HBox(30);

        VBox labelVBox = new VBox(60);
        labelVBox.setPadding(new Insets(20, 20, 0, 30));
        labelVBox.getChildren().add(bannerImageLabel);
        labelVBox.getChildren().add(leftFooterLabel);
        labelVBox.getChildren().add(rightFooterLabel);
        labelVBox.getChildren().add(styleSheetLabel);

        pageHBox.getChildren().add(labelVBox);

        VBox imagesVBox = new VBox();

        imagesVBox.getChildren().add(styleSheetComboBox);  // ADD IMAGSE TO IT 

        VBox changeVBox = new VBox(50);
        changeVBox.setPadding(new Insets(10, 0, 0, 200));
        changeVBox.getChildren().add(changeBannerButton);
        changeVBox.getChildren().add(changeLeftFooterButton);
        changeVBox.getChildren().add(changeRightFooterButton);
        changeVBox.getChildren().add(imagesVBox);
        pageHBox.getChildren().add(changeVBox);

        pageStylePane.getChildren().add(pageHBox);
        pageStylePane.getChildren().add(noteLabel);

        // PAge STyle Pane ENd
        courseMainPane = new VBox();

        courseMainPane.getChildren().add(courseInfoPane);
        courseMainPane.getChildren().add(siteTemplatePane);
        courseMainPane.getChildren().add(pageStylePane);
        ScrollPane sc = new ScrollPane();
        sc.setContent(courseMainPane);
        workspace = new BorderPane();
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(sc);
        sc.setFitToWidth(true);
        //double screenWidth = app.getGUI().getPrimaryStageWidth(); 
        //double width= screenWidth * (2/3); 
        //sc.setMinWidth(width);
        courseTable.setMinWidth(app.getGUI().getPrimaryStageWidth() / 2.5);
        courseTable.setMaxWidth(app.getGUI().getPrimaryStageWidth() / 2.5);
        
        courseTable.minHeight(courseTable.getHeight() / 2);
        useCheckCol.prefWidthProperty().bind(courseTable.widthProperty().divide(8));
        navBarColumn.prefWidthProperty().bind(courseTable.widthProperty().divide(4));
        fileNameColumn.prefWidthProperty().bind(courseTable.widthProperty().divide(4));
        scriptColumn.prefWidthProperty().bind(courseTable.widthProperty().divide(2)); 
                
        

        controller = new CourseDetailsController(app);
        subjectComboBox.setOnAction(e -> {
            controller.saveCourse();

        });
        numberComboBox.setOnAction(e -> {
            controller.saveCourse();
        });
        semesterComboBox.setOnAction(e -> {
            controller.saveCourse();
        });
        yearComboBox.setOnAction(e -> {
            controller.saveCourse();
        });
        titleTF.setOnAction(e -> {
            controller.saveCourse();
        });
        instNameTF.setOnAction(e -> {
            controller.saveCourse();
        });
        instHomeTF.setOnAction(e -> {
            controller.saveCourse();
        });
        changeExportDirButton.setOnAction(x -> {
            System.out.println("Change dir");
            controller.handleChangeDirectoryButton();
        });

    }

    @Override
    public void resetWorkspace() {
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public Label getCourseInfoHeaderLabel() {
        return courseInfoHeaderLabel;
    }

    public Label getSiteTemplateHeaderLabel() {
        return siteTemplateHeaderLabel;
    }

    public Label getPageStyleHeaderLabel() {
        return pageStyleHeaderLabel;
    }

    public Label getSubjectLabel() {
        return subjectLabel;
    }

    public Label getSemesterLabel() {
        return semesterLabel;
    }

    public Label getImageLabel() {
        return bannerImageLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getInstNameLabel() {
        return instNameLabel;
    }

    public Label getInstHomeLabel() {
        return instHomeLabel;
    }

    public Label getNumberLabel() {
        return numberLabel;
    }

    public Label getYearLabel() {
        return yearLabel;
    }

    public Label getExportDirLabel() {
        return exportDirLabel;
    }

    public Label getDirLabel() {
        return dirLabel;
    }

    public Label getSiteTemplateInfoLabel() {
        return siteTemplateInfoLabel;
    }

    public Label getSiteTempDirLabel() {
        return siteTempDirLabel;
    }

    public Label getSitePageLabel() {
        return sitePageLabel;
    }

    public Label getBannerImageLabel() {
        return bannerImageLabel;
    }

    public Label getLeftFooterLabel() {
        return leftFooterLabel;
    }

    public Label getRightFooterLabel() {
        return rightFooterLabel;
    }

    public Label getStyleSheetLabel() {
        return styleSheetLabel;
    }

    public Label getNoteLabel() {
        return noteLabel;
    }

    public TableView<CourseTableData> getCourseTable() {
        return courseTable;
    }

    public TableColumn<CourseTableData, Boolean> getUseCheckCol() {
        return useCheckCol;
    }

    public TableColumn<CourseTableData, String> getNavBarColumn() {
        return navBarColumn;
    }

    public TableColumn<CourseTableData, String> getFileNameColumn() {
        return fileNameColumn;
    }

    public TableColumn<CourseTableData, String> getScriptColumn() {
        return scriptColumn;
    }

    public ComboBox getSubjectComboBox() {
        return subjectComboBox;
    }

    public ComboBox getSemesterComboBox() {
        return semesterComboBox;
    }

    public ComboBox getNumberComboBox() {
        return numberComboBox;
    }

    public ComboBox getYearComboBox() {
        return yearComboBox;
    }

    public ComboBox getStyleSheetComboBox() {
        return styleSheetComboBox;
    }

    public Button getChangeExportDirButton() {
        return changeExportDirButton;
    }

    public Button getSelectDirButton() {
        return selectDirButton;
    }

    public Button getChangeBannerButton() {
        return changeBannerButton;
    }

    public Button getChangeLeftFooterButton() {
        return changeLeftFooterButton;
    }

    public Button getChangeRightFooterButton() {
        return changeRightFooterButton;
    }

    public TextField getTitleTF() {
        return titleTF;
    }

    public TextField getInstNameTF() {
        return instNameTF;
    }

    public TextField getInstHomeTF() {
        return instHomeTF;
    }

    public VBox getCourseInfoPane() {
        return courseInfoPane;
    }

    public VBox getSiteTemplatePane() {
        return siteTemplatePane;
    }

    public VBox getPageStylePane() {
        return pageStylePane;
    }

    public Pane getWorkspace() {
        return workspace;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public VBox getCourseMainPane() {
        return courseMainPane;
    }

    public void setApp(CourseSiteGeneratorApp app) {
        this.app = app;
    }

    public void setCourseMainPane(VBox courseMainPane) {
        this.courseMainPane = courseMainPane;
    }

    public void setCourseInfoHeaderLabel(Label courseInfoHeaderLabel) {
        this.courseInfoHeaderLabel = courseInfoHeaderLabel;
    }

    public void setSiteTemplateHeaderLabel(Label siteTemplateHeaderLabel) {
        this.siteTemplateHeaderLabel = siteTemplateHeaderLabel;
    }

    public void setPageStyleHeaderLabel(Label pageStyleHeaderLabel) {
        this.pageStyleHeaderLabel = pageStyleHeaderLabel;
    }

    public void setSubjectLabel(Label subjectLabel) {
        this.subjectLabel = subjectLabel;
    }

    public void setSemesterLabel(Label semesterLabel) {
        this.semesterLabel = semesterLabel;
    }

    public void setImageLabel(Label imageLabel) {
        this.imageLabel = imageLabel;
    }

    public void setTitleLabel(Label titleLabel) {
        this.titleLabel = titleLabel;
    }

    public void setInstNameLabel(Label instNameLabel) {
        this.instNameLabel = instNameLabel;
    }

    public void setInstHomeLabel(Label instHomeLabel) {
        this.instHomeLabel = instHomeLabel;
    }

    public void setNumberLabel(Label numberLabel) {
        this.numberLabel = numberLabel;
    }

    public void setYearLabel(Label yearLabel) {
        this.yearLabel = yearLabel;
    }

    public void setExportDirLabel(Label exportDirLabel) {
        this.exportDirLabel = exportDirLabel;
    }

    public void setDirLabel(Label dirLabel) {
        this.dirLabel = dirLabel;
    }

    public void setSiteTemplateInfoLabel(Label siteTemplateInfoLabel) {
        this.siteTemplateInfoLabel = siteTemplateInfoLabel;
    }

    public void setSiteTempDirLabel(Label siteTempDirLabel) {
        this.siteTempDirLabel = siteTempDirLabel;
    }

    public void setSitePageLabel(Label sitePageLabel) {
        this.sitePageLabel = sitePageLabel;
    }

    public void setBannerImageLabel(Label bannerImageLabel) {
        this.bannerImageLabel = bannerImageLabel;
    }

    public void setLeftFooterLabel(Label leftFooterLabel) {
        this.leftFooterLabel = leftFooterLabel;
    }

    public void setRightFooterLabel(Label rightFooterLabel) {
        this.rightFooterLabel = rightFooterLabel;
    }

    public void setStyleSheetLabel(Label styleSheetLabel) {
        this.styleSheetLabel = styleSheetLabel;
    }

    public void setNoteLabel(Label noteLabel) {
        this.noteLabel = noteLabel;
    }

    public void setCourseTable(TableView<CourseTableData> courseTable) {
        this.courseTable = courseTable;
    }

    public void setUseCheckCol(TableColumn<CourseTableData, Boolean> useCheckCol) {
        this.useCheckCol = useCheckCol;
    }

    public void setNavBarColumn(TableColumn<CourseTableData, String> navBarColumn) {
        this.navBarColumn = navBarColumn;
    }

    public void setFileNameColumn(TableColumn<CourseTableData, String> fileNameColumn) {
        this.fileNameColumn = fileNameColumn;
    }

    public void setScriptColumn(TableColumn<CourseTableData, String> scriptColumn) {
        this.scriptColumn = scriptColumn;
    }

    public void setSubjectComboBox(ComboBox subjectComboBox) {
        this.subjectComboBox = subjectComboBox;
    }

    public void setSemesterComboBox(ComboBox semesterComboBox) {
        this.semesterComboBox = semesterComboBox;
    }

    public void setNumberComboBox(ComboBox numberComboBox) {
        this.numberComboBox = numberComboBox;
    }

    public void setYearComboBox(ComboBox yearComboBox) {
        this.yearComboBox = yearComboBox;
    }

    public void setStyleSheetComboBox(ComboBox styleSheetComboBox) {
        this.styleSheetComboBox = styleSheetComboBox;
    }

    public void setChangeExportDirButton(Button changeExportDirButton) {
        this.changeExportDirButton = changeExportDirButton;
    }

    public void setSelectDirButton(Button selectDirButton) {
        this.selectDirButton = selectDirButton;
    }

    public void setChangeBannerButton(Button changeBannerButton) {
        this.changeBannerButton = changeBannerButton;
    }

    public void setChangeLeftFooterButton(Button changeLeftFooterButton) {
        this.changeLeftFooterButton = changeLeftFooterButton;
    }

    public void setChangeRightFooterButton(Button changeRightFooterButton) {
        this.changeRightFooterButton = changeRightFooterButton;
    }

    public void setTitleTF(TextField titleTF) {
        this.titleTF = titleTF;
    }

    public void setInstNameTF(TextField instNameTF) {
        this.instNameTF = instNameTF;
    }

    public void setInstHomeTF(TextField instHomeTF) {
        this.instHomeTF = instHomeTF;
    }

    public void setCourseInfoPane(VBox courseInfoPane) {
        this.courseInfoPane = courseInfoPane;
    }

    public void setSiteTemplatePane(VBox siteTemplatePane) {
        this.siteTemplatePane = siteTemplatePane;
    }

    public void setPageStylePane(VBox pageStylePane) {
        this.pageStylePane = pageStylePane;
    }

    public static class CourseTableData {

        public final BooleanProperty check;
        public final StringProperty navTitle;
        public final StringProperty fileName;
        public final StringProperty script;

        public CourseTableData(boolean initCheck, String initNavTile, String initFileName, String initScript) {
            check = new SimpleBooleanProperty(initCheck);
            navTitle = new SimpleStringProperty(initNavTile);
            fileName = new SimpleStringProperty(initFileName);
            script = new SimpleStringProperty(initScript);
        }
        public ObservableBooleanValue getIsCheck() {
        return check;
    }

        public boolean getCheck() {
            return check.get();
        }

        public void setCheck(boolean check) {
            this.check.set(check);
        }

        public String getNavTitle() {
            return navTitle.get();
        }

        public void setNavTitle(String initNavTitle) {
            navTitle.set(initNavTitle);
        }

        public String getFileName() {
            return fileName.get();
        }

        public void setFileName(String initFileName) {
            fileName.set(initFileName);
        }

        public String getScript() {
            return script.get();
        }

        public void setScript(String initScript) {
            script.set(initScript);
        }

    }

    public CourseDetailsController getController() {
        return controller;
    }

    public ObservableList<CourseTableData> getTableData() {
        return tableData;
    }
    

}
