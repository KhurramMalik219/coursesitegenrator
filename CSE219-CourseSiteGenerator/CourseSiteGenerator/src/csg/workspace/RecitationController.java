/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import properties_manager.PropertiesManager;
import static csg.CourseSiteGeneratorProp.*;
import csg.data.CSGData;
import csg.data.Recitation;
import csg.data.RecitationData;
import java.time.LocalTime;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;

/**
 *
 * @author khurr
 */
public class RecitationController {

    CourseSiteGeneratorApp app;
    static jTPS jTPS = new jTPS();

    public RecitationController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    /**
     * This helper to be called every time an edit happens
     */
    private void markWorkAsEdited() {
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This method responds to when a user requests to add a new recitation from
     * the UI NOTE: TF = TextField
     */
    public void handleAddRecitation() {
        CSGWorkspace CSGworkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGworkspace.getRecitationWorkspace();
        TextField sectionTF = workspace.getSectionTF();
        TextField instructorTF = workspace.getInstructorTF();
        //TextField dayTimeTF = workspace.getDayTimeTF();
        ComboBox dayCBox = workspace.getDayComboBox();
        TimeSpinner startTimeSpinner = workspace.getStartTimeSpinner();
        TimeSpinner endTimeSpinner = workspace.getEndTimeSpinner();
        TextField locationTF = workspace.getLocationTF();
        ComboBox superTA1CBox = workspace.getSuperTA_1ComboBox();
        ComboBox superTA2CBox = workspace.getSuperTA_2ComboBox();
        CSGData CSGdata = (CSGData) app.getDataComponent();
        RecitationData data = CSGdata.getRecitationData();

        /**
         * ADD VALIDATION FOR SECTION< AND DAYTIME LATER
         */
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        //Check if empty textFields 
        if (sectionTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SECTION_TITLE), props.getProperty(MISSING_SECTION_MESSAGE));
        } else if (data.containsRecitation(sectionTF.getText())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SECTION_TITLE), "Section ALready EXist ARLReaDY Exist");

        } else if (instructorTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INSTRUCTOR_TITLE), props.getProperty(MISSING_INSTRUCTOR_MESSAGE));
        } /*else if (!dayTimeTF.getText().isEmpty()) { //never true, take out in next version
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_DAYTIME_TITLE), props.getProperty(MISSING_DAYTIME_MESSAGE));
        } */ else if (locationTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_LOCATION_TITLE), props.getProperty(MISSING_LOCATION_MESSAGE));

        } else if (superTA1CBox.getItems().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EMPTY_SUPERTA_TITLE), props.getProperty(EMPTY_SUPERTA_MESSAGE));

        } else if (superTA1CBox.getSelectionModel().isEmpty() && superTA2CBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), props.getProperty(MISSING_SUPERTA_MESSAGE));
        } else if (!sectionTF.getText().matches("^[LRSlrs]\\d{2}")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), "SecTIOn must be in the format of either an R,L or S followed by 2 numbers, ex R03, L12");

        } else if (superTA1CBox.getValue().equals(superTA2CBox.getValue())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), "CaN NoT HaVE SaMe TA1 and ta2");
        } else {
            String section = sectionTF.getText().toUpperCase();
            String instructor = instructorTF.getText();
            //String dayTime = dayTimeTF.getText();
            String day = dayCBox.getValue().toString();
            String startTime = startTimeSpinner.getValueFactory().getValue().toString();
            String endTime = endTimeSpinner.getValueFactory().getValue().toString();
            System.out.println("START TIME" + startTime);
            System.out.println("END TIME" + endTime);
            String location = locationTF.getText();
            String superTA2;
            try {
                superTA2 = superTA2CBox.getValue().toString();
            } catch (NullPointerException e) {
                superTA2 = "";
            }
            String superTA1 = superTA1CBox.getValue().toString();
            jTPS_Transaction transaction1 = new AddRecitation_Transaction(section, instructor, day, startTime, endTime, location, superTA1, superTA2, app);
            jTPS.addTransaction(transaction1);
            markWorkAsEdited();
        }
    }

    public void handleRecitationClicked(Pane pane) {
        CSGWorkspace CSGWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGWorkspace.getRecitationWorkspace();

        TableView recTable = workspace.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            System.out.println("Recitation clikced, test code RecitationController ");
            Recitation rec = (Recitation) selectedItem;
            workspace.sectionTF.setText(rec.getSection());
            // String timeString=workspace.startTimeSpinner.getValueFactory().getValue().toString(); 
            //workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(timeString));
            //workspace.instructorTF.setText(rec.getInstructor());
            // workspace.endTimeSpinner.setValueFactory(workspace.startTimeSpinner.getValueFactory());
            workspace.addUpdateClearHBox.getChildren().clear();
            workspace.addUpdateClearHBox.getChildren().addAll(workspace.updateButton, workspace.clearSelectionButton, workspace.deleteButton);
            workspace.locationTF.setText(rec.getLocation());
            workspace.instructorTF.setText(rec.getInstructor());
            workspace.superTA_1ComboBox.setValue(rec.getTa_1());
            workspace.superTA_2ComboBox.setValue(rec.getTa_2());

            workspace.dayComboBox.setValue(rec.getDay());
            workspace.startTimeSpinner.getValueFactory().setValue(LocalTime.parse(rec.getStartTime()));
            workspace.endTimeSpinner.getValueFactory().setValue(LocalTime.parse(rec.getEndTime()));

        }

    }

    public void handleDelete() {
        // GET THE TABLE
        CSGWorkspace CSworkspace = (CSGWorkspace) app.getWorkspaceComponent();

        RecitationWorkspace workspace = CSworkspace.getRecitationWorkspace();
        TableView recTable = workspace.getRecTable();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Recitation rec = (Recitation) selectedItem;
            String section = rec.getSection();
            jTPS_Transaction transaction1 = new DeleteRecitation_Transaction(section, rec.getInstructor(), rec.getDay(), rec.getStartTime(),
                    rec.getEndTime(), rec.getLocation(), rec.getTa_1(), rec.getTa_2(), app);
            jTPS.addTransaction(transaction1);
            // CSGData CSGdata = (CSGData) app.getDataComponent();
            //RecitationData data = CSGdata.getRecitationData();
            //data.deleteRecitation(section);
            markWorkAsEdited();

        }
    }

    public void handleUpdateRecitation() {
        CSGWorkspace CSGWorkspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace workspace = CSGWorkspace.getRecitationWorkspace();

        TableView recTable = workspace.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Recitation r = (Recitation) selectedItem;

        //OLD ATA
        String orgSection = r.getSection();
        String orgInstructor = r.getInstructor();
        String orgDay = r.getDay();
        String orgStartTime = r.getStartTime();
        String orgEndTime = r.getEndTime();
        String orgLocation = r.getLocation();
        String orgTA1 = r.getTa_1();
        String orgTA2 = r.getTa_2();

        //NEW Data
        String section = workspace.sectionTF.getText();
        String instructor = workspace.instructorTF.getText();
        String day = workspace.dayComboBox.getValue().toString();
        String location = workspace.locationTF.getText();
        String startTime = workspace.startTimeSpinner.getValueFactory().getValue().toString();
        String endTime = workspace.endTimeSpinner.getValueFactory().getValue().toString();
        String ta1 = workspace.superTA_1ComboBox.getValue().toString();
        String ta2 = workspace.superTA_2ComboBox.getValue().toString();
        //---------------------------------------------------------------------------------// 
        CSGData csgData = (CSGData) app.getDataComponent();
        RecitationData data = csgData.getRecitationData();

        if (workspace.sectionTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SECTION_TITLE), props.getProperty(MISSING_SECTION_MESSAGE));
        } else if (data.containsRecitation(workspace.sectionTF.getText()) && !(orgSection.equals(section))) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SECTION_TITLE), "Section ALready EXist ARLReaDY Exist");

        } else if (workspace.instructorTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INSTRUCTOR_TITLE), props.getProperty(MISSING_INSTRUCTOR_MESSAGE));
        } /*else if (!dayTimeTF.getText().isEmpty()) { //never true, take out in next version
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_DAYTIME_TITLE), props.getProperty(MISSING_DAYTIME_MESSAGE));
        } */ else if (workspace.locationTF.getText().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_LOCATION_TITLE), props.getProperty(MISSING_LOCATION_MESSAGE));

        } else if (workspace.superTA_1ComboBox.getItems().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EMPTY_SUPERTA_TITLE), props.getProperty(EMPTY_SUPERTA_MESSAGE));

        } else if (workspace.superTA_1ComboBox.getSelectionModel().isEmpty() && workspace.superTA_2ComboBox.getSelectionModel().isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), props.getProperty(MISSING_SUPERTA_MESSAGE));
        } else if (!workspace.sectionTF.getText().matches("^[LRSlrs]\\d{2}")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), "SecTIOn must be in the format of either an R,L or S followed by 2 numbers, ex R03, L12");

        } else if (workspace.superTA_1ComboBox.getValue().equals(workspace.superTA_2ComboBox.getValue())) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_SUPERTA_TITLE), "CaN NoT HaVE SaMe TA1 and ta2");
        } else {
            if (!section.equals(orgSection) || !instructor.equals(orgInstructor) || !day.equals(orgDay) || !startTime.equals(orgStartTime) || !endTime.equals(orgEndTime)
                    || !location.equals(orgLocation) || !ta1.equals(orgTA1) || !ta2.equals(orgTA2)) {

                jTPS_Transaction transaction1 = new UpdateRecitation_Transaction(orgSection, orgInstructor,
                        orgDay, orgStartTime, orgEndTime, orgLocation, orgTA1, orgTA2,
                        app, section, instructor, day, startTime, endTime, location, ta1, ta2);
                jTPS.addTransaction(transaction1);
                //data.updateRecitation(orgSection, section, instructor, day, startTime, endTime, location, ta1, ta2);
                //Collections.sort(workspace.recTable.getItems());
                //workspace.recTable.refresh();
                markWorkAsEdited();

            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));
            }

        }

    }

//    private boolean validateDate(String date) {
//        //
//        return ((date.matches("Mondays, " + "(?i).\\b(?:(?<twelve>(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[ ][ap]\\.?m\\.?)|(?<twfour>(?:[01]?[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?))\\b.*"))
//                || (date.matches("Tuesdays, " + "(?i).\\b(?:(?<twelve>(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[ ][ap]\\.?m\\.?)|(?<twfour>(?:[01]?[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?))\\b.*")
//                || (date.matches("Wednesdays, " + "(?i).\\b(?:(?<twelve>(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[ ][ap]\\.?m\\.?)|(?<twfour>(?:[01]?[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?))\\b.*"))
//                || (date.matches("Thursdays, " + "(?i).\\b(?:(?<twelve>(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[ ][ap]\\.?m\\.?)|(?<twfour>(?:[01]?[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?))\\b.*"))
//                || (date.matches("Fridays, " + "(?i).\\b(?:(?<twelve>(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[ ][ap]\\.?m\\.?)|(?<twfour>(?:[01]?[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?))\\b.*"))))
//                && ((date.contains("pm")) || date.contains("am"));
//    }
}
