/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import csg.data.TAData;
import csg.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddTA_Transaction implements jTPS_Transaction {

    private String taEmail;
    private String taName;
    private TAData data;
    private Boolean isGrad;
    private CourseSiteGeneratorApp app;

    public AddTA_Transaction(String name, String email, Boolean isGrad, TAData taData, CourseSiteGeneratorApp initApp) {
        taEmail = email;
        taName = name;
        data = taData;
        this.isGrad = isGrad;
        app = initApp;

    }

    @Override
    public void doTransaction() {  //Control Y 
        data.addTA(taName, taEmail, isGrad);
        CSGWorkspace workspace = (CSGWorkspace) app.getWorkspaceComponent();
        RecitationWorkspace w = workspace.getRecitationWorkspace();
        w.getSuperTA_1ComboBox().getItems().add(taName);
        w.getSuperTA_2ComboBox().getItems().add(taName);

        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<TeachingAssistant> taList = data.getTeachingAssistants();
        for (TeachingAssistant ta : taList) {
            if (taName.equals(ta.getName())) {
                taList.remove(ta);
                CSGWorkspace workspace = (CSGWorkspace) app.getWorkspaceComponent();
                RecitationWorkspace w = workspace.getRecitationWorkspace();
                w.getSuperTA_1ComboBox().getItems().remove(taName);
                w.getSuperTA_2ComboBox().getItems().remove(taName); 

                return;
            }

        }
        // data.removeTA(taName);

    }

}
