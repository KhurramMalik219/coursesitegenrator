/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CSGData;

import csg.data.Student;
import csg.data.StudentData;
import csg.data.Team;
import csg.data.TeamData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 *
 * @author khurr
 */
public class ProjectWorkspace extends AppWorkspaceComponent {

    CourseSiteGeneratorApp app;
    ProjectController controller;
    VBox projectMainPane;
    HBox projectHeaderLabelHBox;
    HBox teamHeaderLabelHBox;
    HBox studentHeaderLabelHBox;

    Label projectHeaderLabel;

    VBox teamPane;
    HBox studentPane;
    HBox teamAddUpdateClearHBox;
    HBox studentAddUpdateClearHBox;

    Label teamsHeaderLabel;
    Label studentHeaderLabel;

    Label addEditLabel;
    Label nameLabel;
    Label colorLabel;
    Label textColorLabel;
    Label linkLabel;
    Label firstNameLabel;
    Label lastNameLabel;
    Label teamLabel;
    Label roleLabel;
    
    Label TeamTableLabel;
    Label StudentTableLabel;

    TextField nameTF;
    TextField linkTF;
    TextField firstNameTF;
    TextField lastNameTF;
    TextField roleTF;

    ComboBox teamComboBox;

    Button teamAddUpdateButton;
    Button teamClearButton;
    Button teamClearSelectionButton;
    Button teamDeleteButton;
    Button teamUpdateButton;

    Button studentAddUpdateButton;
    Button studentClearButton;
    Button studentClearSelectionButton;
    Button studentDeleteButton;
    Button studentUpdateButton;

    TableView<Team> teamTable;
    TableColumn<Team, String> nameColumn;
    TableColumn<Team, String> colorColumn;
    TableColumn<Team, String> textColumn;
    TableColumn<Team, String> linkColumn;

    TableView<Student> studentTable;
    TableColumn<Student, String> firstNameColumn;
    TableColumn<Student, String> lastNameColumn;
    TableColumn<Student, String> teamColumn;
    TableColumn<Student, String> roleColumn;

    ColorPicker colorPicker;
    ColorPicker textColorPicker;

    public ProjectWorkspace(CourseSiteGeneratorApp initApp) {
        app = initApp;

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String projectHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.PHLT_LABEL_TEXT.toString());
        String addEditLabelText = props.getProperty(CourseSiteGeneratorProp.ADD_EDIT_LABEL_TEXT.toString());
        String nameLabelText = props.getProperty(CourseSiteGeneratorProp.NAME_LABEL_TEXT.toString());
        String colorLabelText = props.getProperty(CourseSiteGeneratorProp.COLOR_LABEL_TEXT.toString());
        String textColorLabelText = props.getProperty(CourseSiteGeneratorProp.TEXT_COLOR_LABEL_TEXT.toString());
        String linkLabelText = props.getProperty(CourseSiteGeneratorProp.LINK_LABEL_TEXT.toString());
        String studentHeaderLabelText = props.getProperty(CourseSiteGeneratorProp.SHLT_LABEL_TEXT.toString());
        String firstNameLabelText = props.getProperty(CourseSiteGeneratorProp.FIRST_NAME_LABEL_TEXT.toString());
        String lastNameLabelText = props.getProperty(CourseSiteGeneratorProp.LAST_NAME_LABEL_TEXT.toString());
        String teamLabelText = props.getProperty(CourseSiteGeneratorProp.TEAM_LABEL_TEXT.toString());
        String roleLabelText = props.getProperty(CourseSiteGeneratorProp.ROLE_LABEL_TEXT.toString());

        projectHeaderLabel = new Label(projectHeaderLabelText);
        addEditLabel = new Label(addEditLabelText);
        nameLabel = new Label(nameLabelText);
        colorLabel = new Label(colorLabelText);
        textColorLabel = new Label(textColorLabelText);
        linkLabel = new Label(linkLabelText);
        studentHeaderLabel = new Label(studentHeaderLabelText);
        firstNameLabel = new Label(firstNameLabelText);
        lastNameLabel = new Label(lastNameLabelText);
        teamLabel = new Label(teamLabelText);
        roleLabel = new Label(roleLabelText);

        teamTable = new TableView();
        teamTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        studentTable = new TableView();
        studentTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        String nameColText = props.getProperty(CourseSiteGeneratorProp.NAME_COLUMN_TEXT.toString());
        String colorColText = props.getProperty(CourseSiteGeneratorProp.COLOR_COLUMN_TEXT.toString());
        String textColText = props.getProperty(CourseSiteGeneratorProp.TEXT_COLOR_COLUMN_TEXT.toString());
        String linkColText = props.getProperty(CourseSiteGeneratorProp.LINK_COLUMN_TEXT.toString());

        String firstNameColText = props.getProperty(CourseSiteGeneratorProp.FIRST_NAME_COLUMN_TEXT.toString());
        String lastNameColText = props.getProperty(CourseSiteGeneratorProp.LAST_NAME_COLUMN_TEXT.toString());
        String teamColText = props.getProperty(CourseSiteGeneratorProp.TEAM_COLUMN_TEXT.toString());
        String roleColText = props.getProperty(CourseSiteGeneratorProp.ROLE_COLUMN_TEXT.toString());

        nameColumn = new TableColumn(nameColText);
        colorColumn = new TableColumn(colorColText);
        textColumn = new TableColumn(textColText);
        linkColumn = new TableColumn(linkColText);

        firstNameColumn = new TableColumn(firstNameColText);
        lastNameColumn = new TableColumn(lastNameColText);
        teamColumn = new TableColumn(teamColText);
        roleColumn = new TableColumn(roleColText);

        CSGData CSGData = (CSGData) app.getDataComponent();
        TeamData teamData = CSGData.getTeamData();
        StudentData studentData = CSGData.getStudentData();

        ObservableList<Team> teamTableData = teamData.getTeams();
        teamTable.setItems(teamTableData);

        ObservableList<Student> studentTableData = studentData.getStudents();
        studentTable.setItems(studentTableData);

        nameColumn.setCellValueFactory(
                new PropertyValueFactory<>("name")
        );
        colorColumn.setCellValueFactory(
                new PropertyValueFactory<>("color")
        );
        textColumn.setCellValueFactory(
                new PropertyValueFactory<>("textColor")
        );
        linkColumn.setCellValueFactory(
                new PropertyValueFactory<>("link")
        );
        firstNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("firstName")
        );
        lastNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("lastName")
        );
        teamColumn.setCellValueFactory(
                new PropertyValueFactory<>("team")
        );
        roleColumn.setCellValueFactory(
                new PropertyValueFactory<>("role")
        );

        teamTable.getColumns().add(nameColumn);
        teamTable.getColumns().add(colorColumn);
        teamTable.getColumns().add(textColumn);
        teamTable.getColumns().add(linkColumn);

        studentTable.getColumns().add(firstNameColumn);
        studentTable.getColumns().add(lastNameColumn);
        studentTable.getColumns().add(teamColumn);
        studentTable.getColumns().add(roleColumn);

        colorPicker = new ColorPicker();
        textColorPicker = new ColorPicker();
        nameTF = new TextField();
        linkTF = new TextField();
        firstNameTF = new TextField();
        lastNameTF = new TextField();
        roleTF = new TextField();

        teamComboBox = new ComboBox();

        teamAddUpdateButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        teamClearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        studentAddUpdateButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        studentClearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));

        studentDeleteButton = new Button("Delete");
        teamDeleteButton = new Button("Delete");
        studentClearSelectionButton = new Button("clear");
        teamClearSelectionButton = new Button("clear");
        studentUpdateButton = new Button("Update");
        teamUpdateButton = new Button("Update");

        projectHeaderLabelHBox = new HBox();
        teamHeaderLabelHBox = new HBox();
        studentHeaderLabelHBox = new HBox();
        teamPane = new VBox(30);
        studentPane = new HBox(20);

        projectHeaderLabelHBox.getChildren().add(projectHeaderLabel);
        teamHeaderLabelHBox.getChildren().add(new Label("TEAM HEADER LABEL"));
        studentHeaderLabelHBox.getChildren().add(studentHeaderLabel);

        //Team Pane
        //HBox colorPickerHBox = new HBox();
        // colorPickerHBox.getChildren().addAll(colorPicker, textColorPicker);
//        VBox colLabelVBox = new VBox(30);
//        colLabelVBox.getChildren().addAll(nameLabel, colorLabel, linkLabel, teamAddUpdateButton);
//        VBox colInputVBox = new VBox(17);
//        colInputVBox.getChildren().addAll(nameTF, colorPickerHBox, linkTF, teamClearButton);
//        HBox teamsInputHBox = new HBox(20);
//        teamsInputHBox.getChildren().addAll(colLabelVBox, colInputVBox);
        teamAddUpdateClearHBox = new HBox();
        teamAddUpdateClearHBox.getChildren().addAll(teamAddUpdateButton, teamClearButton);
        GridPane grid = new GridPane();
        grid.setHgap(50);
        grid.setVgap(30);

        ColumnConstraints labelColumn = new ColumnConstraints();
        labelColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(labelColumn);

        ColumnConstraints inputColumn = new ColumnConstraints();
        inputColumn.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(inputColumn);
        grid.add(nameLabel, 0, 0);
        grid.add(nameTF, 1, 0);
        grid.add(linkLabel, 0, 1);
        grid.add(linkTF, 1, 1);
        //grid.add(colorLabel, 0, 2);
        //grid.add(colorPicker, 1, 2);
        //grid.add(textColorLabel, 2, 2);
        //grid.add(textColorPicker, 3, 2);
        GridPane grid2 = new GridPane();
        grid2.setHgap(20);
        grid2.setVgap(30);

        ColumnConstraints labelColumn2 = new ColumnConstraints();
        labelColumn2.setHalignment(HPos.LEFT);
        grid2.getColumnConstraints().add(labelColumn2);

        ColumnConstraints inputColumn2 = new ColumnConstraints();
        inputColumn2.setHalignment(HPos.LEFT);
        grid2.getColumnConstraints().add(inputColumn2);
        grid2.add(colorLabel, 0, 0);
        grid2.add(colorPicker, 2, 0);
        grid2.add(textColorLabel, 3, 0);
        grid2.add(textColorPicker, 4, 0);
        //ScrollPane teamTablePane = new ScrollPane(teamTable);
        HBox teamTableHBox = new HBox(20);
        TeamTableLabel = new Label("TEAM");
        teamPane.getChildren().addAll(TeamTableLabel,grid, grid2, teamAddUpdateClearHBox);
        teamTableHBox.getChildren().addAll(teamTable, teamPane);

        GridPane studentGrid = new GridPane();
        studentGrid.setHgap(50);
        studentGrid.setVgap(30);

        ColumnConstraints studentGridlabelColumn = new ColumnConstraints();
        studentGridlabelColumn.setHalignment(HPos.LEFT);
        studentGrid.getColumnConstraints().add(studentGridlabelColumn);

        ColumnConstraints studentGridinputColumn = new ColumnConstraints();
        studentGridinputColumn.setHalignment(HPos.LEFT);
        studentGrid.getColumnConstraints().add(studentGridinputColumn);
        studentGrid.add(firstNameLabel, 0, 0);
        studentGrid.add(firstNameTF, 1, 0);
        studentGrid.add(lastNameLabel, 0, 1);
        studentGrid.add(lastNameTF, 1, 1);
        studentGrid.add(teamLabel, 0, 2);
        studentGrid.add(teamComboBox, 1, 2);
        studentGrid.add(roleLabel, 0, 3);
        studentGrid.add(roleTF, 1, 3);
        
        studentAddUpdateClearHBox = new HBox();
        studentAddUpdateClearHBox.getChildren().addAll(studentAddUpdateButton, studentClearButton);

        VBox studentPaneVBox = new VBox(30);
        StudentTableLabel = new Label("STUDENT"); 
        studentPaneVBox.getChildren().addAll(StudentTableLabel,studentGrid, studentAddUpdateClearHBox);
        studentPane.getChildren().addAll(studentPaneVBox, studentTable);
        HBox emptyPane = new HBox();

        //STUDENT PANE
//        studentPane.getChildren().addAll(studentHeaderLabelHBox, studentTable, addEditLabel);
//        VBox studentLabels = new VBox(30);
//        studentLabels.getChildren().addAll(firstNameLabel, lastNameLabel, teamLabel, roleLabel, studentAddUpdateButton);
//        VBox studentInputFields = new VBox(17);
//        studentInputFields.getChildren().addAll(
//                firstNameTF,
//                lastNameTF,
//                teamComboBox,
//                roleTF,
//                studentClearButton);
//        HBox studentInformationHBox = new HBox(20);
//        studentInformationHBox.getChildren().addAll(studentLabels, studentInputFields);
//
//        studentPane.getChildren().add(studentInformationHBox);
        //---------------------//
        // add all panes to main pane 
        projectMainPane = new VBox(20);
        projectMainPane.setPadding(new Insets(10, 10, 0, 30));
        projectMainPane.getChildren().add(projectHeaderLabelHBox);
        projectMainPane.getChildren().add(teamTableHBox);
        projectMainPane.getChildren().add(studentPane);
        projectMainPane.getChildren().add(emptyPane);
        ScrollPane mainScrollPane = new ScrollPane();
        mainScrollPane.setContent(projectMainPane);
        workspace = new BorderPane();
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(mainScrollPane);
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        mainScrollPane.setFitToWidth(true);
        nameTF.prefWidthProperty().bind(nameTF.widthProperty());
        linkTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.4));

        teamTable.setMinWidth(app.getGUI().getPrimaryStageWidth() / 2.2);
        teamTable.setMaxWidth(app.getGUI().getPrimaryStageWidth() / 2.2);

        nameColumn.prefWidthProperty().bind(teamTable.widthProperty().divide(4));
        colorColumn.prefWidthProperty().bind(teamTable.widthProperty().divide(4));
        textColumn.prefWidthProperty().bind(teamTable.widthProperty().divide(4));
        linkColumn.prefWidthProperty().bind(teamTable.widthProperty().divide(4));

        //teamTablePane.setMaxWidth(app.getGUI().getPrimaryStageWidth()/(2));
        firstNameTF.prefWidthProperty().bind(workspace.widthProperty().multiply(0.4));
        studentGrid.setMinWidth(app.getGUI().getPrimaryStageWidth() / 3);

        studentTable.setMinWidth(app.getGUI().getPrimaryStageWidth() / 2.3);
        studentTable.setMaxWidth(app.getGUI().getPrimaryStageWidth() / 2.3);

        firstNameColumn.prefWidthProperty().bind(studentTable.widthProperty().divide(4));
        lastNameColumn.prefWidthProperty().bind(studentTable.widthProperty().divide(4));
        teamColumn.prefWidthProperty().bind(studentTable.widthProperty().divide(4));
        roleColumn.prefWidthProperty().bind(studentTable.widthProperty().divide(4));
        controller = new ProjectController(app);

        teamAddUpdateButton.setOnAction(e -> {
            controller.handleAddTeam();
            teamClear();
        });

        studentAddUpdateButton.setOnAction(e -> {
            controller.handleAddStudent();
            studentClear();
        });
        //Student Input fields vbox...... need to change to HBox

        studentTable.setFocusTraversable(true);
        studentTable.setOnMousePressed(e -> {

            studentAddUpdateClearHBox.getChildren().clear();
            studentAddUpdateClearHBox.getChildren().addAll(studentUpdateButton, studentClearSelectionButton, studentDeleteButton);
            controller.handleStudentClicked();
            System.out.println("STUDENT CLICKED DONE, PROJECTWORKSPACE");
        });
        studentClearButton.setOnAction(e -> {
            studentClear();
        });
        teamClearButton.setOnAction(e -> {
            teamClear();
        });
        app.getGUI().getUndoButton().setOnAction(e -> {
            controller.handleReDoTransaction();
        });
        app.getGUI().getRedoButton().setOnAction(e -> {
            controller.handleUndoTransaction();
        });
        teamTable.setFocusTraversable(true);

        teamTable.setOnMousePressed(e -> {
            teamAddUpdateClearHBox.getChildren().clear();
            teamAddUpdateClearHBox.getChildren().addAll(teamUpdateButton, teamClearSelectionButton, teamDeleteButton);
            controller.handleTeamClicked();
        });
        studentClearSelectionButton.setOnAction(e -> {
            studentClearSelection();
//            firstNameTF.clear();
//            lastNameTF.clear();
//            teamComboBox.setValue(null);
//            roleTF.clear();
//            studentAddUpdateClearHBox.getChildren().clear();
//            studentAddUpdateClearHBox.getChildren().addAll(studentAddUpdateButton, studentClearButton);
        });
        teamClearSelectionButton.setOnAction(e -> {
            teamClearSelection();
        });
        teamDeleteButton.setOnAction(e -> {
            controller.handleDeleteTeam();
            studentClearSelection();
//            firstNameTF.clear();
//            lastNameTF.clear();
//            teamComboBox.setValue(null);
//            roleTF.clear();
//            studentAddUpdateClearHBox.getChildren().clear();
//            studentAddUpdateClearHBox.getChildren().addAll(studentAddUpdateButton, studentClearButton);

        });
        teamUpdateButton.setOnAction(e -> {
            controller.handleUpdateTeam();
        });
        teamTable.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DELETE)) {
                controller.handleDeleteTeam();

            }
        });
        studentDeleteButton.setOnAction(e -> {
            controller.handleDeleteStudent();
            studentClearSelection();
        });
        studentTable.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DELETE)) {
                controller.handleDeleteStudent();
                studentClearSelection();
            }
        });
        studentUpdateButton.setOnAction(e -> {
            controller.handleUpdateStudent();

        });
        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });

    }

    public void teamClearSelection() {
        nameTF.clear();
        linkTF.clear();
        colorPicker.setValue(Color.WHITE);
        textColorPicker.setValue(Color.WHITE);
        teamAddUpdateClearHBox.getChildren().clear();
        teamAddUpdateClearHBox.getChildren().addAll(teamAddUpdateButton, teamClearButton);
    }

    public void studentClearSelection() {
        firstNameTF.clear();
        lastNameTF.clear();
        teamComboBox.setValue(null);
        roleTF.clear();
        studentAddUpdateClearHBox.getChildren().clear();
        studentAddUpdateClearHBox.getChildren().addAll(studentAddUpdateButton, studentClearButton);

    }

    public void teamClear() {
        nameTF.clear();
        linkTF.clear();
        colorPicker.setValue(Color.WHITE);
        textColorPicker.setValue(Color.WHITE);
    }

    public void studentClear() {
        firstNameTF.clear();
        lastNameTF.clear();
        teamComboBox.setValue(null);
        roleTF.clear();
    }

    @Override
    public void resetWorkspace() {
        
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
    }

    public CourseSiteGeneratorApp getApp() {
        return app;
    }

    public VBox getProjectMainPane() {
        return projectMainPane;
    }

    public HBox getProjectHeaderLabelHBox() {
        return projectHeaderLabelHBox;
    }

    public HBox getTeamHeaderLabelHBox() {
        return teamHeaderLabelHBox;
    }

    public HBox getStudentHeaderLabelHBox() {
        return studentHeaderLabelHBox;
    }

    public Label getProjectHeaderLabel() {
        return projectHeaderLabel;
    }

    public VBox getTeamPane() {
        return teamPane;
    }

    public HBox getStudentPane() {
        return studentPane;
    }

    public Label getTeamsHeaderLabel() {
        return teamsHeaderLabel;
    }

    public Label getStudentHeaderLabel() {
        return studentHeaderLabel;
    }

    public Label getAddEditLabel() {
        return addEditLabel;
    }

    public Label getNameLabel() {
        return nameLabel;
    }

    public Label getColorLabel() {
        return colorLabel;
    }

    public Label getTextColorLabel() {
        return textColorLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Label getFirstNameLabel() {
        return firstNameLabel;
    }

    public Label getLastNameLabel() {
        return lastNameLabel;
    }

    public Label getTeamLabel() {
        return teamLabel;
    }

    public Label getRoleLabel() {
        return roleLabel;
    }

    public TextField getNameTF() {
        return nameTF;
    }

    public TextField getLinkTF() {
        return linkTF;
    }

    public TextField getFirstNameTF() {
        return firstNameTF;
    }

    public TextField getLastNameTF() {
        return lastNameTF;
    }

    public TextField getRoleTF() {
        return roleTF;
    }

    public ComboBox getTeamComboBox() {
        return teamComboBox;
    }

    public Button getTeamAddUpdateButton() {
        return teamAddUpdateButton;
    }

    public Button getTeamClearButton() {
        return teamClearButton;
    }

    public Button getStudentAddUpdateButton() {
        return studentAddUpdateButton;
    }

    public Button getStudentClearButton() {
        return studentClearButton;
    }

    public TableView<Team> getTeamTable() {
        return teamTable;
    }

    public TableColumn<Team, String> getNameColumn() {
        return nameColumn;
    }

    public TableColumn<Team, String> getColorColumn() {
        return colorColumn;
    }

    public TableColumn<Team, String> getTextColumn() {
        return textColumn;
    }

    public TableColumn<Team, String> getLinkColumn() {
        return linkColumn;
    }

    public TableView<Student> getStudentTable() {
        return studentTable;
    }

    public TableColumn<Student, String> getFirstNameColumn() {
        return firstNameColumn;
    }

    public TableColumn<Student, String> getLastNameColumn() {
        return lastNameColumn;
    }

    public TableColumn<Student, String> getTeamColumn() {
        return teamColumn;
    }

    public TableColumn<Student, String> getRoleColumn() {
        return roleColumn;
    }

    public ColorPicker getColorPicker() {
        return colorPicker;
    }

    public ColorPicker getTextColorPicker() {
        return textColorPicker;
    }

    public Pane getWorkspace() {
        return workspace;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public ProjectController getController() {
        return controller;
    }

    public HBox getTeamAddUpdateClearHBox() {
        return teamAddUpdateClearHBox;
    }

    public HBox getStudentAddUpdateClearHBox() {
        return studentAddUpdateClearHBox;
    }

    public Label getTeamTableLabel() {
        return TeamTableLabel;
    }

    public Label getStudentTableLabel() {
        return StudentTableLabel;
    }

    public Button getTeamClearSelectionButton() {
        return teamClearSelectionButton;
    }

    public Button getTeamDeleteButton() {
        return teamDeleteButton;
    }

    public Button getTeamUpdateButton() {
        return teamUpdateButton;
    }

    public Button getStudentClearSelectionButton() {
        return studentClearSelectionButton;
    }

    public Button getStudentDeleteButton() {
        return studentDeleteButton;
    }

    public Button getStudentUpdateButton() {
        return studentUpdateButton;
    }
    

}
