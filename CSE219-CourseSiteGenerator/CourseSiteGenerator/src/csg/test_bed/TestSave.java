/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.CourseData;
import csg.data.RecitationData;
import csg.data.ScheduleData;
import csg.data.StudentData;
import csg.data.TAData;
import csg.data.TeamData;
import csg.file.TimeSlot;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author khurr
 */
public class TestSave {

    CourseSiteGeneratorApp app;
    CSGData csgData;
    //CSGData newcsgData;
    TAData taData;
    RecitationData recitationData;
    CourseData courseData;
    ScheduleData scheduleData;
    TeamData teamData;
    StudentData studentData; 
    ArrayList<TimeSlot> timeSlotData;

    public TestSave() {

        app = new CourseSiteGeneratorApp();
        app.loadProperties("app_properties.xml"); 
        app.buildAppComponentsHookTest(); // ONLY DATA AND FILE COMPONENT ARE INITIALIZED 

        csgData = (CSGData) app.getDataComponent();
        //newcsgData=csgData; 
        //System.out.println(newcsgData.equals(csgData)); 
        taData = csgData.getTAData();
        recitationData = csgData.getRecitationData();
        courseData = csgData.getCourseData();
        scheduleData = csgData.getScheduleData();
        teamData = csgData.getTeamData();
        studentData=csgData.getStudentData();

        timeSlotData = taData.getTimeSlot();
        //Boolean trueBoolean = true;
        //Boolean falseBoolean = false;
        int startHour = 9;
        int endHour = 20;
        taData.setStartHour(startHour);
        taData.setEndHour(endHour);
        scheduleData.setStartMonday("10/16/2017");
        scheduleData.setEndFriday("12/15/2017");
        
        taData.addTA("John Doe", "MrJohnDoe@Stonybrook.edu", true);
        taData.addTA("Doe John", "doeJohn@stonybrook.edu", false);
        taData.addTA("Something Random2", "somethingRandom@stonybrook.edu", false);
        taData.addTA("SomethingWeird", "weird@email.com", true);

        timeSlotData.add(new TimeSlot("MONDAY", "10_00am", "Something Random"));
        timeSlotData.add(new TimeSlot("TUESDAY", "10_00am", "John Doe"));
        timeSlotData.add(new TimeSlot("MONDAY", "10_00am", "Something Random"));
        timeSlotData.add(new TimeSlot("MONDAY", "5_00pm", "Doe John"));
        timeSlotData.add(new TimeSlot("TUESDAY", "3_00pm", "Something Random"));
        

//        taData.addOfficeHoursReservation("MONDAY", "10_00am", "Something Random");
//
//        taData.addOfficeHoursReservation("TUESDAY", "10_00am", "John Doe");
//        taData.addOfficeHoursReservation("MONDAY", "10_00am", "Something Random");
//        taData.addOfficeHoursReservation("MONDAY", "5_00pm", "Doe John");
//        taData.addOfficeHoursReservation("TUESDAY", "3_00pm", "Something Random")
//        
        recitationData.addRecitation("R01", "Mckenna", "Monday,","12:05","15:35" ,"Old Computer Science 2114", "John Doe", "Something Random2");
        recitationData.addRecitation("R02", "Mckenna", "Thursday,","15:25","17:03" , "Old Computer Science 2114", "Something Random2", "SomethingWeird");
        recitationData.addRecitation("R03", "Mckenna", "Wednesday,","13:00","14:53" , "Old Computer Science 2114", "John Doe", "Joe Shmo");

        courseData.addCourse("CSE", "Fall", "219", "2017", "CS III", "Richard Mckenna", "www.cs.stonybrook.edu");

        scheduleData.addSchedule("HOLIDAY", "02/09/2017", "", "SNOW DAY", "", "somelink", "");
        scheduleData.addSchedule("Lecture", "02/14/2017", "", "Lecture 3 ", "Event Programming", "", "");
        scheduleData.addSchedule("HW", "03/27/2017", "", "Homework 3 ", "UML", "", "");
        
        
        teamData.addTeam("TEAM 1", "4d", "33", "99", "#FEB366", "someLink");
        teamData.addTeam("TEAM 2", "b3", "4d", "1a", "#994D66", "someLink");
        teamData.addTeam("TEAM 3", "4d", "80", "80", "#CC99CC", "someLink");
        teamData.addTeam("TEAM 4", "80", "4d", "80", "#804D80", "someLink");
        
        studentData.addStudent("Beau", "Brummell", "TEAM 1", "Lead Designer");
        studentData.addStudent("Jane", "Doe", "TEAM 2", "Lead Programmer");
        studentData.addStudent("Noonlan", "Soong", "TEAM 3", "Data Desginer");
        studentData.addStudent("Joe", "Shmo", "TEAM 3", "GUI Desginer");
        studentData.addStudent("Richard", "Mckenna", "TEAM 2", "Data Desginer");
        studentData.addStudent("Something", "Random", "TEAM 4", "Data Desginer");
        
        
        
        
        

    }

    public void testSave(String filePath) throws IOException{
        app.getFileComponent().saveData(csgData, filePath);
    }
    

}
