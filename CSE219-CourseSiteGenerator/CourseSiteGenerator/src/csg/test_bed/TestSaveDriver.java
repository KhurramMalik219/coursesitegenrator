/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import java.io.IOException;

/**
 *
 * @author khurr
 */
public class TestSaveDriver {

    // public static CourseSiteGeneratorApp app;
    public static void main(String[] args) throws IOException {

        //app = new CourseSiteGeneratorApp();
        //app.loadProperties("app_properties.xml");
        // app.buildAppComponentsHookTest();
        //TestSave testSave = new TestSave(app);
        // CSGData newData = testSave.getData();
        
        // app.getFileComponent().saveData(newData, filePath);
        
        
        String filePath = "../CourseSiteGenerator/work/SiteSaveTest.json";
        String filePathExpected = "../CourseSiteGenerator/work/SiteSaveTestExpected.json";
        String errorFilePath="../CourseSiteGenerator/work/SiteSaveTestError.json";
        TestSave testSave = new TestSave();
        
        
        //testSave.testSave(errorFilePath);
        testSave.testSave(filePath);
        testSave.testSave(filePathExpected);

    }

}
