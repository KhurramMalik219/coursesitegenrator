/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import csg.CourseSiteGeneratorApp;
import csg.data.CSGData;
import csg.data.CourseData;
import csg.data.RecitationData;
import csg.data.ScheduleData;
import csg.data.TAData;
import csg.file.CSGFiles;
import csg.file.TimeSlot;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author khurr
 */
public class TestLoad {

    CourseSiteGeneratorApp app;
    CSGData csgData;
    //CSGData newcsgData;
   

    public TestLoad() {

        app = new CourseSiteGeneratorApp();
        app.loadProperties("app_properties.xml");
        app.buildAppComponentsHookTest();
        csgData = (CSGData) app.getDataComponent();

    }

    public boolean loadTest(String filePath, String expectedFilePath) throws IOException {
        
        CSGFiles csgFile = (CSGFiles)app.getFileComponent();
        boolean same;
        try{
             same=csgFile.loadDataForTest(csgData, filePath, expectedFilePath);
        }catch(Exception e){
            same=false;
            System.out.println("error");
        }
        System.out.println(same);
        return same; 
        //app.getFileComponent().loadData(csgData, filePath);
        //return (CSGData) app.getDataComponent();

    }
}
