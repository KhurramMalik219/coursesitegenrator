/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * @author Richard McKenna
 * @co-author Khurram Malik
 * @version 2.0 
 */
public enum CourseSiteGeneratorProp {
     // FOR SIMPLE OK/CANCEL DIALOG BOXES
    OK_PROMPT,
    CANCEL_PROMPT,
    
    // THESE ARE FOR TEXT PARTICULAR TO THE APP'S WORKSPACE CONTROLS
    TAS_HEADER_TEXT,
    GRAD_COLUMN_TEXT,
    NAME_COLUMN_TEXT,
    EMAIL_COLUMN_TEXT,
    NAME_PROMPT_TEXT,
    EMAIL_PROMPT_TEXT,
    START_HOUR_PROMPT_TEXT,
    END_HOUR_PROMPT_TEXT, 
    ADD_BUTTON_TEXT,
    CHANGE_TIME_BUTTON_TEXT, 
    UPDATE_TA_BUTTON_TEXT,
    CLEAR_BUTTON_TEXT,
    OFFICE_HOURS_SUBHEADER,
    OFFICE_HOURS_TABLE_HEADERS,
    DAYS_OF_WEEK,
    UPDATE_TIME_TITLE,
    UPDATE_TIME_MESSAGE,
    TA_DATA_TAB_TEXT,
    COURSE_TAB_TEXT,
    SCHEDULE_TAB_TEXT,
    RECITATION_TAB_TEXT,
    PROJECT_TAB_TEXT,
    
    // Recitation Workspace
    SECTION_COLUMN_TEXT,
    INSTRUCTOR_COLUMN_TEXT,
    DAY_TIME_COLUMN_TEXT,
    LOCATION_COLUMN_TEXT,
    TA_COLUMN_TEXT,
    
    RHL_HEADER_TEXT,
    ADL_HEADER_TEXT,
    SECTION_LABEL_TEXT,
    INSTRUCTOR_LABEL_TEXT,
    DAY_TIME_LABEL_TEXT,
    LOCATION_LABEL_TEXT,
    SUPER_TA_LABEL_TEXT,
    
    ADD_UPDATE_BUTTON_TEXT,
    UPDATE_REC_BUTTON_TEXT,
    DELETE_BUTTON_TEXT,
    
    // ------------------ // 
    
    //Schedule Workspace 
    STARTING_MONDAY_LABEL_TEXT,
    ENDING_FRIDAY_LABEL_TEXT,
    SHL_LABEL_TEXT,
    SIHL_LABEL_TEXT,
    TYPE_LABEL_TEXT,
    DATE_LABEL_TEXT,
    TIME_LABEL_TEXT,
    TITLE_LABEL_TEXT,
    TOPIC_LABEL_TEXT,
    LINK_LABEL_TEXT,
    
    CRITERIA_LABEL_TEXT,
    ADD_EDIT_LABEL_TEXT,
    CALENDAR_LABEL_TEXT,
    
    MISSING_TYPE_TITLE,
    MISSING_TYPE_MESSAGE,
    
    MISSING_DATE_TITLE,
    MISSING_DATE_MESSAGE,
    
    MISSING_TIME_TITLE,
    MISSING_TIME_MESSAGE,
    
    MISSING_TOPIC_TITLE,
    MISSING_TOPIC_MESSAGE,
    
    MISSING_LINK_TITLE,
    MISSING_LINK_MESSAGE,
    
    MISSING_CRITERIA_TITLE,
    MISSING_CRITERIA_MESSAGE,
    
    MISSING_TITLE_TITLE,
    MISSING_TITLE_MESSAGE,
   
    
    //--------------------//
    
    //CourseWorkspace 
    SUBJECT_LABEL_TEXT,
    SEMESTER_LABEL_TEXT,
    INST_HOME_LABEL_TEXT,
    INST_NAME_LABEL_TEXT,
    EXPORT_LABEL_TEXT,
    DIR_LABEL_TEXT,
    NUMBER_LABEL_TEXT,
    YEAR_LABEL_TEXT,
    CIHLT_LABEL_TEXT,
    STHLT_LABEL_TEXT,
    PSHLT_LABEL_TEXT,
    STIT_LABEL_TEXT,
    BANNER_LABEL_TEXT,
    LEFT_FOOTER_LABEL_TEXT,
    RIGHT_FOOTER_LABEL_TEXT,
    STYLESHEET_LABEL_TEXT,
    NOTE_LABEL_TEXT,
    STLT_LABEL_TEXT,
    
    
    
    //---------------//
    
    //Project Tab Stuff // 
    PHLT_LABEL_TEXT,
    THLT_LABEL_TEXT,
    NAME_LABEL_TEXT,
    COLOR_LABEL_TEXT,
    TEXT_COLOR_LABEL_TEXT,
 
    FIRST_NAME_LABEL_TEXT,
    LAST_NAME_LABEL_TEXT,
    TEAM_LABEL_TEXT,
    ROLE_LABEL_TEXT,
    SHLT_LABEL_TEXT,
    TEXT_COLOR_COLUMN_TEXT,
    
    COLOR_COLUMN_TEXT,
    LINK_COLUMN_TEXT,
    FIRST_NAME_COLUMN_TEXT,
    LAST_NAME_COLUMN_TEXT,
    TEAM_COLUMN_TEXT,
    ROLE_COLUMN_TEXT,
    //--------------------//
    
    // THESE ARE FOR ERROR MESSAGES PARTICULAR TO THE APP
    MISSING_TA_NAME_TITLE,
    MISSING_TA_NAME_MESSAGE,
    MISSING_TA_EMAIL_TITLE,
    MISSING_TA_EMAIL_MESSAGE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE,
    INVALID_TA_EMAIL_TITLE,
    INVALID_TA_EMAIL_MESSAGE,
    MISSING_SECTION_TITLE,
    MISSING_SECTION_MESSAGE,
    MISSING_INSTRUCTOR_TITLE,
    MISSING_INSTRUCTOR_MESSAGE,
    MISSING_DAYTIME_TITLE,
    MISSING_DAYTIME_MESSAGE,
    MISSING_SUPERTA_TITLE,
    MISSING_SUPERTA_MESSAGE,
    MISSING_LOCATION_TITLE,
    MISSING_LOCATION_MESSAGE,
    EMPTY_SUPERTA_TITLE,
    EMPTY_SUPERTA_MESSAGE,
    NO_UPDATE_TITLE,
    NO_UPDATE_MESSAGE,
    INVALID_TIME_INPUT_TITLE,
    INVALID_TIME_INPUT_MESSAGE
}
