/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khurr
 */
public class TestSaveTest {

    public TestSaveTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of testSave method, of class TestSave.
     */
    @Test
    public void testTestSave() throws Exception {
        System.out.println("TEST SAVE");
        String filePath = "../CourseSiteGenerator/work/SiteSaveTest.json";
        String filePathExpected = "../CourseSiteGenerator/work/SiteSaveTestExpected.json";

        TestSave instance = new TestSave();
        instance.testSave(filePath);
        File expectedFile = new File(filePathExpected);
        File result = new File(filePath);
        String expectedFileString = FileUtils.readFileToString(expectedFile, "utf-8");
        String resultFileString = FileUtils.readFileToString(result, "utf-8");

        assertEquals("Inconsistencies in file", expectedFileString, resultFileString);

    }

}
