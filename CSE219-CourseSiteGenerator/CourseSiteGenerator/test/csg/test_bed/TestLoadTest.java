/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khurr
 */
public class TestLoadTest {
    
    public TestLoadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of loadTest method, of class TestLoad.
     */
    @Test
    public void testLoadTest() throws Exception {
        System.out.println("loadTest");
        String filePath ="../CourseSiteGenerator/work/SiteSaveTest.json";
        String expectedFilePath ="../CourseSiteGenerator/work/SiteSaveTestExpected.json";
        TestLoad instance = new TestLoad();
        boolean result= instance.loadTest(filePath, expectedFilePath);
        boolean expected=true; 
        assertEquals("Data Values Are Not The Same!",result,expected); 
        
    }
    
}
